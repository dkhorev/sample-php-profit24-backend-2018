<?php

namespace WebNow;


use Behat\Transliterator\Transliterator;

class Helpers
{
    /**
     * простейшая валидация наличия полей в формах
     *
     * @param array $input
     * @param array $rules
     *
     * @return array
     */
    public static function validateArray(array $input, array $rules): array
    {
        foreach ($input as $field => $value) {
            $thisRules = $rules[$field];

            if ($thisRules) {
                switch ($thisRules) {
                    case 'required':
                        if (strlen(trim($value)) === 0) {
                            return [
                                'error'    => true,
                                'msg'      => 'Поле ' . $field . ' обязательное',
                                'msgHuman' => 'Поле ' . $field . ' обязательное',
                            ];
                        }
                        break;
                }
            }
        }

        return [];
    }

    /**
     * @param string $sVal
     * @param $b24_id
     *
     * @return string
     */
    public static function getCode(string $sVal, $b24_id = false): string
    {
        $res = Transliterator::transliterate($sVal, '_');

        // фирмы из црм могут легко иметь дубли по кодам/названиям
        if ($b24_id) {
            $res = $b24_id . '_' . $res;
        }

        return $res;
    }

    /**
     * @param $field
     * @param $arInput
     *
     * @return array
     */
    public static function extractByField($field, $arInput)
    {
        $ar = array_map(function($item) use ($field) {
            return $item[$field];
        }, $arInput);
        return array_filter(array_unique($ar));
    }

    /**
     * нормализует строки, чтобы мы могли их сранвивать
     * минус - пробелы, знаки препинания и все такое
     * плюс - транслитерация
     *
     * @param $str
     *
     * @return mixed
     */
    public static function normalizeStr($str)
    {
        $str = html_entity_decode($str);
        $str = strtolower($str);
        $str = str_replace("\xc2\xa0", "\x20", $str); // заменяет пробелы на норм символы
        $str = preg_replace('/[^\p{L}\p{N}\s]/u', '', $str);
        $str = preg_replace('/\s+/', '', $str);
        return trim(trim($str, '_'));
    }

    /**
     * @param $arPortal
     *
     * @return string
     */
    public static function getBillsPath($arPortal)
    {
        return BILLS_PATH . $arPortal['member_id'] . '/';
    }
}