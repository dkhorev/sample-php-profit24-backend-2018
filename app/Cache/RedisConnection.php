<?php

namespace WebNow\Cache;

use Bitrix\Main\Config\Option;

abstract class RedisConnection
{
    protected static $client;
    protected static $server;
    protected static $has_connect = true;
    
    public function __construct()
    {
        if (!static::$client) {
            static::$client = new \Predis\Client([
                'scheme' => Option::get(WEBNOW_MODULE_ID, 'REDIS_CONNECT', DEFAULT_REDIS_CONNECT),
                'host'   => Option::get(WEBNOW_MODULE_ID, 'REDIS_IP', DEFAULT_REDIS_IP),
                'port'   => Option::get(WEBNOW_MODULE_ID, 'REDIS_PORT', DEFAULT_REDIS_PORT),
            ]);
            
            static::$server = Option::get(WEBNOW_MODULE_ID, 'REDIS_PREFIX', DEFAULT_REDIS_PREFIX);
        }
    }
    
    /**
     * доступ к объекту
     *
     * @return \Predis\Client
     */
    public static function GetClient()
    {
        return static::$client;
    }
    
    /**
     * @param string $key
     *
     * @return string
     */
    protected static function GetFullKey(string $key)
    {
        return static::$server . ':' . $key;
    }
    
}