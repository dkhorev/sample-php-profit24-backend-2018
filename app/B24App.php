<?php

namespace WebNow;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use WebNow\Project\Model\B24Status;

/**
 * Class B24App
 * отвечает за создание обработку запросов к REST API битрикс
 * продление токенов и обновление инфы по ним
 *
 * @package WebNow
 */
class B24App
{
    protected $app;
    protected $arScope = ['user', 'crm', 'entity', 'department'];
    protected $btokenRefreshed = false;
    protected $errorMessage;
    protected $arParams;
    private static $arInvoiceSelect = [
        'ID',
        'ACCOUNT_NUMBER',
        'DATE_BILL',
        'DATE_INSERT',
        'DATE_PAYED',
        'DATE_STATUS',
        'DATE_UPDATE',
        'PAY_VOUCHER_DATE',
        'STATUS_ID',
        'PRICE',
        'RESPONSIBLE_ID',
        'UF_COMPANY_ID',
        'UF_CONTACT_ID',
        'PERSON_TYPE_ID',
        'UF_MYCOMPANY_ID',
        // 'PAY_SYSTEM_ID',
        'ORDER_TOPIC',
        // ?
        'EMP_PAYED_ID',
        'EMP_STATUS_ID',
        'PAYED',
    ];
    private static $arLeadSelect = [
        'ID',
        'DATE_CREATE',
        'TITLE',
        'OPPORTUNITY',
        'STATUS_ID',
        'COMPANY_ID',
        'CONTACT_ID',
        'ASSIGNED_BY_ID',
        'NAME',
        'SECOND_NAME',
        'LAST_NAME',
    ];
    private static $arCompanySelect = ['ID', 'TITLE', 'IS_MY_COMPANY'];

    /**
     * B24App constructor.
     *
     * @param array $arParams
     * @param bool  $bTestTokenExpire
     *
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     */
    public function __construct(array $arParams, bool $bTestTokenExpire = true)
    {
        $this->arParams = $arParams;

        $log = new Logger('loading.b24');
        $log->pushHandler(new StreamHandler(LOG_FILE, Logger::INFO));

        // init lib
        $obB24App = new \Bitrix24\Bitrix24(false, $log);
        $obB24App->setApplicationScope($this->arScope);
        $obB24App->setApplicationId(CLIENT_ID);
        $obB24App->setApplicationSecret(CLIENT_SECRET);

        // set user-specific settings
        $obB24App->setDomain($arParams['domain']);
        $obB24App->setMemberId($arParams['member_id']);
        $obB24App->setRefreshToken($arParams['refresh_token']);
        $obB24App->setAccessToken($arParams['access_token']);
        // dump('access_token before = ' . $arParams['access_token']);
        // dump('refresh_token before = ' . $arParams['refresh_token']);

        // проверяем токен если надо
        if ($bTestTokenExpire) {
            // dump('testing token start...');
            $resExpire = false;

            try {
                $resExpire = $obB24App->isAccessTokenExpire();
            } catch (\Exception $e) {
                $this->errorMessage = utf8_encode($e->getMessage());
            }
            // $resExpire = true;
            // dump('$resExpire = ');
            // dump($resExpire);

            if ($resExpire) {
                $obB24App->setRedirectUri(APP_REG_URL);

                $result = false;
                try {
                    $result = $obB24App->getNewAccessToken();
                    // dump($result);
                } catch (\Exception $e) {
                    $this->errorMessage = utf8_encode($e->getMessage());
                }
                if ($result === false) {
                    $this->errorMessage = 'access denied';
                } elseif (is_array($result) && array_key_exists('access_token',
                        $result) && !empty($result['access_token'])) {

                    $obB24App->setRefreshToken($result['refresh_token']);
                    $obB24App->setAccessToken($result['access_token']);

                    // dump('access_token after = ' . $result['access_token']);
                    // dump('refresh_token after = ' . $result['refresh_token']);

                    $this->arParams['expires_in'] = $result['expires'];
                    $this->arParams['refresh_token'] = $result['refresh_token'];
                    $this->arParams['access_token'] = $result['access_token'];


                    $this->btokenRefreshed = true;
                } else {
                    $this->btokenRefreshed = false;
                }
            } else {
                $this->btokenRefreshed = false;
            }
        }

        if ($this->errorMessage) {
            static::returnJSONResult(['status' => 'error', 'result' => $this->errorMessage]);
        }

        $this->app = $obB24App;

        if ($this->btokenRefreshed) {
            $this->saveAuth();
        }
    }

    /**
     * @param $entity
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getStatusList($entity)
    {
        $arOrder = ["SORT" => "ASC"]; // !!! это важно, т.к. операции по группам идут по порядку сортировки.... такой б24
        $arFilter = ['ENTITY_ID' => $entity];

        $method = 'crm.status.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * crm.invoice.update(id, fields)
     *
     * @param $arData
     *
     * @return array
     */
    public function setInvoicesPaid($arData)
    {
        $arRes = [];

        $method = 'crm.invoice.update';
        $rsApp = $this->app;

        $count = 0;
        foreach ($arData as $ar) {
            $rsApp->addBatchCall(
                $method,
                [
                    'id'     => $ar['ID'],
                    'fields' => [
                        'STATUS_ID'        => $ar['STATUS_ID'],
                        'PAY_VOUCHER_DATE' => $ar['PAY_VOUCHER_DATE'],
                    ],
                ],
                function ($result) use (&$arRes) {
                    // save result
                    $arRes[] = $result;
                }
            );
            $count++;

            if ($count % BATCH_LIMIT === 0) {
                $rsApp->processBatchCalls(0, BATCH_DELAY);
                $count = 0;
            }
        }

        if ($count > 0) {
            $rsApp->processBatchCalls(0, BATCH_DELAY);
        }

        return $arRes;
    }

    /**
     * это используется только в загрузке при установке
     *
     * @param bool $date_from
     * @param bool $date_to
     *
     * @param int  $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getInvoices($date_from = false, $date_to = false, $from = 0)
    {
        $arOrder = ['ID' => 'ASC'];
        $arFilter = [];
        if ($date_from) {
            $arFilter['>=DATE_BILL'] = date("Y-m-d H:i:s", strtotime($date_from));
        }
        if ($date_to) {
            $arFilter["<DATE_BILL"] = date("Y-m-d H:i:s", strtotime($date_to) + 86400);
        }

        // умолчанию не грузим проигранные счета
        $arBadStatuses = B24Status::getStatuses(B24Status::STATUS_INVOICE, B24Status::GROUP_FAIL);
        $arFilter['!STATUS_ID'] = $arBadStatuses;

        $arSelect = static::$arInvoiceSelect;
        // $arSelect = [];

        $method = 'crm.invoice.list';
        $arQuery = [
            'start'  => $from,
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        // dump($arQuery);
        // die;

        return $this->executeQueryOnce($method, $arQuery);
    }

    /**
     * @param $arId
     * @param $bAnyStatus
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getInvoicesById($arId, $bAnyStatus = false)
    {
        if (!is_array($arId)) {
            $arId = [$arId];
        }

        $arOrder = ['DATE_BILL' => 'ASC'];
        $arFilter = ['ID' => $arId];

        // по умолчанию не грузим проигранные счета
        if (false === $bAnyStatus) {
            $arBadStatuses = B24Status::getStatuses(B24Status::STATUS_INVOICE, B24Status::GROUP_FAIL);
            $arFilter['!STATUS_ID'] = $arBadStatuses;
        }

        $arSelect = static::$arInvoiceSelect;
        // $arSelect = [];

        $method = 'crm.invoice.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * все открытые счета
     * @param $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getInvoicesOpen()
    {
        $arOrder = ['DATE_BILL' => 'ASC'];

        $arFilter = [];
        $arStatuses = B24Status::getStatuses(B24Status::STATUS_INVOICE, [B24Status::GROUP_NEW, B24Status::GROUP_PROGRESS]);
        $arFilter['STATUS_ID'] = $arStatuses;

        // $arSelect = static::$arInvoiceSelect;
        $arSelect = ['ID', 'ACCOUNT_NUMBER'];

        $method = 'crm.invoice.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * это используется только в загрузке при установке
     *
     * @param $date_from
     * @param $date_to
     * @param $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getLeads($date_from = false, $date_to = false, $from = 0)
    {
        $arOrder = ['ID' => 'ASC'];

        // по умолчанию не грузим закрытые лиды
        $arBadStatuses = B24Status::getStatuses(B24Status::STATUS_LEAD, B24Status::GROUP_SUCCESS);
        $arFilter = ['>OPPORTUNITY' => 0, '!STATUS_ID' => $arBadStatuses];

        if ($date_from) {
            $arFilter['>=DATE_CREATE'] = date("Y-m-d H:i:s", strtotime($date_from));
        }
        if ($date_to) {
            $arFilter["<DATE_CREATE"] = date("Y-m-d H:i:s", strtotime($date_to) + 86400);
        }

        $arSelect = static::$arLeadSelect;

        $method = 'crm.lead.list';
        $arQuery = [
            'start'  => $from,
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        // dump($arQuery);

        return $this->executeQueryOnce($method, $arQuery);
    }



    /**
     * @param $arId
     * @param $bAnyStatus
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getLeadsById($arId, $bAnyStatus = false)
    {
        if (!is_array($arId)) {
            $arId = [$arId];
        }

        $arOrder = ['DATE_CREATE' => 'ASC'];
        $arFilter = ['ID' => $arId]; // грузим открытые или проваленные

        // по умолчанию не грузим закрытые лиды
        if (false === $bAnyStatus) {
            $arBadStatuses = B24Status::getStatuses(B24Status::STATUS_LEAD, B24Status::GROUP_SUCCESS);
            $arFilter['!STATUS_ID'] = $arBadStatuses;
        }
        $arSelect = static::$arLeadSelect;

        $method = 'crm.lead.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param $arId
     * @param $type
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getRequisiteById($arId = [], $type = 'company')
    {
        if ($arId && !is_array($arId)) {
            $arId = [$arId];
        }

        $arOrder = ['ID' => 'ASC'];
        $arSelect = ["ID", "RQ_INN", "ENTITY_ID", "ENTITY_TYPE_ID"];

        // Б24 хардкод ...
        $arFilter = ['ENTITY_TYPE_ID' => 4]; // реквизиты компаний
        if ('company' !== $type) {
            $arFilter['ENTITY_TYPE_ID'] = 3; // реквизиты контактов
        }

        if (count($arId)) {
            $arFilter['ENTITY_ID'] = $arId;
        }

        $method = 'crm.requisite.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getCompaniesById($arId = [])
    {
        if ($arId && !is_array($arId)) {
            $arId = [$arId];
        }

        $arOrder = ['ID' => 'ASC'];
        $arFilter = [];
        // $arSelect = ['ID', 'TITLE', 'IS_MY_COMPANY', 'ORIGINATOR_ID', 'ORIGIN_ID'];
        $arSelect = static::$arCompanySelect;

        if (count($arId)) {
            $arFilter['ID'] = $arId;
        }

        $method = 'crm.company.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getMyCompanies($arId = [])
    {
        $arOrder = ['ID' => 'ASC'];
        $arFilter = ['IS_MY_COMPANY' => 'Y'];
        // $arSelect = ['ID', 'TITLE', 'IS_MY_COMPANY', 'BANKING_DETAILS'];
        $arSelect = static::$arCompanySelect;

        if (count($arId)) {
            $arFilter['ID'] = $arId;
        }

        $method = 'crm.company.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getNotMyCompanies($arId = [])
    {
        $arOrder = ['ID' => 'ASC'];
        $arFilter = ['IS_MY_COMPANY' => 'N'];
        // $arSelect = ['ID', 'TITLE', 'IS_MY_COMPANY', 'BANKING_DETAILS'];
        $arSelect = static::$arCompanySelect;

        if (count($arId)) {
            $arFilter['ID'] = $arId;
        }

        $method = 'crm.company.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param array $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getContactsById($arId = [])
    {
        return $this->getContacts($arId);
    }

    /**
     * @param array $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getContacts($arId = [])
    {
        if ($arId && !is_array($arId)) {
            $arId = [$arId];
        }

        $arOrder = ['ID' => 'ASC'];
        $arFilter = [];
        $arSelect = ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME'];

        if (count($arId)) {
            $arFilter['ID'] = $arId;
        }

        $method = 'crm.contact.list';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     * @param array $arId
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getUsers($arId = [])
    {
        $arOrder = ['ID' => 'ASC'];
        $arFilter = [];
        $arSelect = ['ID', 'NAME', 'LAST_NAME', 'EMAIL', 'ACTIVE', 'UF_DEPARTMENT']; // не работает отбор

        if (count($arId)) {
            $arFilter['ID'] = $arId;
        }

        $method = 'user.get';
        $arQuery = [
            'order'  => $arOrder,
            'filter' => $arFilter,
            'select' => $arSelect,
        ];

        return $this->executeQuery($method, $arQuery);
    }

    /**
     *
     * @param int $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function getUsersActive($from = 0)
    {
        $arFilter = ['ACTIVE' => true];

        $method = 'user.get';
        $arQuery = [
            'start'  => $from,
            'SORT'   => 'ID',
            'ORDER'  => 'ASC',
            'FILTER' => $arFilter,
        ];

        $arAll = $this->executeQueryOnce($method, $arQuery);

        $arAll['data'] = array_filter($arAll['data'], function ($item) {
            return count($item['UF_DEPARTMENT']) > 0;
        });

        return $arAll;
    }

    /**
     * @param $method
     * @param $arQuery
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    private function executeQuery($method, $arQuery)
    {
        $arRes = [];

        $rsApp = $this->app;
        $rsApp->addBatchCall(
            $method,
            $arQuery,
            function ($result) use ($rsApp, &$arRes, $arQuery, $method) {
                // save first page
                foreach ($result['result'] as $ar) {
                    $arRes[$ar['ID']] = $ar;
                }

                // add calls for subsequent pages
                if ($result['next']) {
                    for ($i = $result['next']; $i < $result['total']; $i += $result['next']) {
                        // dump('next $i = ' . $i);
                        $rsApp->addBatchCall(
                            $method,
                            array_merge(['start' => $i], $arQuery),
                            function ($result) use (&$arRes) {
                                // save subsequent pages
                                foreach ($result['result'] as $ar) {
                                    $arRes[$ar['ID']] = $ar;
                                }
                            }
                        );
                    }
                }
            }
        );
        $rsApp->processBatchCalls(0, BATCH_DELAY);

        return $arRes;
    }

    /**
     * @param $method
     * @param $arQuery
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    private function executeQueryOnce($method, $arQuery)
    {
        $arRes = [];
        $arData = [];

        $rsApp = $this->app;
        $rsApp->addBatchCall(
            $method,
            $arQuery,
            function ($result) use (&$arRes, &$arData) {
                // save first page
                foreach ($result['result'] as $ar) {
                    $arRes[$ar['ID']] = $ar;
                }
                $arData = [
                    'next'  => $result['next'],
                    'total' => $result['total'],
                ];
            }
        );
        $rsApp->processBatchCalls(0, BATCH_DELAY);

        return [
            'data' => $arRes,
            'info' => $arData,
        ];
    }

    /**
     * @return bool
     */
    public function getTokenStatus()
    {
        return $this->btokenRefreshed;
    }

    /**
     * запись текущей авторизации
     */
    private function saveAuth()
    {
        App::Log('Пишем авторизацию для портала ' . $this->app->getDomain());

        $rsPortal = Portal::query()
                          ->where('code', $this->arParams['code'])
                          ->first();
        if ($rsPortal) {
            $rsPortal->fill($this->arParams);
            $bOk = $rsPortal->save();

            if ($bOk) {
                App::Log('Закончили запись');
            } else {
                App::Log('!!! ОШИБКА записи');
            }
        } else {
            App::Log('!!! ОШИБКА записи - портал по коду не найден');
        }
    }

    /**
     * @return \Bitrix24\Bitrix24
     */
    public function getB24(): \Bitrix24\Bitrix24
    {
        return $this->app;
    }

    /**
     * @param $answer
     */
    private function returnJSONResult($answer)
    {
        $response = new JsonResponse($answer);
        $response->send();
        die;
    }
}