<?php

namespace WebNow\Project\Charts;

interface ChartFormat
{
    public function format(array $data): array;
}