<?php

namespace WebNow\Project\Charts;


use Illuminate\Support\Carbon;
use WebNow\Project\Model\Statistic;

class ChartJsFormat implements ChartFormat
{
    private $labelFormat;

    public function __construct($arParams)
    {
        $this->labelFormat = is_string($arParams['labelFormat']) ? trim($arParams['labelFormat']) : $arParams['labelFormat'];

        if (!$this->labelFormat) {
            throw new \Exception('Не указан формат подписей для графиков');
        }
    }

    public function format(array $data): array
    {
        $arLabels = [];
        $arDatasets = [];

        if ($data) {
            // метки во всех сетах одинаковые, поэтому вытащим из первого
            // setlocale(LC_TIME, 'Russian');
            setlocale(LC_TIME, 'ru_RU.UTF-8');
            // Carbon::setLocale('ru');
            foreach (current($data) as $ar) {
                $arLabels[] = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $ar['label_date'])
                                    ->formatLocalized($this->labelFormat);
            }

            // создаем датасеты, последовательно
            foreach ($data as $dataset) {
                $set = array_map(function($item) {
                    return $item['summ'];
                }, $dataset);
                $arDatasets[] = array_values($set);
            }
        }

        return [
            'labels'   => $arLabels,
            'datasets' => $arDatasets,
        ];
    }
}