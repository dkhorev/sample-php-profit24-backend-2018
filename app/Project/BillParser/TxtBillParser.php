<?php

namespace WebNow\Project\BillParser;


use Illuminate\Support\Carbon;

/**
 * 1CClientBankExchange
 *
 * Поддерживаем:
 * ВерсияФормата=1.01
 * ВерсияФормата=1.02
 *
 * Class TxtBillParser
 *
 * @package WebNow\Project\BillParser
 */
class TxtBillParser implements BillParserInterface
{
    protected $data;
    protected $version;

    // поля без которых записи игнорирутся
    protected $requiredFields;
    // конвертация полей из текста в стандартное представление у нас в системе чтобы BillLoader мог его понять
    protected $convertMap;

    // Поля ВерсияФормата=1.01
    private $fieldsV01 = [
        'Номер',
        'ДатаСписано|ДатаПоступило',
        'Сумма',
        // 'Плательщик',
        // 'Получатель',
        'НазначениеПлатежа',
        'ПлательщикИНН',
        'ПолучательИНН',
    ];
    private $mapV01 = [
        'Номер'             => 'bank_id',
        'ДатаСписано'       => 'date_cost',
        'ДатаПоступило'     => 'date_revenue',
        'Сумма'             => 'summ',
        // 'Плательщик'        => 'from_name',
        // 'Получатель'        => 'to_name',
        'ПлательщикИНН'     => 'from_inn',
        'ПолучательИНН'     => 'to_inn',
        'НазначениеПлатежа' => 'desc',
    ];

    // Поля ВерсияФормата=1.02
    private $fieldsV02 = [
        'Номер',
        'ДатаСписано|ДатаПоступило',
        'Сумма',
        // 'Плательщик1',
        // 'Получатель1',
        'НазначениеПлатежа',
        'ПлательщикИНН',
        'ПолучательИНН',
    ];
    private $mapV02 = [
        'Номер'             => 'bank_id',
        'ДатаСписано'       => 'date_cost',
        'ДатаПоступило'     => 'date_revenue',
        'Сумма'             => 'summ',
        // 'Плательщик1'        => 'from_name',
        // 'Получатель1'        => 'to_name',
        'ПлательщикИНН'     => 'from_inn',
        'ПолучательИНН'     => 'to_inn',
        'НазначениеПлатежа' => 'desc',
    ];

    function __construct($data)
    {

        if (!$data) {
            throw new \Exception('Пустой файл');
        }

        $this->data = $data;

        $this->parseEncoding();
        $this->parseVersion();
    }

    /**
     * @throws \Exception
     */
    private function parseVersion()
    {
        $re = '/ВерсияФормата=(?\'version\'\d\.\d{2})/miu';
        preg_match_all($re, $this->data, $matches, PREG_SET_ORDER, 0);
        $matches = $matches[0];

        $this->version = $matches['version'];

        switch ($this->version) {
            case '1.01':
                $this->requiredFields = $this->fieldsV01;
                $this->convertMap = $this->mapV01;
                break;
            case '1.02':
                $this->requiredFields = $this->fieldsV02;
                $this->convertMap = $this->mapV02;
                break;
            default:
                throw new \Exception('Неизвестный формат текстовой выгрузки: ' . $matches['version']);
        }
    }

    /**
     *
     */
    private function parseEncoding()
    {
        $testStr = mb_convert_encoding($this->data, 'utf-8', 'cp-1251');
        // dump($testStr);
        $re = '/Кодировка=(?\'encoding\'\w+)/miu';
        preg_match_all($re, $testStr, $matches, PREG_SET_ORDER, 0);
        $matches = $matches[0];

        if ('Windows' === $matches['encoding']) {
            $this->data = $testStr;
        }
        // dump($this->data);
    }

    /**
     * @return null|string|string[]
     */
    public function getDataRaw()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        $arData = explode('СекцияДокумент=', $this->data);
        // dump($arData);

        $arParsed = [];
        foreach ($arData as $key => $text) {
            $text = preg_replace('/\r\n|\n/', '#SPLIT#', $text);
            $arCur = explode('#SPLIT#', $text);
            $arCur = array_filter(array_map(function ($item) {
                return trim($item);
            }, $arCur));
            // dump($arCur);
            foreach ($arCur as $ar) {
                list($field, $value) = explode('=', $ar);
                if ($value) {
                    $arParsed[$key][$field] = $value;
                }
            }
        }

        $arFiltered = $this->filterByReqParams($arParsed);
        // dump($arFiltered);
        // die;

        return $this->convertFields($arFiltered);
    }

    /**
     * фильтрует вход по наличию необходимых полей в записи, чтобы вычленить полную операцию
     *
     * @param $arData
     *
     * @return array
     */
    private function filterByReqParams($arData)
    {
        $arFieldAll = [];
        $arFieldSplit = [];
        foreach ($this->requiredFields as $field) {
            $curField = explode('|', $field);
            if (count($curField) === 1) {
                $arFieldAll[] = $field;
                continue;
            }
            if (count($curField) > 1) {
                $arFieldSplit = array_merge($arFieldSplit, $curField);
            }
        }
        // dump($arFieldAll);
        // dump($arFieldSplit);

        $arFilters = (count($arFieldSplit)) ? [] : [$arFieldAll];
        foreach ($arFieldSplit as $field) {
            $arFilters[] = array_merge($arFieldAll, [$field]);
        }

        // dump($arFilters);

        return array_filter($arData, function ($item) use ($arFilters) {
            foreach ($arFilters as $arFilter) {
                $bOk = array_has($item, $arFilter);
                if ($bOk) {
                    return $bOk;
                }
            }

            return false;
        });
    }

    /**
     * конвертирует результат в стандартную форму по карте полей
     *
     * @param $arData
     *
     * @return array
     */
    private function convertFields($arData): array
    {
        foreach ($arData as &$ar) {
            $keys = array_keys($ar);
            $values = array_values($ar);

            foreach ($keys as $key => &$fieldName) {
                if ($this->convertMap[$fieldName]) {
                    $fieldName = $this->convertMap[$fieldName];
                } else {
                    unset($keys[$key]);
                    unset($values[$key]);
                }
            }
            // dump($keys);
            // dump($values);
            $ar = array_combine($keys, $values);
        }
        // dump($arData);
        // die;

        return $this->normalize($arData);
    }

    /**
     * нормализуем данные этого формата
     *
     * @param $arData
     *
     * @return array
     */
    private function normalize($arData): array
    {
        return array_values(array_map(function ($item) {
            // чистим описание от двойных пробелов
            $item['desc'] = preg_replace('/\s+/u', ' ', $item['desc']);

            // конвертируем дату
            if ($item['date_revenue']) {
                list($day, $month, $year) = explode('.', $item['date_revenue']);
                $date = Carbon::create($year, $month, $day);
                $item['date_revenue'] = $date->toDateString();
            }
            if ($item['date_cost']) {
                list($day, $month, $year) = explode('.', $item['date_cost']);
                $date = Carbon::create($year, $month, $day);
                $item['date_cost'] = $date->toDateString();
            }

            $item['summ'] = (double)$item['summ'];
            $item['bank_id'] = (int)$item['bank_id'];

            return $item;
        }, $arData));
    }
}