<?php

namespace WebNow\Project\BillParser;


interface BillParserInterface
{
    public function toArray(): array;
}