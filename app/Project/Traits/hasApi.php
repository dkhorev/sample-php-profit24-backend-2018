<?php

namespace WebNow\Project\Traits;

use WebNow\App;

/**
 * для тех контроллеров которые работают по API с фронтом
 *
 * Trait hasApi
 *
 * @package WebNow\Project\Traits
 */
trait hasApi
{
    /* @var \Illuminate\Database\Eloquent\Model $Model */
    protected $Model;
    /* @var \WebNow\Project\Controller\OperationController $Controller */
    protected $Controller;

    /**
     * обработчик аякс запросов
     *
     * @param $arParams
     *
     * @return array
     */
    public function handle($arParams): array
    {
        $modelName = '\WebNow\\Project\\Model\\' . $arParams['model'];
        $controllerName = '\WebNow\\Project\\Controller\\' . $arParams['model'] . 'Controller';
        if (class_exists($modelName)) {
            $this->Model = new $modelName();
            $this->Controller = new $controllerName();
        } else {
            return [
                'error'    => true,
                'msg'      => 'Модель не найдена',
                'msgHuman' => 'Модель не найдена',
            ];
        }

        switch ($arParams['operation']) {
            case 'delete':
                $arRes = $this->destroy($arParams['id']);
                break;

            case 'get':
                $arRes = $this->index();
                break;

            case 'show':
                $arRes = $this->show($arParams['data']);
                break;

            case 'add':
                $arRes = $this->store($arParams['data']);
                break;

            case 'update':
                $arRes = $this->update($arParams['data']);
                break;

            default:
                $arRes = [
                    'error'    => true,
                    'msg'      => 'Api метод не найден',
                    'msgHuman' => 'Api метод не найден',
                ];
        }

        return $arRes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    private function index(): array
    {
        $query = $this->Model::query();
        $query = $this->Controller->applyScopes($query);
        return $query->get()->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $data
     *
     * @return array
     */
    private function store($data): array
    {
        $arValidate = static::validate($data);

        if (true === $arValidate['error']) {
            $arRes = $arValidate;
        } else {
            // дефолтные значения
            $data = static::fillDefault($data);

            $this->Model->fill($data);

            try {
                $this->Model->save();
                $arRes = $this->Model->toArray();
            } catch (\Exception $e) {
                $arRes = [
                    'error'    => true,
                    'msg'      => utf8_encode($e->getMessage()),
                    'msgHuman' => App::ErrorToHuman($e->getMessage()),
                ];
            }
        }

        return $arRes;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     *
     * @return array
     */
    private function update($data): array
    {
        $arValidate = static::validate($data);

        if (true === $arValidate['error']) {
            $arRes = $arValidate;
        } else {
            try {
                $rsEl = $this->Model::query()->findOrNew($data['id']);

                if ($rsEl) {
                    // дефолтные значения
                    $data = static::fillDefault($data);

                    $rsEl->fill($data);
                    $rsEl->save();
                    $arRes = $rsEl->toArray();
                } else {
                    $arRes = [
                        'error'    => true,
                        'msg'      => 'Элемент не найден',
                        'msgHuman' => 'Элемент не найден',
                    ];
                }
            } catch (\Exception $e) {
                $arRes = [
                    'error'    => true,
                    'msg'      => utf8_encode($e->getMessage()),
                    'msgHuman' => App::ErrorToHuman($e->getMessage()) ?: utf8_encode($e->getMessage()),
                ];
            }
        }

        return $arRes;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return array
     */
    private function destroy($id): array
    {
        $arRes = [];
        try {
            $rsEl = $this->Model::query()->find($id);

            if ($rsEl) {
                $rsEl->delete();
            }
        } catch (\Exception $e) {
            $arRes = [
                'error'    => true,
                'msg'      => utf8_encode($e->getMessage()),
                'msgHuman' => App::ErrorToHuman($e->getMessage()),
            ];
        }

        return $arRes;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function show($data)
    {
        try {
            $arRes = $this->Model::query()->where($data['field'], $data['value'])->first();
            if ($arRes) {
                $arRes = $arRes->toArray();
            } else {
                return [];
            }
        } catch (\Exception $e) {
            $arRes = [
                'error'    => true,
                'msg'      => utf8_encode($e->getMessage()),
                'msgHuman' => App::ErrorToHuman($e->getMessage()),
            ];
        }

        return $arRes;
    }
}