<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Model;
use WebNow\Project\Controller\OptionController;

/**
 * Class Option
 * ORM для работы с опциями
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Option extends Model
{
    protected $fillable = ['key', 'value'];
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @param $key
     */
    public static function del($key)
    {
        $arOpt = static::query()->where('key', $key)->first();

        if ($arOpt) {
            $arOpt = $arOpt->toArray();
        }
        // dump($arOpt);

        if ($arOpt['id']) {
            $arRes = (new OptionController)->handle([
                'model'     => 'Option',
                'operation' => 'delete',
                'id'        => $arOpt['id'],
            ]);
            // dump($arRes);
        }
    }

    /**
     * @param $key
     * @param $value
     *
     * @return bool
     */
    public static function set($key, $value)
    {
        if (is_array($value)) {
            $value = serialize($value);
        }
        // dump($value);

        $arOpt = static::query()->firstOrNew(['key' => $key])->toArray();
        // dump($arOpt);

        $arRes = (new OptionController)->handle([
            'model'     => 'Option',
            'operation' => 'update',
            'data'      => [
                'id'    => $arOpt['id'],
                'key'   => $key,
                'value' => $value,
            ],
        ]);
        // dump($arRes);

        if ($arRes['id']) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     *
     * @return string
     */
    public static function get($key)
    {
        $arRes = (new OptionController)->handle([
            'model'     => 'Option',
            'operation' => 'show',
            'data'      => [
                'field' => 'key',
                'value' => $key,
            ],
        ]);

        $is_array = unserialize($arRes['value']);
        if ($is_array !== false) {
            $arRes['value'] = $is_array;
        }

        return $arRes['value'] ? : null;
    }
}