<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Paytype
 * ORM для работы с типами платежей
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Paytype extends Model
{
    const TYPE_LEAD = 1; // Лид в работе
    const TYPE_OVER = 2; // Перенос
    const TYPE_REVENUE = 3; // Приход
    const TYPE_COST = 4; // Расход
    const TYPE_EXPECT_REVENUE = 5; // Ожидаемый приход
    const TYPE_EXPECT_COST = 6; // Ожидаемый расход
    const TYPE_LEAD_LOST = 7; // Лид проигран

    protected $fillable = ['name', 'type'];
    protected $hidden = ['created_at', 'updated_at'];
}