<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MonthlyTask
 * ORM для работы с Ежемесячными операциями
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 *
 * @method static Builder byMonthYear($month, $year)
 */
class MonthlyTask extends Model
{
    protected $fillable = ['month', 'year', 'open_bills', 'open_leads', 'wage_50', 'wage_100', 'fixed_costs'];
    protected $hidden = ['created_at', 'updated_at', 'id', 'month', 'year'];

    /**
     * @param Builder $query
     * @param mixed $month
     * @param mixed $year
     * @return Builder
     */
    public function scopeByMonthYear($query, $month, $year)
    {
        return $query->where('month', $month)->where('year', $year);
    }
}