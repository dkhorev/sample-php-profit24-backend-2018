<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Contractor
 * ORM для работы с Контрагентами
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 *
 * @method static Builder myCompanies()
 * @method static Builder notMyCompanies()
 */
class Contractor extends Model
{
    protected $fillable = ['name', 'b24_id', 'is_company', 'is_my', 'inn'];
    protected $hidden = ['created_at', 'updated_at', 'code'];

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeMyCompanies($query)
    {
        return $query->where('is_my', true);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotMyCompanies($query)
    {
        return $query->where('is_my', false);
    }
}