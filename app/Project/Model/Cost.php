<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cost
 * ORM для работы с расходами
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Cost extends Model
{
    protected $fillable = ['name', 'price'];
    protected $hidden = ['created_at', 'updated_at'];
}