<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employee
 * ORM для работы с сотрудниками
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Employee extends Model
{
    protected $fillable = ['name', 'lastname', 'card_number', 'wage_real', 'wage', 'is_remote', 'b24_id', 'is_active', 'departments'];
    protected $hidden = ['created_at', 'updated_at'];

    // public function operations()
    // {
    //     return $this->hasMany(Operation::class)->get()->toArray();
    // }

    /**
     * магический метод - на фронт отдаем другое представление
     *
     * @param $wage
     *
     * @return double
     */
    public function getWageAttribute($wage){
        return $wage ?: 0;
    }

    /**
     * магический метод - на фронт отдаем другое представление
     *
     * @param $wage
     *
     * @return double
     */
    public function getWageRealAttribute($wage){
        return $wage ?: 0;
    }
}