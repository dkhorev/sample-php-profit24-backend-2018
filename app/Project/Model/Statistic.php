<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Statistic
 * ORM для работы со статистикой
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Statistic extends Model
{
    const TYPE_WEEK = 1; // недельная стата
    const TYPE_MONTH = 2; // месячная
    const TYPE_DAY = 3; // ежедневная
    const LABEL_DATE_FORMAT = 'Y-m-d'; // стандартынй формат записи в поле label_date

    protected $fillable = ['year', 'type', 'label_date', 'metric', 'summ'];
    protected $hidden = ['created_at', 'updated_at'];
}