<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use WebNow\App;
use WebNow\Project\Controller\PaytypeController;

/**
 * Class Operation
 * ORM для работы с Транзакциями
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 *
 * @method static Builder byRequestMonthYear()
 * @method static Builder byMonthYear($year, $month)
 * @method static Builder byPreviousMonthYear($month, $year)
 * @method static Builder byPaytype($month, $year)
 */
class Operation extends Model
{
    protected $fillable = [
        'paytype_id',
        'firm_id',
        'contractor_id',
        'employee_id',
        'manager_id',
        'summ',
        'date',
        'b24_id',
        'bank_id',
        'account',
        'comment',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    protected $casts = ['created_at', 'updated_at', 'date'];

    // protected $dates = ['date'];

    protected $appends = ['is_lead'];

    /**
     * Get lead flag
     *
     * @return bool
     */
    public function getIsLeadAttribute()
    {
        return in_array($this->attributes['paytype_id'], PaytypeController::getAllLeadId());
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /* @deprecated: т.к. приходилось все время его отключать при работе с данными (query->withoutGlobalScopes())
         * фильтрация по датам в OperationController->applyScopes
         */
        // организуем глобальную фильтрацию по датам
        // static::addGlobalScope('date', function (Builder $builder) {
        //     $request = App::Request();
        //     $month = $request->get('month') ? : date('m');
        //     $year = $request->get('year') ? : date('Y');
        //
        //     $dateFrom = Carbon::create($year, $month, 1, 0, 0, 0);
        //     $dateTo = $dateFrom->copy()->addMonth();
        //
        //     $builder
        //         ->where('date', '>=', $dateFrom->toDateString())
        //         ->where('date', '<', $dateTo->toDateString());
        // });
    }

    /**
     * магический метод - на фронт отдаем другое представление даты
     *
     * @param $date
     *
     * @return string
     */
    public function getDateAttribute($date)
    {
        return $this->asDateTime($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeByRequestMonthYear($query)
    {
        $request = App::Request();
        $month = $request->get('month') ? : date('m');
        $year = $request->get('year') ? : date('Y');

        $dateFrom = Carbon::create($year, $month, 1, 0, 0, 0);
        $dateTo = $dateFrom->copy()->addMonth();

        return $query->where('date', '>=', $dateFrom->toDateString())
                     ->where('date', '<', $dateTo->toDateString());
    }

    /**
     * @param Builder $query
     * @param         $year
     * @param         $month
     *
     * @return Builder
     */
    public function scopeByMonthYear($query, $year, $month)
    {
        $dateFrom = Carbon::create($year, $month, 1, 0, 0, 0);
        $dateTo = $dateFrom->copy()->addMonth();

        return $query->where('date', '>=', $dateFrom->toDateString())
                     ->where('date', '<', $dateTo->toDateString());
    }

    /**
     * @param Builder $query
     * @param mixed   $month
     * @param mixed   $year
     *
     * @return Builder
     */
    public function scopeByPreviousMonthYear($query, $month, $year)
    {
        $dateTo = Carbon::create($year, $month, 1, 0, 0, 0);
        $dateFrom = $dateTo->copy()->subMonth();
        // print_r($dateFrom);
        // print_r($dateTo);

        return $query->where('date', '>=', $dateFrom->toDateString())
                     ->where('date', '<', $dateTo->toDateString());
    }

    /**
     * @param Builder $query
     * @param mixed   $arTypeId
     *
     * @return Builder
     */
    public function scopeByPaytype($query, $arTypeId)
    {
        if (!is_array($arTypeId)) {
            $arTypeId = [$arTypeId];
        }

        return $query->whereIn('paytype_id', $arTypeId);
    }
}