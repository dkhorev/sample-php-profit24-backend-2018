<?php

namespace WebNow\Project\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use WebNow\Helpers;

/**
 * Class B24Status
 * ORM для работы со статусами счетов/лидов в Б24
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 *
 * @method static Builder getByEntity($entity)
 */
class B24Status extends Model
{
    // название сущностей в Б24
    const STATUS_INVOICE = 'INVOICE_STATUS';
    const STATUS_LEAD = 'STATUS';

    // логические группы статусов у нас в приложении
    const GROUP_NEW = 1; // новый
    const GROUP_PROGRESS = 2; // в процессе
    const GROUP_SUCCESS = 3; // удача
    const GROUP_FAIL = 4; // провал

    // хардкод статусы из Б24 по счетам
    const INVOICE_NEW = 'N';
    const INVOICE_PROGRESS = 'S';
    const INVOICE_SUCCESS = 'P';
    const INVOICE_FAIL = 'D';
    // хардкод статусы из Б24 по лидам
    const LEAD_NEW = 'NEW';
    const LEAD_PROGRESS = 'IN_PROCESS';
    const LEAD_SUCCESS = 'CONVERTED';
    const LEAD_FAIL = 'JUNK';

    protected static $statusMap;

    protected $fillable = ['b24_id', 'entity', 'status_id', 'group'];
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @param Builder $query
     *
     * @param         $entity
     *
     * @return Builder
     */
    public function scopeGetByEntity($query, $entity)
    {
        if (!is_array($entity)) {
            $entity = [$entity];
        }

        return $query->whereIn('entity', $entity);
    }

    /**
     * @param $entity
     * @param $type
     *
     * @return mixed
     */
    public static function getStatuses($entity, $type = [])
    {
        if (!is_array($type)) {
            $type = [$type];
        }

        $md5 = md5($entity . implode('', $type));

        if (!static::$statusMap[$md5]) {
            $arRes = B24Status::getByEntity($entity)->get()->toArray();
            $arRes = array_filter($arRes, function($item) use ($type) {
                return in_array($item['group'], $type);
            });
            $arRes = Helpers::extractByField('status_id', $arRes);
            static::$statusMap[$md5] = $arRes;
        }

        return static::$statusMap[$md5];
    }

    /**
     * @param $arData
     */
    public static function updateByEntity($arData)
    {
        $coll = collect($arData)->groupBy('ENTITY_ID');

        foreach ($coll as $entity => $data) {
            $data = collect($data);
            $arRes = $data->map(function ($item) {
                return [
                    'b24_id'    => $item['ID'],
                    'entity'    => $item['ENTITY_ID'],
                    'status_id' => $item['STATUS_ID'],
                ];
            })->toArray();

            $arRes = static::fillGroups($arRes, $entity);
            // dump($arRes);

            foreach ($arRes as $ar) {
                B24Status::query()->updateOrCreate(['b24_id' => $ar['b24_id']], $ar);
            }
        }
    }

    /**
     * @param $data
     * @param $entity
     *
     * @return mixed
     */
    private static function fillGroups($data, $entity)
    {
        switch ($entity) {
            case B24Status::STATUS_INVOICE:
                return static::fillInvoiceGroups($data);
            case B24Status::STATUS_LEAD:
                return static::fillLeadGroups($data);
        }

        return $data;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private static function fillInvoiceGroups($data)
    {
        // начинается всегда с НОВОГО
        $nCurGroup = B24Status::GROUP_PROGRESS;
        foreach ($data as &$status) {
            switch ($status['status_id']) {
                case B24Status::INVOICE_SUCCESS:
                    $nCurGroup = B24Status::GROUP_SUCCESS;
                    break;
                case B24Status::INVOICE_FAIL:
                    $nCurGroup = B24Status::GROUP_FAIL;
                    break;
            }
            $status['group'] = $nCurGroup;
        }

        return $data;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private static function fillLeadGroups($data)
    {
        // начинается всегда с НОВОГО
        $nCurGroup = B24Status::GROUP_PROGRESS;
        foreach ($data as &$status) {
            switch ($status['status_id']) {
                case B24Status::LEAD_SUCCESS:
                    $nCurGroup = B24Status::GROUP_SUCCESS;
                    break;
                case B24Status::LEAD_FAIL:
                    $nCurGroup = B24Status::GROUP_FAIL;
                    break;
            }
            $status['group'] = $nCurGroup;
        }

        return $data;
    }
}