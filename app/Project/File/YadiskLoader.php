<?php


namespace WebNow\Project\File;

use DirectoryIterator;
use WebNow\Helpers;
use WebNow\Project\Model\Option;
use ZipArchive;


/**
 * загрузка архива с ЯД и распаковка, отбор файлов которые обработали
 *
 * Class YadiskLoader
 *
 * @package WebNow\Project\File
 */
class YadiskLoader
{
    protected $db;
    protected $portal;
    protected $params;
    private $url;
    private $path;
    public static $OPT_NAME = 'YA.PARSED.FILES';

    public function __construct($db, $portal, $params)
    {
        $this->db = $db;
        $this->portal = $portal;
        $this->params = $params;
        $this->url = "https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=" . $params['file'];

        // создаем папки автоматом
        $this->path = Helpers::getBillsPath($this->portal);
        if (!is_dir($this->path)) {
            mkdir($this->path, 0700, true);
        }
    }

    /**
     * @return array
     */
    public function getNewFiles()
    {
        $fileZip = $this->downloadFile();
        // dump($fileZip);

        $this->unpackZip($fileZip);

        $arFiles = $this->getAllFiles($this->path);
        $arParsed = $this->getParsedFiles();
        // dump($arFiles);
        // dump($arParsed);

        // отделим разобранные файлы от новых
        if ($arParsed) {
            foreach ($arFiles as $key => $file) {
                $hash = md5($file);
                if (in_array($hash, $arParsed)) {
                    unset($arFiles[$key]);
                }
            }
        }

        return $arFiles;
    }


    /**
     * @return string
     */
    private function downloadFile()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST           => 0,
            CURLOPT_HEADER         => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $this->url,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, 1);

        curl_setopt_array($curl, [
            CURLOPT_HEADER         => 0,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_FAILONERROR    => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_URL            => $result['href'],
        ]);

        $archFile = curl_exec($curl);
        curl_close($curl);

        if (!$archFile) {
            throw new \Exception('Файл не существует');
        }

        $archFileName = $this->path . "Archive.zip";
        file_put_contents($archFileName, $archFile);

        return $archFileName;
    }

    /**
     * @param $fileZip
     */
    private function unpackZip($fileZip)
    {
        $zipObject = new ZipArchive();
        if (true === $zipObject->open($fileZip)) {
            $zipObject->extractTo($this->path);
            // unlink($fileZip);
        }
    }

    /**
     * @param $path
     *
     * @return array
     */
    private function getAllFiles($path)
    {
        $arRes = [];
        foreach (new DirectoryIterator($path) as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            if ($fileInfo->isDir()) {
                $arRes = array_merge($arRes, $this->getAllFiles($path . DIRECTORY_SEPARATOR . $fileInfo->getFilename()));
            } else {
                if ('txt' === $fileInfo->getExtension()) {
                    // $arRes[] = [
                    //     'file' => $fileInfo->getFilename(),
                    //     'dir' => $fileInfo->getPath(),
                    // ];
                    $arRes[] = $fileInfo->getPathname();
                }
            }
        }

        return $arRes;
    }

    /**
     * @return mixed
     */
    private function getParsedFiles()
    {
        return Option::get(static::$OPT_NAME);
    }

    /**
     * @param $arFiles
     *
     * @return bool
     */
    public function updateParsed($arFiles)
    {
        $arParsed = $this->getParsedFiles();
        $arParsed = $arParsed ?: [];
        // dump($arParsed);

        $arNewParsed = array_filter($arFiles, function ($item) {
           return false ===  $item['error'];
        });
        $arNewParsed = Helpers::extractByField('filePath', $arNewParsed);
        $arNewParsed = array_map(function ($item) {
            return md5($item);
        }, $arNewParsed);
        // dump($arNewParsed);

        $arResult = $arParsed;
        foreach ($arNewParsed as $md5) {
            $arResult[] = $md5;
        }
        $arResult = array_unique(array_filter($arResult));
        // dump($arResult);

        return Option::set(static::$OPT_NAME, $arResult);
    }
}