<?php

namespace WebNow\Project\Event;

use Bitrix24\Presets\Event\Event;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as Capsule;
use WebNow\App;
use WebNow\B24App;
use WebNow\Portal;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Data\PortalDataLoader;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Operation;

/**
 * отвечает за обработку событий приходящих из Б24
 *
 * Class B24Event
 *
 * @package WebNow\Project\Event
 */
class B24Event
{
    protected $db;
    protected $event;
    protected $data;
    protected $entity_id;
    protected $auth;
    protected $portal;
    protected $b24;

    protected static $ERR_CONSTRAINT = 23000;

    function __construct(Capsule $db, array $arParams)
    {
        $this->db = $db;
        $this->event = $arParams['event'];
        $this->data = $arParams['data'];
        $this->entity_id = $arParams['data']['FIELDS']['ID'];
        $this->auth = $arParams['auth'];

        if (!$this->auth['member_id'] || !$this->auth['application_token']) {
            $this->throwError('no member_id and/or application_token');
        }

        $this->authPortal();
        $this->b24 = new B24App($this->portal);

        // добавим новое соединение с БД клиента
        PortalController::addConnection($this->portal);
    }

    /**
     * авторизуем портал по ключу из события
     *
     * @throws \Exception
     */
    private function authPortal()
    {
        switch ($this->event) {
            // при установке еще нет application_token
            // https://dev.1c-bitrix.ru/rest_help/general/events/event_safe.php
            case Event::ON_APP_INSTALL:
                $this->portal = PortalController::findByMemberId($this->auth['member_id']);
                break;
            default:
                $this->portal = PortalController::findByAppToken($this->auth['application_token']);
                break;
        }

        if (!$this->portal) {
            App::EventLog('Ошибка авторизации: портал не найден');
            $this->throwError('Portal not found wrong application_token and/or member_id');
        }
    }

    /**
     * распеределяет события в свои обработчики
     *
     * @throws \Exception
     */
    public function handle()
    {
        App::EventLog('FOR entity_id = ' . $this->entity_id);

        $bOk = false;
        switch ($this->event) {
            // записать application_token для дальнейшей идентификации приложения
            case Event::ON_APP_INSTALL:
                $bOk = $this->saveAppToken();
                break;

            // удаление приложения
            case Event::ON_APP_UNINSTALL:
                break;

            case Event::ON_CRM_LEAD_ADD:
                $bOk = $this->addLead();
                break;

            case Event::ON_CRM_LEAD_UPDATE:
                $bOk = $this->updateLead();
                break;

            case Event::ON_CRM_LEAD_DELETE:
                $bOk = $this->deleteLead();
                break;

            // изменения в контакте crm
            case Event::ON_CRM_CONTACT_ADD:
            case Event::ON_CRM_CONTACT_UPDATE:
                $bOk = $this->updateContact();
                break;

            // удаление контакта crm
            case Event::ON_CRM_CONTACT_DELETE:
                $bOk = $this->deleteContact();
                break;

            // изменения в компании crm
            case Event::ON_CRM_COMPANY_ADD:
            case Event::ON_CRM_COMPANY_UPDATE:
                $bOk = $this->updateCompany();
                break;

            // удаление компании crm
            case Event::ON_CRM_COMPANY_DELETE:
                $bOk = $this->deleteCompany();
                break;

            // добавление в счете crm
            case 'ONCRMINVOICEADD':
                $bOk = $this->addInvoice();
                break;

            // изменения в счете crm
            case 'ONCRMINVOICEUPDATE':
                $bOk = $this->updateInvoice();
                break;

            // изменения в статусе счета crm
            // приходит когда кликают на полоски статуса счета
            // приходит перед обновлением счета ONCRMINVOICEUPDATE (баг б24?)
            case 'ONCRMINVOICESETSTATUS':
                $bOk = $this->updateInvoiceStatus();
                break;

            // удаление счета в crm
            case 'ONCRMINVOICEDELETE':
                $bOk = $this->deleteInvoice();
                break;


        }

        if (false === $bOk) {
            $this->throwError('Ошибка обрабочика события');
        }
    }

    /**
     * добавляем/обновляем Компанию или Контакт по ID (у нас это одна сущность Контрагент)
     * добавляем Компанию с флагом "моя" в Каналы
     *
     * @return bool
     */
    private function updateContact()
    {
        // тянем контакт из Б24 если это не компания
        $arNewCompany = $this->b24->getContactsById($this->entity_id);

        if ($arNewCompany[$this->entity_id]) {
            $rsDataLoader = new PortalDataLoader(App::Capsule(), $this->portal);
            $arReqs = $rsDataLoader->loadRequisites($this->entity_id, 'contact');
            $arRes = $rsDataLoader->storeContractorsContacts($arNewCompany, $arReqs);

            return $arRes['result'];
        }

        return false;
    }

    /**
     * добавляем/обновляем Компанию по ID (у нас это Контрагент)
     * добавляем Компанию с флагом "моя" в Каналы
     *
     * @return bool
     */
    private function updateCompany()
    {
        // тянем компанию из Б24
        $arNewCompany = $this->b24->getCompaniesById($this->entity_id);

        if ($arNewCompany[$this->entity_id]) {
            $rsDataLoader = new PortalDataLoader(App::Capsule(), $this->portal);
            $arReqs = $rsDataLoader->loadRequisites($this->entity_id);

            if ('Y' === $arNewCompany[$this->entity_id]['IS_MY_COMPANY']) {
                $arRes = $rsDataLoader->storeFirms($arNewCompany, $arReqs);
            } else {
                $arRes = $rsDataLoader->storeContractors($arNewCompany, $arReqs);
            }

            return $arRes['result'];
        }

        return false;
    }

    /**
     * удаляем из системы сущность Контрагент (Контакт из Б24)
     * в б24 это все Компании и Контакты
     *
     * @return bool|null
     * @throws \Exception
     */
    private function deleteContact()
    {
        if (!$this->entity_id) {
            $this->throwError('deleteContact: No entity_id!');
        }

        /* @var Contractor $curEntity */
        $curEntity = Contractor::query()
                               ->where('b24_id', $this->entity_id)
                               ->where('is_company', false)
                               ->where('is_my', false)
                               ->get()->first();

        if ($curEntity) {
            return $this->deleteWithConstraints($curEntity);
        }

        // если нет в системе то ок тоже
        return true;
    }

    /**
     * удаляем из системы сущность Firm или Contactor (Company из Б24)
     * в б24 это все Компании и Контакты
     *
     * @return bool|null
     * @throws \Exception
     */
    private function deleteCompany()
    {
        if (!$this->entity_id) {
            $this->throwError('deleteCompany: No entity_id!');
        }

        // вытянуть сущность из нашей системы
        /* @var Contractor $curEntity */
        $curEntity = Contractor::query()
                                ->where('b24_id', $this->entity_id)
                                ->where('is_company', true)
                                ->get()->first();

        if ($curEntity) {
            return $this->deleteWithConstraints($curEntity);
        }

        // если нет в системе то ок тоже
        return true;
    }

    /**
     * @param Contractor $curEntity
     *
     * @return bool|mixed
     * @throws \Throwable
     */
    private function deleteWithConstraints($curEntity)
    {
        // пробуем удалить сущность сразу
        try {
            return $curEntity->delete();
        } catch (\Exception $e) {
            // исключение с кодом 23000 значит сущность используется в Операциях
            // удалить сначала связи из операций
            if (static::$ERR_CONSTRAINT === (int)$e->getCode()) {
                App::EventLog('Поймали исключение: есть зависимые операции с этой компанией');

                $entity_id = $curEntity->getAttribute('id');

                $rsOps = Operation::query()
                                  ->where('firm_id', $entity_id)
                                  ->orWhere('contractor_id', $entity_id)
                                  ->get();
                // dump($rsOps);

                if (count($rsOps)) {
                    $db = $this->db::connection();
                    $bOk = $db->transaction(function () use ($db, $rsOps, $curEntity, $entity_id) {
                        /* @var Operation $ar */
                        foreach ($rsOps as $ar) {
                            if ($entity_id === $ar->getAttribute('firm_id')) {
                                $ar->setAttribute('firm_id', null)->save();
                            }
                            if ($entity_id === $ar->getAttribute('contractor_id')) {
                                $ar->setAttribute('contractor_id', null)->save();
                            }
                        }
                        App::EventLog('Удалили все привязки компании/контакта в операциях.');

                        return $curEntity->delete();
                    });

                    return $bOk;
                }
            }

            return false;
        }
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    private function deleteInvoice()
    {
        if (!$this->entity_id) {
            $this->throwError('deleteInvoice: No entity_id!');
        }

        // вытянуть сущность из нашей системы
        /* @var Operation $curInvoice */
        $curInvoice = Operation::query()
                               ->where('b24_id', $this->entity_id)
                               ->get()
                               ->last();

        if ($curInvoice) {
            // todo удалить историю операции сначала (когда она будет)
            return $curInvoice->delete();
        }

        // если счета у нас нет в системе то все ок тоже
        return true;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function addInvoice()
    {
        if (!$this->entity_id) {
            $this->throwError('updateInvoice: No entity_id!');
        }

        $arRes = (new PortalDataLoader(App::Capsule(), $this->portal))->runForInvoices($this->entity_id);

        return $arRes['result'];
    }

    /**
     * нужно удалять крансые счета
     * @return mixed
     * @throws \Exception
     */
    private function updateInvoice()
    {
        if (!$this->entity_id) {
            $this->throwError('updateInvoice: No entity_id!');
        }

        // вытянуть данные по сделке
        $arNewInvoice = $this->b24->getInvoicesById($this->entity_id, $bAnyStatus = true);
        $newStatus = $arNewInvoice[$this->entity_id]['STATUS_ID'];
        $nNewPaytypeId = PaytypeController::getInvoicePayTypeId($newStatus);

        App::EventLog('Update Invoice from B24 = ');
        App::EventLog($arNewInvoice);

        if (false === $nNewPaytypeId) {
            App::EventLog('"Red invoice"  = deleting');
            // вытянуть сущность из нашей системы
            /* @var Operation $curInvoice */
            $curInvoice = Operation::query()
                                   ->where('b24_id', $this->entity_id)
                                   ->get()
                                   ->last(); // счета могут быть перенесены с прошлого месяца, поэтому тут нам нужен последний
            if ($curInvoice) {
                return $curInvoice->delete();
            }
        } else {
            // обновляем счет
            $arRes = (new PortalDataLoader(App::Capsule(), $this->portal))->storeOperations($arNewInvoice);
            // dump($arRes);
            return $arRes['result'];

        }

        return true;
    }

    /**
     * нужно удалять крансые счета
     * @return mixed
     * @throws \Exception
     */
    private function updateInvoiceStatus()
    {
        if (!$this->entity_id) {
            $this->throwError('updateInvoiceStatus: No entity_id!');
        }

        // вытянуть данные по сделке
        $arNewInvoice = $this->b24->getInvoicesById($this->entity_id, $bAnyStatus = true);
        $arNewInvoice = $arNewInvoice[$this->entity_id];
        $nNewPaytypeId = PaytypeController::getInvoicePayTypeId($arNewInvoice['STATUS_ID']);

        // вытянуть сущность из нашей системы
        /* @var Operation $curInvoice */
        $curInvoice = Operation::query()
                               ->where('b24_id', $this->entity_id)
                               ->get()
                               ->last(); // счета могут быть перенесены с прошлого месяца, поэтому тут нам нужен последний

        App::EventLog('Update Invoice Status from B24 = ');
        App::EventLog($arNewInvoice);
        App::EventLog('Old status = ' . $curInvoice->getAttribute('paytype_id'));
        App::EventLog('New status = ' . $nNewPaytypeId);

        if (false === $nNewPaytypeId) {
            App::EventLog('"Red invoice"  = deleting');
            return $curInvoice->delete();
        } else {
            // обновляем только дату и тип платежа
            $curInvoice->setAttribute('paytype_id', $nNewPaytypeId);
            $curInvoice->setAttribute('date', $arNewInvoice['DATE_PAYED'] ?: $arNewInvoice['DATE_BILL']);
            return $curInvoice->save();
        }
    }

    /**
     * save application_token
     *
     * @return bool
     */
    private function saveAppToken()
    {
        $rsPortal = Portal::query()->find($this->portal['id']);
        $rsPortal->setAttribute('application_token', $this->auth['application_token']);

        return $rsPortal->save();
    }

    /**
     * @param string $msg
     *
     * @throws \Exception
     */
    private function throwError(string $msg = '')
    {
        throw new \Exception($msg);
    }

    /**
     * добавит/обновит операцию по Лиду из Б24
     *
     * @return bool
     * @throws \Exception
     */
    private function addLead()
    {
        if (!$this->entity_id) {
            $this->throwError('addLead: No entity_id!');
        }

        $arRes = (new PortalDataLoader(App::Capsule(), $this->portal))->runForLeads($this->entity_id);

        if (false === $arRes['result']) {
            $this->throwError($arRes['error']);
        }

        return $arRes['result'];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function updateLead()
    {
        if (!$this->entity_id) {
            $this->throwError('updateLead: No entity_id!');
        }

        // вытянуть данные по лиду
        $arNewLead = $this->b24->getLeadsById($this->entity_id, $bAnyStatus = true);
        $sNewStatus = $arNewLead[$this->entity_id]['STATUS_ID'];
        // dump($arNewLead);
        // dump($sNewStatus);

        if (!$sNewStatus) {
            App::EventLog('Ошибка: Lead пришел без статуса!');
            return false;
        }

        $nNewPaytypeId = PaytypeController::getLeadPayTypeId($sNewStatus);
        // dump($nNewPaytypeId);
        if (false === $nNewPaytypeId) {
            App::EventLog('Green[CONVERTED] Lead = deleting from app');
            // вытянуть сущность из нашей системы
            /* @var Operation $curLead */
            $curLead = Operation::query()
                                ->where('b24_id', $this->entity_id)
                                ->get()
                                ->last();
            if ($curLead) {
                App::EventLog('Old status = ' . $curLead->getAttribute('paytype_id'));
                App::EventLog('New status = ' . $nNewPaytypeId);
                App::EventLog('New status text = ' . $sNewStatus);

                return $curLead->delete();
            }
        } else {
            // обновляем лид
            $arRes = (new PortalDataLoader(App::Capsule(), $this->portal))->storeLeads($arNewLead);
            // dump($arRes);
            return $arRes['result'];
        }

        // если в системе нет Лида то тоже все ок
        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function deleteLead()
    {
        if (!$this->entity_id) {
            $this->throwError('deleteLead: No entity_id!');
        }

        // вытянуть сущность из нашей системы
        /* @var Operation $curLead */
        $curLead = Operation::query()
                            ->whereIn('paytype_id', PaytypeController::getAllLeadId())
                            ->where('b24_id', $this->entity_id)
                            ->get()
                            ->last();

        if ($curLead) {
            return $curLead->delete();
        }

        // если у нас нет в системе то все ок тоже
        return true;
    }
}