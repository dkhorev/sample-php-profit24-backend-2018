<?php

namespace WebNow\Project\Data;


use Carbon\Carbon;
use WebNow\App;
use WebNow\B24App;
use WebNow\Helpers;
use WebNow\Project\Controller\OperationController;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Operation;

class BillLoader
{
    protected $data;
    protected $invMap;
    private $arPortal;
    /* @var \WebNow\B24App $b24 */
    private $b24;
    private $updateB24;

    // карта соответствия INN => id компании/контакта у нас в базе
    protected $arInnMap;

    // типы платежей которые назначаются по умолчанию
    protected $idRevenue;
    protected $idCost;

    protected static $TAG_COMMENT = 'Загружено из Банк-Клиента';


    public function __construct($data, $arPortal, $invMap = [])
    {
        $this->data = $data;
        $this->invMap = static::convertForRegExp($invMap);
        $this->arPortal = $arPortal;

        $arCtr = $this->getContractors();
        foreach ($arCtr as $ar) {
            if ($ar['inn']) {
                $arInn = explode(',', $ar['inn']);
                foreach ($arInn as $val) {
                    $this->arInnMap[$val] = $ar['id'];
                }
            }
        }

        $this->idCost = PaytypeController::getFirstCost()['id'];
        $this->idRevenue = PaytypeController::getFirstRevenue()['id'];

        if (!$this->idCost || !$this->idRevenue) {
            throw new \Exception('Нет необходимого типа платежа (расхода или прихода)');
        }
    }

    /**
     * надо убирать пробелы и тире, оставлять только цифры и буквы
     *
     * @param $invMap
     *
     * @return mixed
     */
    public static function convertForRegExp($invMap)
    {
        if (!is_array($invMap)) {
            $invMap = [$invMap];
        }

        foreach ($invMap as $id => &$text) {
            // удалим двойные+ пробелы
            $text = preg_replace('/\s+/uis', ' ', $text);
            // вырезаем все даты из комментов
            $text = preg_replace('/(\d{2}[.,\/г])(\d{2}[.,\/г])(\d{4}[.,\/г ]|\d{2}[.,\/г ])/uis', '', $text);
            // это банковсие и личне счета которые могут быть в комменте
            $text = preg_replace('/\d{20}|\d{11}/uis', '', $text);
            $text = preg_replace('/(\s+)|[-_:;]/uis', '', $text);
            $text = str_replace('\\', '', $text);
            $text = str_replace('/', '', $text);
        }
        return $invMap;
    }

    /**
     * @param bool $updateB24
     *
     * @return array
     */
    public function load($updateB24 = true)
    {
        $this->updateB24 = $updateB24;
        list($arRevenue, $arCost) = $this->parseBills();

        // App::Log('Прочитанные расходы:');
        // App::Log($arCost);
        // App::Log('Прочитанные доходы:');
        // App::Log($arRevenue);

        // dump($arCost);
        // dump($arRevenue);
        // die;
        $arResCost = [];
        $arResRevenue = [];

        // расходы мы просто пищем в приложение (если там их нет)
        if (count($arCost)) {
            $arResCost = $this->addCostOperations($arCost);
        }
        // dump($arResCost);

        // по доходам мы обновляем статус счета в Б24 (в приложении обновится счет по событию)
        if (count($arRevenue)) {
            $arResRevenue = $this->addRevenueOperations($arRevenue);
        }

        // dump($arResRevenue);
        // die;

        return [
            'error'                       => false,
            'count_cost'                  => $arResCost['count'] ? : 0,
            'count_revenue_insert'        => $arResRevenue['count_revenue_insert'] ? : 0,
            'count_revenue_update_in_b24' => $arResRevenue['count_revenue_update_in_b24'] ? : 0,
        ];
    }

    /**
     * доходы мы сопоставляем отчасти с ЦРМ, отчасти пишем в базу
     * выбираем все найденные номера счетов, суммы и компании - делаем гет запрос в Б24 и парсим
     * все остальное что не определилось по Б24 - пишем в приложение как операции
     *
     * @param $arRevenue
     *
     * @return array
     */
    private function addRevenueOperations($arRevenue)
    {
        $arData = $arRevenue;
        // dump($arData);

        // разбиваем все приходы на те что нашли в Б24 и неизвестные
        $arUpdInvoices = [];
        $arInsertRevenue = [];
        foreach ($arData as $ar) {
            if ($ar['b24_id']) {
                $arUpdInvoices[] = $ar;
            } else {
                $ar['b24_id'] = null;
                $ar['account'] = null;
                $arInsertRevenue[] = $ar;
            }
        }
        // dump($arUpdInvoices);
        // dump($arInsertRevenue);

        // СНАЧАЛА ВНЕСЕМ ВСЕ НОВЫЕ ОПЕРАЦИИ с проверкой на дубли
        $arResIns = [];
        if (count($arInsertRevenue)) {
            $arResIns = $this->addNewRevenueOps($arInsertRevenue);
        }

        // ТЕПЕРЬ ОБНОВИМ В Б24 счета по нашим данным
        $arResUpd = [];
        if (count($arUpdInvoices)) {
            // обновляем операцию в БД клиента (только тип платежа)
            foreach ($arUpdInvoices as $ar) {
                $rsOp = Operation::query()
                                 ->where('b24_id', $ar['b24_id'])
                                 ->get()
                                 ->last();
                if ($rsOp) {
                    $rsOp->update($ar);
                }
            }
            // когда есть подключение Б24 - обновляем у клиента счета сразу
            if ($this->updateB24) {
                $this->b24 = new B24App($this->arPortal);
                if ($this->b24) {
                    $arResUpd = $this->updateInB24($arUpdInvoices);
                }
            }

        }
        // dump($arRes);
        // die;

        return [
            'error'                       => false,
            'count_revenue_insert'        => $arResIns['count'],
            'count_revenue_update_in_b24' => count($arUpdInvoices),
        ];
    }

    /**
     * по номеру счета надо отправить статус Оплачен
     *
     * @param $arData
     *
     * @return array
     */
    private function updateInB24($arData)
    {
        // dump($arData);
        $arUpd = [];
        foreach ($arData as $ar) {
            $arUpd[] = [
                'ID'               => $ar['b24_id'],
                'STATUS_ID'        => PaytypeController::$B24_BILL_STATUS_PAYED,
                'PAY_VOUCHER_DATE' => Carbon::createFromFormat('Y-m-d', $ar['date'])
                                            ->minute(0)
                                            ->hour(0)
                                            ->second(0)
                                            ->toDateTimeString(),
            ];
        }
        // dump($arUpd);

        // todo на больших данных виснет обратное событие (нужны очереди)
        $arRes = $this->b24->setInvoicesPaid($arUpd);
        $arRes['count'] = count($arRes);
        // $arRes['count'] = 0;

        return $arRes;
    }

    /**
     * @param $arInsertRevenue
     *
     * @return array
     */
    private function addNewRevenueOps($arInsertRevenue)
    {
        // фильтруем операции которые уже добавлялись когда-то
        $arInsert = $this->filterExistingOperations($arInsertRevenue);

        $arRes['count'] = 0;
        if (count($arInsert)) {
            $arRes = $this->addOperationsToDb($arInsert);
            $arRes['count'] = count($arInsert);
        }

        return $arRes;
    }

    /**
     * @param $data
     * @param $field
     *
     * @return array
     */
    private function mapFieldToArray($data, $field)
    {
        return array_filter(
            array_unique(
                array_map(function ($item) use ($field) {
                    return $item[$field];
                },
                $data
                )
            )
        );
    }

    /**
     *  сопостовлять с данными приложения мы будем по
     *  bank_id (номер документа из выгрузки) и date, summ
     * @param $data
     *
     * @return array
     */
    private function filterExistingOperations($data)
    {
        $arFields = ['bank_id', 'date', 'summ'];

        $rsQuery = Operation::query();

        foreach ($arFields as $field) {
            // $arIds = $this->mapFieldToArray($data, $field);
            $arIds = Helpers::extractByField($field, $data);
            if ($arIds) {
                $rsQuery->whereIn($field, $arIds);
            }
        }
        $arExistOps = $rsQuery->get(['bank_id', 'firm_id', 'contractor_id', 'date', 'summ'])
                              ->toArray();
        // dump($arExistOps);

        // составим хеш существующих операций по ключам
        $arHashedExistOps = [];
        foreach ($arExistOps as $ar) {
            $hash = md5($ar['date'] . $ar['summ'] . $ar['bank_id'] . $ar['firm_id'] . $ar['contractor_id']);
            $arHashedExistOps[$hash] = 1;
        }
        // dump($arHashedExistOps);

        // фильтруем операции которые уже добавлялись когда-то
        $arInsert = [];
        foreach ($data as $ar) {
            $hash = md5($ar['date'] . $ar['summ'] . $ar['bank_id'] . $ar['firm_id'] . $ar['contractor_id']);
            // dump($hash);
            if (!isset($arHashedExistOps[$hash])) {
                $arInsert[] = $ar;
            }
        }
        // dump($arInsert);

        return $arInsert;
    }

    /**
     * расходы мы просто пищем в приложение (если там их нет)
     *
     * @param $arCost
     *
     * @return array
     */
    private function addCostOperations($arCost)
    {
        // фильтруем операции которые уже добавлялись когда-то
        $arInsert = $this->filterExistingOperations($arCost);

        // return [];

        $arRes = ['result' => false, 'count' => 0];
        if (count($arInsert)) {
            $arRes = $this->addOperationsToDb($arInsert);
            $arRes['count'] = count($arInsert);
        }

        // dump($arRes);

        return $arRes;
    }

    /**
     * @param $arData
     *
     * @return array
     */
    private function addOperationsToDb($arData)
    {
        $ctr = new OperationController;
        // фильтруем инпут стандартным обработчиком
        foreach ($arData as $key => &$ar) {
            $ar = $ctr->fillDefault($ar);
        }
        // dump($arData);
        // die;

        $arRes = ['result' => true];

        try {
            Operation::query()->insert($arData);
        } catch (\Exception $e) {
            $arRes = [
                'result'     => false,
                'error'      => App::ErrorToHuman($e->getMessage()),
                'error_real' => utf8_encode($e->getMessage()),
            ];
            App::Log('Ошибка добавления расходов/приходов из файла банка:');
            App::Log($arRes);
            App::Log($arData);
        }

        return $arRes;
    }

    /**
     * @return array
     */
    private function parseBills()
    {
        // dump(count($this->data));

        $arRevenue = []; // приходы, которые надо обновить будет в б24 (в приложении обновится счет по событию)
        $arCost = []; // это расходы, мы их должны записать в приложение
        $dateNow = date('Y-m-d H:i:s');
        foreach ($this->data as $arBill) {
            $arNew = [];
            // dump($arBill);
            $idPaytype = $this->getPaytypeByBill($arBill);
            // dump($idPaytype);
            // die;

            // если у нас не находится фирма - пишем в ответ на фронт
            if (!$idPaytype) {
                App::setApiAdditionalResponse([
                    'unknown_paytype' => $arBill,
                ]);
            }

            if ($idPaytype) {
                $arNew['paytype_id'] = $idPaytype;
                $arNew['summ'] = $arBill['summ'];
                $arNew['bank_id'] = $arBill['bank_id'];
                $arNew['b24_id'] = null;
                $arNew['comment'] = $arBill['desc'];
                if ($arNew['comment']) {
                    $arNew['comment'] = static::$TAG_COMMENT . ' (' . $arNew['comment'] . ')';
                } else {
                    $arNew['comment'] = static::$TAG_COMMENT;
                }
                $arNew['created_at'] = $dateNow;
                $arNew['updated_at'] = $dateNow;

                switch ($idPaytype) {
                    case $this->idCost:
                        $arNew['date'] = $arBill['date_cost'];
                        $arNew['firm_id'] = $this->guessContractor($arBill['from_inn']);
                        $arNew['contractor_id'] = $this->guessContractor($arBill['to_inn']);
                        $arCost[] = $arNew;
                        break;

                    case $this->idRevenue:
                        $arNew['date'] = $arBill['date_revenue'];
                        $arNew['firm_id'] = $this->guessContractor($arBill['to_inn']);
                        $arNew['contractor_id'] = $this->guessContractor($arBill['from_inn']);
                        // номер счета угадывать из Назначения Платежа только если это Приход
                        // $arNew['b24_id'] = $this->guessBillNumber($arBill['desc']);
                        $arNew['b24_id'] = $this->findBillIdByNumber($arBill['desc'], $this->invMap);
                        if ($arNew['b24_id']) {
                            $arNew['account'] = 'Счет №' . $arNew['b24_id'];
                        }
                        $arRevenue[] = $arNew;
                        break;
                }
            }
        }

        return [$arRevenue, $arCost];
    }

    /**
     * @param $inn
     *
     * @return false|int|null|string
     */
    private function guessContractor($inn)
    {
        if ($this->arInnMap[$inn]) {
            return $this->arInnMap[$inn];
        }

        return null;
    }

    /**
     * @param $ar
     *
     * @return mixed
     */
    private function getPaytypeByBill($ar)
    {
        if ($ar['date_cost']) {
            return $this->idCost;
        }

        if ($ar['date_revenue']) {
            return $this->idRevenue;
        }

        return false;
    }

    /**
     * угадываем номер счета по названию скааннмоу из Б24
     *
     * @param $text
     * @param $arInvMap
     *
     * @return int|null|string
     */
    public static function findBillIdByNumber($text, $arInvMap) {
        $idRes = null;

        $text = end(static::convertForRegExp($text));

        foreach ($arInvMap as $b24_id => $accNumber) {
            // dump($accNumber);
            if (preg_match('/'.$accNumber.'/iu', $text)) {
                return $b24_id;
            }
        }

        return $idRes;
    }

    /**
     * @deprecated
     * угадываем номер счета по строке и регулярным выражениям
     *
     * @param $text
     *
     * @return mixed
     */
    public static function guessBillNumber($text)
    {
        // удалим двойные+ пробелы
        $text = preg_replace('/\s+/uis', ' ', $text);
        // вырезаем все даты из комментов
        $text = preg_replace('/(\d{2}[.,\/г])(\d{2}[.,\/г])(\d{4}[.,\/г ]|\d{2}[.,\/г ])/uis', '', $text);
        // это банковсие и личне счета которые могут быть в комменте
        $text = preg_replace('/\d{20}|\d{11}/uis', '', $text);

        // основная регулярка парсера
        // $re = "/[#№N](?'id1'\d+)|[счетуСЧЕТУ.]\s{0,}(?'id2'\d+)|[#№N]\s{0,}(?'id3'\d+)|[счетаСЧЕТА.]\s{0,}(?'id4'\d+)/uis";
        // $re = "/сч(\.\s?№?\s?)(?'id1'\d+)|счету(\s?№?\s?)(?'id2'\d+)|счета\s?(?'id3'\d+)|[#N№](?'id4'\d+)/uis";
        $re = "/сч(\.\s?№?\s?)(?'id1'\d+)|счету(\s?№?\s?)(?'id2'\d+)|счета\s?(?'id3'\d+)|[#N№](?'id4'\d+)|счету(\s?№?\s?)(?'id5'\w+[-\/\\_]\d+)/uis";
        preg_match_all($re, $text, $matches, PREG_SET_ORDER, 0);

        // dump($text);
        // dump($matches);
        $arRes = [];
        foreach ($matches as $ar) {
            $arRes[] = $ar['id1'];
            $arRes[] = $ar['id2'];
            $arRes[] = $ar['id3'];
            $arRes[] = $ar['id4'];
            $arRes[] = $ar['id5'];
        }
        $arRes = array_unique(array_filter($arRes));
        // dump($arRes);

        $idRes = null;
        if (count($arRes) === 1) {
            $idRes = end($arRes);
        } elseif (count($arRes) > 1) {
            $idRes = max(...$arRes);
        }
        // dump(is_numeric($idRes));
        // echo $text, '<br>';
        return $idRes;
    }

    /**
     * @return array
     */
    private function getContractors()
    {
        return Contractor::all(['id', 'inn'])->toArray();
    }
}