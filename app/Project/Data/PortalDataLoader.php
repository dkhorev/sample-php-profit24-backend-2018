<?php

namespace WebNow\Project\Data;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use WebNow\App;
use WebNow\B24App;
use WebNow\Helpers;
use WebNow\Project\Controller\ContractorController;
use WebNow\Project\Controller\ControllerInterface;
use WebNow\Project\Controller\EmployeeController;
use WebNow\Project\Controller\OperationController;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Model\B24Status;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Employee;
use WebNow\Project\Model\Operation;

/**
 * отвечает за загрузку первоначальных данных в порталы
 * а также рекурсивную подгрузку обновлений (из событий)
 *
 * Class PortalDataLoader
 *
 * @package WebNow\Project\Data
 */
class PortalDataLoader
{
    protected $db;
    /* @var B24App $b24 */
    protected $b24;
    protected $arPortal;
    protected $arParams;
    protected $paytypes;

    function __construct(Capsule $db, array $arPortal, array $arParams = [])
    {
        $this->db = $db;
        $this->arPortal = $arPortal;
        $this->arParams = $arParams;
    }

    /**
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function loadAllStatuses(): array
    {
        try {
            $obB24App = $this->getB24();

            B24Status::updateByEntity(array_merge(
                $obB24App->getStatusList(B24Status::STATUS_INVOICE),
                $obB24App->getStatusList(B24Status::STATUS_LEAD)
            ));

            $arRes = [
                'result' => true,
                'next'   => null,
                'total'  => B24Status::all()->count(),
            ];
        } catch (\Exception $e) {
            $arRes = [
                'result' => false,
                'error'  => 'Не удалось загрузить статусы сущностей из Битрикс24',
            ];
        }

        return $arRes;
    }

    /**
     * @param int $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function loadActiveEmployees($from = 0): array
    {
        $arRes['result'] = true;

        $obB24App = $this->getB24();

        $arAllActive = $obB24App->getUsersActive($from);
        // dump($arAllActive['data']);
        // die;
        if ($arAllActive['data']) {
            $arRes = $this->storeEmployees($arAllActive['data']);
        }
        // dump($arRes);
        // die;

        return array_merge($arRes, $arAllActive['info']);
    }

    /**
     * @param $arId
     * @param $type
     *
     * @return array
     */
    public function loadRequisites($arId, $type = 'company')
    {
        if ($arId && !is_array($arId)) {
            $arId = [$arId];
        }

        $obB24App = $this->getB24();

        $arReqs = $obB24App->getRequisiteById($arId, $type);
        $arReqs = collect($arReqs)->groupBy('ENTITY_ID')->toArray();

        return array_map(function ($arReq) {
            $arInn = [];
            foreach ($arReq as $ar) {
                $arInn[] = $ar['RQ_INN'];
            }
            $arInn = array_filter(array_unique($arInn));

            return implode(',', $arInn) ? : null;
        }, $arReqs);
    }

    /**
     * @return B24App
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     */
    private function getB24()
    {
        if (!$this->b24) {
            $this->b24 = new B24App($this->arPortal);
        }

        return $this->b24;
    }

    /**
     * рекурсивно тянет счета по датам или по ID и к ним все сущности
     *
     * @param array $byId
     *
     * @param int   $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function runForInvoices($byId = [], $from = 0): array
    {
        if ($byId && !is_array($byId)) {
            $byId = [$byId];
        }

        $arInfo = [];

        $obB24App = $this->getB24();

        $dateFrom = $this->arParams['dateFrom'] ? : false;
        $dateTo = $this->arParams['dateTo'] ? : false;

        if (count($byId)) {
            // это при работе с обновлением уже на бэкенде
            $arInvoice = $obB24App->getInvoicesById($byId);
        } else {
            // эти запросы идут при установке приложения
            $arResponse = $obB24App->getInvoices(...[$dateFrom, $dateTo, $from]);
            $arInvoice = $arResponse['data'];
            $arInfo = $arResponse['info'];
        }

        // если счет не найден
        if (!$arInvoice) {
            $arRes['result'] = true;

            return array_merge($arRes, $arInfo);
        }
        // dump($arInvoice);
        // die;

        $arRes['result'] = true;

        // получаем все компании ("Мои компании" в Б24) + Компании
        $arFirm = Helpers::extractByField(...['UF_MYCOMPANY_ID', $arInvoice]);
        $arCompany = Helpers::extractByField(...['UF_COMPANY_ID', $arInvoice]);
        $arAllCmp = array_filter(array_unique(array_merge($arFirm, $arCompany)));

        if ($arAllCmp) {

            $arB24Firm = $obB24App->getCompaniesById($arAllCmp);
            $arColl = collect($arB24Firm)->groupBy('IS_MY_COMPANY')->toArray();
            $arFirms = $arColl['Y'];
            $arContractors = $arColl['N'];

            $arReqs = $this->loadRequisites($arAllCmp);
            if ($arFirms) {
                $arRes = $this->storeFirms($arFirms, $arReqs); // пишем все Каналы
            }

            if (true === $arRes['result'] && $arContractors) {
                $arRes = $this->storeContractors($arContractors, $arReqs); // это сохранение компаний в Контрагенты
            }
        }

        // пишем Контакты
        if (true === $arRes['result']) {
            // получаем Контрагенты Контакты
            $arCtrContact = Helpers::extractByField(...['UF_CONTACT_ID', $arInvoice]);
            if ($arCtrContact) {
                $arReqs = $this->loadRequisites($arCtrContact, 'contact');
                $arB24Contact = $obB24App->getContactsById($arCtrContact);
                $arRes = $this->storeContractorsContacts($arB24Contact,
                    $arReqs); // это сохранение Контактов в Контрагенты
            }
        }

        // пишем Сотрудников
        if (true === $arRes['result']) {
            // Сотрудники
            $arEmployee = Helpers::extractByField(...['RESPONSIBLE_ID', $arInvoice]);
            if ($arEmployee) {
                $arEmployee = $obB24App->getUsers($arEmployee);
                $arRes = $this->storeEmployees($arEmployee);
            }
        }

        // пишем счета в Операции (ура, наконец)
        if (true === $arRes['result']) {
            $arRes = $this->storeOperations($arInvoice);
        }

        return array_merge($arRes, $arInfo);
    }

    /**
     * рекурсивно тянет счета по датам или по ID и к ним все сущности
     *
     * @param array $byId
     *
     * @param int   $from
     *
     * @return array
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    public function runForLeads($byId = [], $from = 0): array
    {
        if ($byId && !is_array($byId)) {
            $byId = [$byId];
        }

        $arInfo = [];

        $obB24App = $this->getB24();

        $dateFrom = $this->arParams['dateFrom'] ? : false;
        $dateTo = $this->arParams['dateTo'] ? : false;

        if (count($byId)) {
            $arLead = $obB24App->getLeadsById($byId);
        } else {
            $arResponse = $obB24App->getLeads(...[$dateFrom, $dateTo, $from]);
            $arLead = $arResponse['data'];
            $arInfo = $arResponse['info'];
        }

        // сделки без суммы пропускаем
        $arLead = array_filter($arLead, function ($item) {
            return (float)$item['OPPORTUNITY'] > 0;
        });

        // dump($arLead);
        // die;

        // если лиды не найдены, это не ошибка
        if (0 === count($arLead)) {
            $arRes['result'] = true;

            return array_merge($arRes, $arInfo);
        }
        // dump($arLead);
        // die;

        // Сотрудники
        $arEmployee = Helpers::extractByField(...['ASSIGNED_BY_ID', $arLead]);
        $arEmployee = $obB24App->getUsers($arEmployee);
        $arRes = $this->storeEmployees($arEmployee);

        // пишем счета в Операции (ура, наконец)
        if (true === $arRes['result']) {
            $arRes = $this->storeLeads($arLead);
        }


        return array_merge($arRes, $arInfo);
    }

    /**
     * @param array $arData
     *
     * @return array
     */
    public function storeLeads(array $arData): array
    {
        // нужны все связанные данные портала
        $arEmployees = Employee::all(['id', 'b24_id'])->toArray();
        // маппим b24_id => id
        $arEmployees = $this->mapB24IdToId($arEmployees);

        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            // FIO make
            $nameContact = trim(implode(' ', [$ar['LAST_NAME'], $ar['NAME'], $ar['SECOND_NAME']]));
            $arInsert[$ar['ID']] = [
                'created_at'  => $ar['DATE_CREATE'],
                'updated_at'  => $dateNow,
                'b24_id'      => $ar['ID'],
                'paytype_id'  => PaytypeController::getLeadPayTypeId($ar['STATUS_ID']),
                'manager_id'  => $arEmployees[$ar['ASSIGNED_BY_ID']],
                'employee_id' => $arEmployees[$ar['ASSIGNED_BY_ID']],
                'summ'        => (float)($ar['OPPORTUNITY']),
                'date'        => $ar['DATE_CREATE'],
                'comment'     => $ar['TITLE'] . ' (' . $nameContact . ')',
                'account'     => 'Лид №' . $ar['ID'],
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));

        // dump($arInsert);

        return $this->updateByModel($arInsert, $arB24Id, new Operation, new OperationController);
    }

    /**
     * @param array $arData
     *
     * @return array
     */
    public function storeOperations(array $arData): array
    {
        // нужны все связанные данные портала
        $arEmployees = Employee::all(['id', 'b24_id'])->toArray();
        $arContractors = Contractor::all(['id', 'b24_id', 'is_company', 'is_my'])->where('is_my', false)->toArray();
        $arFirms = Contractor::all(['id', 'b24_id', 'is_company', 'is_my'])->where('is_my', true)->toArray();
        // dump($arContractors);
        // dump($arFirms);
        // маппим b24_id => id
        $arEmployees = $this->mapB24IdToId($arEmployees);
        $arContractors = $this->mapB24IdToIdContractors($arContractors);
        $arFirms = $this->mapB24IdToId($arFirms);
        // dump($arContractors);
        // dump($arFirms);
        // die;

        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            // dump($ar);
            $arInsert[$ar['ID']] = [
                'created_at'    => $ar['DATE_BILL'],
                'updated_at'    => $dateNow,
                'b24_id'        => (int)$ar['ID'],
                'paytype_id'    => PaytypeController::getInvoicePayTypeId($ar['STATUS_ID']),
                'firm_id'       => $arFirms[$ar['UF_MYCOMPANY_ID']],
                'contractor_id' => $arContractors[$ar['UF_COMPANY_ID']][(int)true] ? : $arContractors[$ar['UF_CONTACT_ID']][(int)false],
                'manager_id'    => $arEmployees[$ar['RESPONSIBLE_ID']],
                'employee_id'   => $arEmployees[$ar['RESPONSIBLE_ID']],
                'summ'          => (double)($ar['PRICE']),
                'date'          => $ar['DATE_PAYED'] ? : $ar['DATE_BILL'],
                'comment'       => $ar['ORDER_TOPIC'],
                'account'       => 'Счет №' . $ar['ID'],
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));

        // dump($arInsert);
        // dump($arB24Id);
        // die;

        return $this->updateByModel($arInsert, $arB24Id, new Operation, new OperationController);
    }

    /**
     * @param array $arData
     *
     * @return array
     */
    private function mapB24IdToId(array $arData)
    {
        $arTmp = [];
        foreach ($arData as $ar) {
            $arTmp[$ar['b24_id']] = $ar['id'];
        }

        return $arTmp;
    }

    /**
     * @param array $arData
     *
     * @return array
     */
    private function mapB24IdToIdContractors(array $arData)
    {
        $arTmp = [];
        foreach ($arData as $ar) {
            $arTmp[$ar['b24_id']][$ar['is_company']] = $ar['id'];
        }

        return $arTmp;
    }

    /**
     * @param array $arData
     *
     * @return array
     */
    public function storeEmployees(array $arData): array
    {
        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            $arInsert[$ar['ID']] = [
                'created_at'  => $dateNow,
                'updated_at'  => $dateNow,
                'name'        => $ar['NAME'],
                'lastname'    => $ar['LAST_NAME'],
                'b24_id'      => $ar['ID'],
                'is_active'   => (is_array($ar['UF_DEPARTMENT']) && count($ar['UF_DEPARTMENT'])) && $ar['ACTIVE'],
                'departments' => implode(',', $ar['UF_DEPARTMENT']),
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));
        // dump($arInsert);
        // die;

        return $this->updateByModel($arInsert, $arB24Id, new Employee, new EmployeeController);
    }

    /**
     * сохранение Компаний в Контрагенты
     *
     * @param array $arData
     * @param array $arReqs
     *
     * @return array
     */
    public function storeContractors(array $arData, $arReqs = []): array
    {
        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            $arInsert[$ar['ID']] = [
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
                'name'       => $ar['TITLE'],
                'b24_id'     => $ar['ID'],
                'is_company' => true,
                'is_my'      => false,
                'inn'        => $arReqs[$ar['ID']] ? : null,
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));
        // dump($arInsert);
        // die;

        return $this->updateByModel(
            $arInsert, $arB24Id, new Contractor, new ContractorController,
            ['is_company', [(int)true], 'is_my', [(int)false]]
        );
    }

    /**
     * сохранение Контактов в Контрагенты
     *
     * @param array $arData
     * @param array $arReqs
     *
     * @return array
     */
    public function storeContractorsContacts(array $arData, $arReqs = []): array
    {
        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            // FIO make
            $nameContact = trim(implode(' ', [$ar['LAST_NAME'], $ar['NAME'], $ar['SECOND_NAME']]));
            $arInsert[$ar['ID']] = [
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
                'name'       => $nameContact,
                'b24_id'     => $ar['ID'],
                'is_company' => false,
                'is_my'      => false,
                'inn'        => $arReqs[$ar['ID']] ? : null,
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));
        // dump($arInsert);
        // die;

        return $this->updateByModel(
            $arInsert, $arB24Id, new Contractor, new ContractorController,
            ['is_company', [(int)false], 'is_my', [(int)false]]
        );
    }

    /**
     * @param array $arData
     * @param array $arReqs
     *
     * @return array
     */
    public function storeFirms(array $arData, $arReqs = []): array
    {
        $dateNow = date('Y-m-d H:i:s');
        $arB24Id = []; // это записи которые мы обновим по b24_id
        $arInsert = [];
        foreach ($arData as $ar) {
            $arInsert[$ar['ID']] = [
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
                'name'       => $ar['TITLE'],
                'b24_id'     => $ar['ID'],
                'is_company' => true,
                'is_my'      => true,
                'inn'        => $arReqs[$ar['ID']] ? : null,
            ];
            $arB24Id[] = $ar['ID'];
        }
        $arB24Id = array_unique(array_filter($arB24Id));

        return $this->updateByModel(
            $arInsert, $arB24Id, new Contractor, new ContractorController,
            ['is_company', [(int)true], 'is_my', [(int)true]]
        );
    }

    /**
     * учитываем передаваемую иногда доп фильтрацию
     *
     * @param Builder $rsUpdate
     * @param array   $arAddon
     *
     * @return mixed
     */
    private function applyFilters($rsUpdate, $arAddon = [])
    {
        $arColl = collect($arAddon)->chunk(2)->toArray();
        $arColl = array_map('array_values', $arColl);
        if ($arColl) {
            foreach ($arColl as $addon) {
                $rsUpdate->whereIn(...$addon);
            }
        }

        return $rsUpdate;
    }

    /**
     * @param array $arInsert
     * @param array $arB24Id
     * @param       $model
     * @param       $ctr
     * @param array $arAddon
     *
     * @return array
     */
    private function updateByModel(
        array $arInsert,
        array $arB24Id,
        Model $model,
        ControllerInterface $ctr,
        array $arAddon = []
    ) {
        App::Log('PortalDataLoader: обновление сущности ' . get_class($model));
        App::Log('PortalDataLoader: всего данных на вход = ' . count($arInsert));

        // фильтруем инпут стандартным обработчиком
        foreach ($arInsert as $key => $ar) {
            $ar = $ctr->fillDefault($ar);
            $arInsert[$key] = $ar;
        }
        // dump($arInsert);

        // тянем существующие записи
        // todo это делать в storeXXXXX методах, т.к. у некоторых уник отбор дублей
        $arUpdate = [];
        if (count($arB24Id)) {
            $rsUpdate = $model::query()->whereIn('b24_id', $arB24Id);
            // учитываем передаваемую иногда доп фильтрацию
            $rsUpdate = $this->applyFilters($rsUpdate, $arAddon);
            $arUpdate = $rsUpdate->get()->toArray();
        }
        // dump($arUpdate);
        // die;

        // СЧЕТА могут быть перенесены с прошлого месяца, поэтому тут нам нужен последний из всех дублей
        if (is_a($model, Operation::class)) {
            $arTmp = [];
            foreach ($arUpdate as $ar) {
                unset($ar['is_lead']);
                $arTmp[$ar['b24_id']] = $ar;
            }
            $arUpdate = $arTmp;
        }

        // обновляем выборку (если нужно) и чистим Insert массив
        // todo тут будут проблемы с фирмами, контактами и тп с одним b24_id
        foreach ($arUpdate as $key => $arEl) {
            $arNew = $arInsert[$arEl['b24_id']];

            $bNeedsUpdate = false;

            if ($arNew) {
                $bNeedsUpdate = true;
                // todo у контрагентов мы также проверем поля is_company, is_my - они должны совпадать
                if (is_a($model, Contractor::class)) {
                    if (
                        (bool)$arNew['is_company'] !== (bool)$arEl['is_company']
                        || (bool)$arNew['is_my'] !== (bool)$arEl['is_my']
                    ) {
                        $bNeedsUpdate = false;
                    }
                }
            }

            if ($bNeedsUpdate) {
                $arUpdate[$key] = $ctr->fillDefault($arNew);
                // чистим массив insert'a
                unset($arInsert[$arEl['b24_id']]);
            } else {
                // если новых данных нет, мы не обновляем
                unset($arUpdate[$key]);
            }
        }
        // if (is_a($model, Operation::class)) {
        //     dump($arInsert);
        //     dump($arUpdate);
        //     die;
        // }

        $arRes = ['result' => true];
        try {

            if (count($arInsert)) {
                $model::query()->insert($arInsert);
            }

            if (count($arUpdate)) {
                $db = $this->db::connection();
                $db->transaction(function () use ($db, $arUpdate, $model, $arAddon) {
                    foreach ($arUpdate as $ar) {
                        // эта штука обновит запись только если она существует
                        /* @var Operation $model */
                        $rsUpdate = $model::query()->where('b24_id', $ar['b24_id']);
                        // учитываем передаваемую иногда доп фильтрацию
                        $rsUpdate = $this->applyFilters($rsUpdate, $arAddon);


                        // if (is_a($model, Operation::class)) {
                        //     // $rsUpdate->get()-;
                        //     // dump($arAddon);
                        //     // dump($rsUpdate->get()->toArray());
                        // }

                        // счета могут быть перенесены с прошлого месяца, поэтому тут нам нужен последний
                        // (другие элементы уникальны по b24_id + $arAddon)
                        $count = $rsUpdate->get()->last()->update($ar);
                        // dump($count);
                    }
                });
            }

            App::Log('Обновление успешно.');
        } catch (\Exception $e) {
            // dump($e->getMessage());
            $arRes = [
                'result'     => false,
                'error'      => App::ErrorToHuman($e->getMessage()),
                'error_real' => utf8_encode($e->getMessage()),
            ];
            App::Log('Ошибка обновления:');
            App::Log($arRes);
        }

        // dump($arInsert);

        return $arRes;
    }

}