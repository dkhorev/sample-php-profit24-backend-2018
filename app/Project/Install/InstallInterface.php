<?php

namespace WebNow\Project\Install;


interface InstallInterface
{
    /**
     * make DB, make tables
     * @return mixed
     */
    public function Up();
    
    
    /**
     * drop DB
     *
     * @return mixed
     */
    public function Down();
}