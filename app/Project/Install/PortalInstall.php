<?php

namespace WebNow\Project\Install;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use WebNow\App;
use WebNow\Portal;
use WebNow\Project\Model\Cost;
use WebNow\Project\Model\Paytype;

class PortalInstall implements InstallInterface
{
    
    protected $arData;
    protected $storeType;
    private $db;

    public function __construct(array $arData, Capsule $db)
    {
        $this->db = $db;

        $this->arData = $arData;
        $this->storeType = $arData['storeType'];

        // эти переменные мешают в автозаполнении портала, но приходят с фронта
        unset($this->arData['operation']);
        unset($this->arData['storeType']);
    }

    /**
     * проверяет кастомное соединение перед установкой портала
     *
     * @return array
     */
    public function ValidateCustomConnection(): array
    {
        $arRes = ['result' => false, 'error' => ''];

        try {
            if ($this->validate()) {
                $capsule = new Capsule;
                $capsule->addConnection([
                    'driver'    => 'mysql',
                    'host'      => $this->arData['db_host'],
                    'database'  => $this->arData['db_name'],
                    'username'  => $this->arData['db_user'],
                    'password'  => $this->arData['db_pass'],
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                ]);
                // Make this Capsule instance available globally via static methods... (optional)
                $capsule->setAsGlobal();

                $pdo = $capsule->getConnection()->getPdo();
                $schema = $capsule->schema();

                // $schema = $capsule->
                // тест прав
                $pdo->query("SHOW GRANTS")->fetchAll();

                // создаем таблицу
                $schema->create('testTable', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->charset = 'utf8';
                    $table->collation = 'utf8_unicode_ci';

                    $table->increments('id');
                    $table->string('text');
                    $table->timestamps();
                });

                // наполняем элементами
                $pdo->query("INSERT INTO testTable (text) VALUES ('test1')");
                $pdo->query("INSERT INTO testTable (text) VALUES ('test2')");
                $pdo->query("INSERT INTO testTable (text) VALUES ('test3')");

                // проверяем что селект тоже нам доступен
                $pdo->query('SELECT * FROM testTable')->fetchAll();

                // удаляем таблицу
                $pdo->query('DROP TABLE testTable');


                $arRes['result'] = true;
            }
        } catch (\Exception $e) {
            $arRes['error'] = utf8_encode($e->getMessage());
        }

        return $arRes;
    }
    
    /**
     * проверяем инпут на валидность
     *
     * @return bool
     * @throws \Exception
     */
    private function validate()
    {
        if (!$this->arData['domain']) {
            throw new \Exception('No domain!');
        }
        
        if (!$this->arData['member_id']) {
            throw new \Exception('No member_id!');
        }
        
        if (!$this->arData['access_token']) {
            throw new \Exception('No access_token!');
        }
        
        if (!$this->arData['refresh_token']) {
            throw new \Exception('No refresh_token!');
        }
        
        if (!$this->arData['expires_in']) {
            throw new \Exception('No expires_in!');
        }
        
        // если клиент передал свои настройки, они должны быть полными
        if ('self' === $this->storeType) {
            if (
                !$this->arData['db_name']
                || !$this->arData['db_host']
                || !$this->arData['db_user']
                || !$this->arData['db_pass']
            ) {
                throw new \Exception('Не хватает данных в параметрах БД!');
            }
        }
        
        return true;
    }
    
    /**
     * @return Capsule
     */
    private function makeClientConnection()
    {
        // по умолчанию (хранится у нас)
        $DB_HOST = DB_HOST;
        $DB_NAME = '';
        $DB_USER = DB_USER;
        $DB_PASS = DB_PASS;

        // если клиент предоставил свои настройки
        switch($this->storeType) {
            case 'self':
                $DB_HOST = $this->arData['db_host'];
                $DB_NAME = $this->arData['db_name'];
                $DB_USER = $this->arData['db_user'];
                $DB_PASS = $this->arData['db_pass'];
                break;
        }

        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => $DB_HOST,
            'database'  => $DB_NAME,
            'username'  => $DB_USER,
            'password'  => $DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        return $this->db;
    }
    
    /**
     * @return string
     */
    private function getDbName()
    {
        // return App::getPortalDbName($this->arData['domain']);
        if ('self' === $this->storeType) {
            return $this->arData['db_name'];
        }

        return 'portal_' . $this->arData['member_id'];
    }
    
    /**
     * @return string
     */
    private function getDbUser()
    {
        // return App::getPortalDbUser($this->arData['domain']);
        // return 'user_' . $this->arData['member_id'];
        list($domain, $host, $ru) = explode('.', $this->arData['domain']);

        $sUser = 'user_' . strtolower($domain) . '_' . $this->arData['member_id'];
        $sUser = substr($sUser, 0, 32); // SQLSTATE[HY000]: General error should be no longer than 32
        return $sUser;
    }
    
    /**
     * генерация пароля для нового портала
     *
     * @return string
     */
    private function makeDbPass()
    {
        return str_shuffle(bin2hex(openssl_random_pseudo_bytes(12)));
    }

    /**
     * для инстанса приложения клиента секретный ключ
     *
     * @return string
     */
    private function makeSecretCode()
    {
        return md5($this->arData['member_id'] . APP_SECRET_CODE);
    }

    /**
     * проверяем хранится ли БД клиента у нас
     *
     * @return bool
     */
    private function isDbAppHosted()
    {
        return $this->arData['db_host'] === null;
    }
    
    /**
     * make DB, make tables
     *
     * @return mixed
     */
    public function Up()
    {
        App::Log('Устанавливаем портал в систему: ' . $this->arData['doamin']);
        
        try {
            $this->validate(); // может кинуть исключение
            
            // 0 todo Начать транзакцию
            
            // 1.1 пишем данные клиента в Portal основной базы
            $rsPortal = Portal::query()->where('member_id', $this->arData['member_id'])->first();
            if (!$rsPortal) {
                $rsPortal = new Portal();
            }
            $rsPortal->fill($this->arData);

            // 1.1.1 генерим для приложения клиента секретный ключ
            $sSecretCode = $this->makeSecretCode();
            $rsPortal->setAttribute('code', $sSecretCode);

            // создаем новое подключение для клиента
            $capsule = $this->makeClientConnection();

            // 1.2.1 -создание базы данных под проект (подключение по дефолту или по указанным айпи)
            // создаем базу у себя, если клиент не задал свои настройки
            $dbName = $this->getDbName();
            if ($dbName !== $capsule->getConnection()->getDatabaseName()) {
                // create DB
                $capsule->getConnection()->getPdo()->exec("CREATE DATABASE IF NOT EXISTS `{$dbName}`");
                $capsule->getConnection()->getPdo()->exec("USE `{$dbName}`");
                $capsule->getConnection()->setDatabaseName($dbName);
                
                // create New User for db
                $dbUser = $this->getDbUser();
                $dbPass = $this->makeDbPass();
                $capsule->getConnection()->getPdo()->exec("DROP USER IF EXISTS `{$dbUser}`@`localhost`");
                $capsule->getConnection()->getPdo()->exec("CREATE USER IF NOT EXISTS `{$dbUser}`@`localhost` IDENTIFIED BY '{$dbPass}'");
                $capsule->getConnection()->getPdo()->exec("GRANT ALL PRIVILEGES ON `{$dbName}` . * TO `{$dbUser}`@`localhost`");
                $capsule->getConnection()->getPdo()->exec("FLUSH PRIVILEGES");
                
                // update Portal data
                $rsPortal->setAttribute('db_name', $dbName);
                $rsPortal->setAttribute('db_user', $dbUser);
                $rsPortal->setAttribute('db_pass', $dbPass);
            }

            // 1.3 сохраняем инфу клиента в Portal
            $rsPortal->save();
            
            // 2 -создание всех стандартных таблиц у клиента
            $this->makeDefaultTables($capsule);
            
            // 3 -запись дефолт данных у клиента
            $this->fillDefaultData($capsule);
            
            // 4 todo Закончить транзакцию

            // обязательно вернуть код
            return $sSecretCode;
            
        } catch (\Exception $e) {
            echo 'Error: ' . utf8_encode($e->getMessage());
            
            return false;
        }
    }
    
    /**
     * drop DB
     *
     * @return mixed
     */
    public function Down()
    {
        App::Log('Удаляем портал из системы: ' . $this->arData['domain']);
        
        $capsule = $this->makeClientConnection();
        
        try {
            if ($this->isDbAppHosted()) {
                // destroy DB
                /* @info пока не удаляем автоматом */
                // $dbName = $this->getDbName();
                // $capsule->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `{$dbName}`");

                // DROP USER ‘demo’@‘localhost’;
                $dbUser = $this->getDbUser();
                $capsule->getConnection()->getPdo()->exec("DROP USER IF EXISTS `{$dbUser}`@`localhost`");
            }

            // clear Portal
            $rsPortal = Portal::query()->where('member_id', $this->arData['member_id'])->first();
            if ($rsPortal) {
                $rsPortal->delete();
            }
        } catch (\Exception $e) {
            echo 'Error: ' . utf8_encode($e->getMessage());
    
            return false;
        }
        
        
        return true;
    }
    
    /**
     * создаем стандартные таблицы по списку
     *
     * @param Capsule $capsule
     */
    private function makeDefaultTables(Capsule $capsule)
    {
        $schema = $capsule->schema();
        
        // Каналы (юр лица клиента, принимающие средства)
        /* @deprecated: 21/07/2018 все компании из црм в одной таблице contractors*/
        // if (!$schema->hasTable('firms')) {
        //     $schema->create('firms', function (Blueprint $table) {
        //         $table->engine = 'InnoDB';
        //         $table->charset = 'utf8';
        //         $table->collation = 'utf8_unicode_ci';
        //
        //         $table->increments('id');
        //         $table->string('name', 255);
        //         $table->string('code', 255)->unique();
        //         $table->unsignedInteger('b24_id')->nullable(true);
        //         $table->timestamps();
        //     });
        // }
        
        // Контрагенты (юр лица с которыми работаем)
        if (!$schema->hasTable('contractors')) {
            $schema->create('contractors', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->string('name', 255);
                $table->unsignedInteger('b24_id')->nullable(true);
                $table->boolean('is_company')->default(true);
                $table->boolean('is_my')->default(false);
                $table->text('inn')->nullable(true);
                $table->timestamps();
            });
        }
        
        // Расходы
        if (!$schema->hasTable('costs')) {
            $schema->create('costs', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->string('name', 255)->unique();
                $table->double('price', 10, 2)->default(0);
                $table->timestamps();
            });
        }
        
        // Сотрудники
        if (!$schema->hasTable('employees')) {
            $schema->create('employees', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->string('name', 255);
                $table->string('lastname', 255)->nullable(true);
                $table->float('roi', 8, 2)->nullable(true);
                $table->string('card_number', 255)->nullable(true);
                $table->double('wage_real', 10, 2)->default(0);
                $table->double('wage', 10, 2)->default(0);
                $table->boolean('is_remote')->default(false);
                $table->unsignedInteger('b24_id')->nullable(true);
                $table->boolean('is_active')->default(false);
                $table->string('departments', 255)->nullable(true);
                $table->timestamps();
            });
        }
        
        // Типы платежей
        if (!$schema->hasTable('paytypes')) {
            $schema->create('paytypes', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->string('name', 255)->unique();
                $table->tinyInteger('type');
                $table->timestamps();
            });
        }
        
        // ROI
        if (!$schema->hasTable('rois')) {
            $schema->create('rois', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->unsignedInteger('employee_id');
                $table->year('year');
                $table->string('month', 2);
                $table->double('revenue', 16, 2)->default(0);
                $table->double('costs', 16, 2)->default(0);
                $table->double('roi', 8, 2)->default(0);

                $table->foreign('employee_id')->references('id')->on('employees');
                $table->timestamps();
            });
        }
        
        // Статистика
        // Недельная статистика / Месячная статистика
        if (!$schema->hasTable('statistics')) {
            $schema->create('statistics', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';

                $table->increments('id');
                $table->tinyInteger('type'); // тип 1 - недельный, 2 - месячный, 3 - дневной
                $table->date('label_date')->useCurrent(); // дата для лейбла (недельный - начало недели, месячный - начало месяца, годовой - ...)
                $table->text('metric'); // разрезы статисткики, через зпт типы платежей
                $table->double('summ', 20, 2)->default(0);

                $table->timestamps();
            });
        }
        
        // Транзакции
        if (!$schema->hasTable('operations')) {
            $schema->create('operations', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->increments('id');
                $table->unsignedInteger('paytype_id');
                $table->unsignedInteger('firm_id')->nullable(true);
                $table->unsignedInteger('contractor_id')->nullable(true);
                $table->unsignedInteger('employee_id')->nullable(true);
                $table->unsignedInteger('manager_id')->nullable(true);
                $table->double('summ', 16, 2)->default(0);
                $table->timestamp('date')->useCurrent();
                $table->text('account')->nullable(true);
                $table->unsignedInteger('b24_id')->nullable(true);
                $table->unsignedInteger('bank_id')->nullable(true);
                $table->text('comment')->nullable(true);
                
                $table->foreign('paytype_id')->references('id')->on('paytypes');
                $table->foreign('firm_id')->references('id')->on('contractors');
                $table->foreign('contractor_id')->references('id')->on('contractors');
                $table->foreign('employee_id')->references('id')->on('employees');
                $table->foreign('manager_id')->references('id')->on('employees');

                $table->timestamps();
            });
        }

        // Статус ежемесячных операций
        if (!$schema->hasTable('monthly_tasks')) {
            $schema->create('monthly_tasks', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';

                $table->increments('id');
                $table->tinyInteger('month');
                $table->smallInteger('year');
                $table->boolean('open_bills')->default(false);
                $table->boolean('open_leads')->default(false);
                $table->boolean('wage_50')->default(false);
                $table->boolean('wage_100')->default(false);
                $table->boolean('fixed_costs')->default(false);

                $table->timestamps();
            });
        }

        // карта статусов счетов/лидов из Б24
        if (!$schema->hasTable('b24_statuses')) {
            $schema->create('b24_statuses', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';

                $table->increments('id');
                $table->unsignedInteger('b24_id');
                $table->string('entity', 20); // STATUS_INVOICE = 14 chars
                $table->string('status_id', 255);
                $table->tinyInteger('group');

                $table->timestamps();
            });
        }

        // кастомные опции порталов
        if (!$schema->hasTable('options')) {
            $schema->create('options', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';

                $table->increments('id');
                $table->string('key', 255)->unique();
                $table->longText('value')->nullable(true);

                $table->index('key');

                $table->timestamps();
            });
        }
    }
    
    /**
     * наполняем портал данными по умолчанию
     *
     * @param Capsule $capsule
     */
    private function fillDefaultData(Capsule $capsule)
    {
        $capsule->bootEloquent();
        
        // Типы платежей
        $arDefaultPaytype = [
            ['name' => 'Лид в работе', 'type' => Paytype::TYPE_LEAD],
            ['name' => 'Лид проигран', 'type' => Paytype::TYPE_LEAD_LOST],
            ['name' => 'Перенос', 'type' => Paytype::TYPE_OVER],
            ['name' => 'Приход', 'type' => Paytype::TYPE_REVENUE],
            ['name' => 'Расход', 'type' => Paytype::TYPE_COST],
            ['name' => 'Ожидаемый приход', 'type' => Paytype::TYPE_EXPECT_REVENUE],
            ['name' => 'Ожидаемый расход', 'type' => Paytype::TYPE_EXPECT_COST],
        ];
        
        foreach ($arDefaultPaytype as $ar) {
            Paytype::query()->firstOrCreate($ar);
        }
        // dump(Paytype::all());
        
        // Расходы
        $arDefaultCost = [
            ['name' => 'Аренда', 'price' => 0],
            ['name' => 'Офис', 'price' => 0],
            ['name' => 'Бухгалтерия', 'price' => 0],
        ];
    
        foreach ($arDefaultCost as $ar) {
            Cost::query()->firstOrCreate(['name' => $ar['name']], ['price' => $ar['price']]);
        }
        // dump(Cost::all());
    }
}