<?php

namespace WebNow\Project\Install;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

/**
 * отвечает за генерацию БД основного портала и привязанных к нему таблиц
 *
 * Class AppInstall
 *
 * @package WebNow\Project\Install
 */
class AppInstall implements InstallInterface
{
    /**
     * @var Capsule
     */
    protected $db;

    function __construct(Capsule $db)
    {
        $this->db = $db;

        // копируем root подключение и создаем дефолтное без БД (нужно для осздания своей БД)
        $arConfig = $this->db->getConnection(CONNECTION_MAIN)->getConfig();
        $arConfig['database'] = '';
        unset($arConfig['name']);
        $this->db->addConnection($arConfig);
    }

    /**
     * @return Capsule
     */
    private function getDb()
    {
       return $this->db;
    }

    /**
     * make DB, make tables
     *
     * @param string $dbName
     *
     * @return mixed
     */
    public function Up(string $dbName = DB_NAME)
    {
        $capsule = $this->getDb();

        // create DB
        $capsule->getConnection()->getPdo()->exec("CREATE DATABASE IF NOT EXISTS `{$dbName}`");
        $capsule->getConnection()->getPdo()->exec("USE `{$dbName}`");
        $capsule->getConnection()->setDatabaseName($dbName);
        
        // create Portals
        if (!$capsule->schema()->hasTable('portals')) {
            $capsule->schema()->create('portals', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
                
                $table->bigIncrements('id');
                
                // это придет из Б24
                $table->string('domain', 255);
                $table->string('member_id', 255)->unique();
                $table->string('code', 255)->unique();
                $table->string('application_token', 255)->unique()->nullable(true);
                $table->string('refresh_token', 255);
                $table->string('access_token', 255);
                $table->bigInteger('expires_in');
                
                // это может прийти, а может нет от клиента
                $table->string('db_host', 255)->nullable(true);
                $table->string('db_name', 255)->nullable(true);
                $table->string('db_user', 255)->nullable(true);
                $table->string('db_pass', 255)->nullable(true);
                $table->timestamps();
            });
        }
        
        // echo 'App Install OK';
        
        return true;
    }

    /**
     * drop DB
     *
     * @param string $dbName
     *
     * @return mixed
     */
    public function Down(string $dbName = DB_NAME)
    {
        $capsule = $this->getDb();
        
        // destroy DB
        $capsule->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `{$dbName}`");
        
        // echo 'App Uninstall OK';
        
        return true;
    }
}