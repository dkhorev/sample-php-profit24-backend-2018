<?php


namespace WebNow\Project\Api;


use WebNow\B24App;
use WebNow\Project\BillParser\TxtBillParser;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Data\BillLoader;
use WebNow\Project\Model\Operation;

class BillsFileImport
{
    private static $arInvoiceMap;
    protected $files;
    protected $arPortal;

    /**
     * BillsFileImport constructor.
     *
     * @param $files
     * @param $arPortal
     */
    public function __construct($files, $arPortal)
    {
        $this->files = $files;
        $this->arPortal = $arPortal;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function run()
    {
        $arRes = [];

        foreach ($this->files as $file) {
            try {
                $arRes[] = array_merge(
                    $this->runOnce($file),
                    [
                        'file'     => $this->cleanUpName($file),
                        'filePath' => $file,
                    ]
                );
            } catch (\Exception $e) {
                $arRes[] = [
                    'error'    => true,
                    'msg'      => $e->getMessage(),
                    'msgHuman' => $e->getMessage(),
                    'file'     => $this->cleanUpName($file),
                    'filePath' => $file,
                ];
            }
        }

        return $arRes;
    }

    /**
     * @param $file
     *
     * @return mixed
     */
    private function cleanUpName($file)
    {
        $file = str_replace(BILLS_PATH, '', $file);
        $file = str_replace($this->arPortal['member_id'], '', $file);

        return $file;
    }

    /**
     * получаем незакрытые счета из Б24 и октрытые за текущий месяц
     *
     * @return mixed
     * @throws \Bitrix24\Exceptions\Bitrix24ApiException
     * @throws \Bitrix24\Exceptions\Bitrix24Exception
     * @throws \Bitrix24\Exceptions\Bitrix24SecurityException
     * @throws \Bitrix24\Exceptions\Bitrix24TokenIsExpiredException
     */
    private function getInvoiceMap()
    {
        if (!static::$arInvoiceMap) {
            $obB24App = new B24App($this->arPortal);
            $arOpenInv = $obB24App->getInvoicesOpen();

            $arOpenInvMap = array_map(function ($item) {
                return $item['ACCOUNT_NUMBER'];
            }, $arOpenInv);

            // получаем все операции текущего месяца и их счета
            $arOps = Operation::byMonthYear(date('Y'), date('n'))
                              ->where('b24_id', '>', 0)
                              ->whereIn('paytype_id', array_diff(
                                  PaytypeController::getAllId(),
                                  PaytypeController::getAllLeadId()
                              ))
                              ->get(['b24_id'])
                              ->map(function ($itm) {
                                  return $itm['b24_id'];
                              })
                              ->toArray();

            $arAppInvMap = array_map(function ($item) {
                return $item['ACCOUNT_NUMBER'];
            }, $obB24App->getInvoicesById($arOps));
            // dump($arAppInvMap);

            // соединяем оба массива
            static::$arInvoiceMap = array_replace($arOpenInvMap, $arAppInvMap);
            // dump($arInv);
            // die;
        }

        return static::$arInvoiceMap;
    }

    /**
     * @param $file
     *
     * @return array
     * @throws \Exception
     */
    private function runOnce($file)
    {
        $arRes = [];
        $rsParser = false;

        $fileData = file_get_contents($file);
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        if ($fileData) {
            switch ($ext) {
                case 'txt':
                    $rsParser = new TxtBillParser($fileData);
                    break;

                default:
                    $arRes = [
                        'error'    => true,
                        'msg'      => 'Формат файла пока не поддерживается',
                        'msgHuman' => 'Формат файла пока не поддерживается',
                    ];
            }
        }

        // заливаем операции в базу клиента
        if (!$arRes['error'] && $rsParser) {
            $arData = $rsParser->toArray();

            if (count($arData)) {
                try {
                    // получаем незакрытые счета из Б24
                    $arInvMap = $this->getInvoiceMap();
                    // грузим счета из файла
                    $arRes = (new BillLoader($arData, $this->arPortal, $arInvMap))->load();
                } catch (\Exception $e) {
                    $arRes = [
                        'error'    => true,
                        'msg'      => utf8_encode($e->getMessage()),
                        'msgHuman' => utf8_encode($e->getMessage()),
                    ];
                }
            } else {
                $arRes = [
                    'error'    => false,
                    'msg'      => 'Нет данных для обработки',
                    'msgHuman' => 'Нет данных для обработки',
                ];
            }
        }

        return $arRes;
    }
}