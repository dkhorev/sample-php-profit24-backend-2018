<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Eloquent\Builder;
use WebNow\Helpers;
use WebNow\Project\Model\Cost;
use WebNow\Project\Traits\hasApi;

class CostController implements ControllerInterface
{
    use hasApi;

    /**
     * простейшая валидация наличия полей в формах
     *
     * @param $input
     *
     * @return array
     */
    public function validate(array $input): array
    {
        $rules = [
            'name' => 'required'
        ];

        return Helpers::validateArray($input, $rules);
    }

    /**
     * наполняет входящие данные дефолтными значениями
     *
     * @param array $data
     *
     * @return array
     */
    public function fillDefault(array $data): array
    {
        $data['price'] = (double)$data['price'] ? : 0;

        return $data;
    }


    public function applyScopes(Builder $query): Builder
    {
        return $query;
    }
}