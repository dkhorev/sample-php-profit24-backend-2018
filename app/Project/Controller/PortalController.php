<?php

namespace WebNow\Project\Controller;

use WebNow\Portal;
use WebNow\App;

final class PortalController
{
    /**
     * @param string $sCode
     * @param string $field
     *
     * @return array|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    private static function findPortal(string $sCode = '', string $field = 'code')
    {
        $rsPortal = Portal::query()
                          ->where($field, $sCode)
                          ->first();

        if ($rsPortal) {
            $rsPortal = $rsPortal->toArray();
        } else {
            $rsPortal = null;
        }

        return $rsPortal;
    }

    /**
     * @param string $value
     *
     * @return array|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findByCode(string $value)
    {
        return static::findPortal($value, 'code');
    }

    /**
     * @param string $value
     *
     * @return array|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findByMemberId(string $value)
    {
        return static::findPortal($value, 'member_id');
    }

    /**
     * @param string $value
     *
     * @return array|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findByAppToken(string $value)
    {
        return static::findPortal($value, 'application_token');
    }

    /**
     * добавляет в глобальные подключения - этот портал (автоматом привяжет все модели к БД портала - Cost, Paytype, Employee и т.д.)
     *
     * @param $arPortal
     */
    public static function addConnection($arPortal)
    {
        $capsule = App::Capsule();
        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => $arPortal['db_host'] ?: DB_HOST,
            'database'  => $arPortal['db_name'],
            'username'  => $arPortal['db_user'],
            'password'  => $arPortal['db_pass'],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);
    }


}