<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Capsule\Manager as Capsule;

interface StatisticInterface
{
    public function __construct(Capsule $db, $arParams);
    public function get(): array;
    public function build(): array;
}