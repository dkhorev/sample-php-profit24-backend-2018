<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Eloquent\Builder;

interface ControllerInterface
{
    public function validate(array $input): array;
    public function fillDefault(array $data): array;
    public function applyScopes(Builder $query): Builder;
}