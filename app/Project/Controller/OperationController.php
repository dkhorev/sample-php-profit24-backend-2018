<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Eloquent\Builder;
use WebNow\App;
use WebNow\Helpers;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Operation;
use WebNow\Project\Traits\hasApi;

class OperationController implements ControllerInterface
{
    use hasApi;

    /**
     * простейшая валидация наличия полей в формах
     *
     * @param $input
     *
     * @return array
     */
    public function validate(array $input): array
    {
        $rules = [
            'paytype_id' => 'required',
            'date'       => 'required',
        ];

        return Helpers::validateArray($input, $rules);
    }

    /**
     * наполняет входящие данные дефолтными значениями
     *
     * @param array $data
     *
     * @return array
     */
    public function fillDefault(array $data): array
    {

        $data['summ'] = (double)$data['summ'] ? : 0;
        $data['firm_id'] = strlen($data['firm_id']) ? $data['firm_id'] : null;
        $data['contractor_id'] = strlen($data['contractor_id']) ? $data['contractor_id'] : null;
        $data['employee_id'] = strlen($data['employee_id']) ? $data['employee_id'] : null;
        $data['manager_id'] = strlen($data['manager_id']) ? $data['manager_id'] : null;

        // soft create - если не существует Контрагент создаст его OperationController на бэкенде
        if (!is_integer($data['contractor_id']) && !is_null($data['contractor_id'])) {

            // создаем или тянем этого контрагента
            $rsCtr = new Contractor;
            $rsCtr->setAttribute('name', $data['contractor_id']);
            $rsCtr->setAttribute('is_company', true);
            $arCtr = $rsCtr->toArray();
            $arCtr = (new ContractorController)->fillDefault($arCtr);
            $rsNewCtr = Contractor::query()->firstOrCreate($arCtr);

            if ($rsNewCtr) {
                $arNewCtr = $rsNewCtr->toArray();
                $data['contractor_id'] = $arNewCtr['id'];
                // мы установим на возврат доп данные глобально
                App::setApiAdditionalResponse([
                    'model' => 'Contractor',
                    'add'   => $arNewCtr,
                ]);
            } else {
                throw new \Exception('OperationController: Не могу создать нового контрагента автоматически');
            }
        }

        return $data;
    }

    /**
     * применяет фильтры к запросам (замена глобальным)
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function applyScopes(Builder $query): Builder
    {
        $query = (new Operation)->scopeByRequestMonthYear($query);

        return $query;
    }
}