<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Eloquent\Builder;
use WebNow\Helpers;
use WebNow\Project\Traits\hasApi;

class ContractorController implements ControllerInterface
{
    use hasApi;

    /**
     * простейшая валидация наличия полей в формах
     *
     * @param $input
     *
     * @return array
     */
    public function validate(array $input): array
    {
        $rules = [
            'name' => 'required'
        ];

        return Helpers::validateArray($input, $rules);
    }

    /**
     * наполняет входящие данные дефолтными значениями
     *
     * @param array $data
     *
     * @return array
     */
    public function fillDefault(array $data): array
    {
        $data['b24_id'] = $data['b24_id'] ? : null;

        return $data;
    }

    public function applyScopes(Builder $query): Builder
    {
        return $query;
    }
}