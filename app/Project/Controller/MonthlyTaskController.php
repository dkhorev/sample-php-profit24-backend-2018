<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Carbon;
use WebNow\App;
use WebNow\Project\Model\Cost;
use WebNow\Project\Model\Employee;
use WebNow\Project\Model\MonthlyTask;
use WebNow\Project\Model\Operation;
use WebNow\Project\Model\Paytype;

class MonthlyTaskController
{
    protected $params;
    protected $db;

    public static $WAGE_TAG = 'Зарплата';
    public static $WAGE_50_DAY = 5;
    public static $WAGE_100_DAY = 20;
    public static $FIXED_COSTS_TAG = 'Фикс. расходы';
    public static $OVER_TAG = 'перенос';

    public static $ERR_OP_ALR_DONE = 'Операция уже выполнялась';
    public static $ERR_OP_FAIL = 'Ошибка операции';
    public static $ERR_TASK_SAVE_FAIL = 'Ошибка сохранения';

    public function __construct(Capsule $db, array $params)
    {
        $this->params = $params;
        $this->db = $db;
    }

    /**
     * получение статуса за месяц (вернет (создаст если нету) - статус за выбранный месяц)
     *
     * @return array
     */
    public function getTaskStatus()
    {
        return MonthlyTask::query()
                          ->firstOrCreate(['month' => $this->params['month'], 'year' => $this->params['year']])
                          ->fresh()
                          ->toArray();
    }

    /**
     * произвести некое задание из списка и вернуть текущий статус
     *
     * @return array
     */
    public function makeTask()
    {
        $bOk = false;

        $arCurStatuses = $this->getTaskStatus();

        // операция уже выполнялась
        if ($arCurStatuses[$this->params['task']] === (int)true) {
            $arRes = [
                'error'    => true,
                'msg'      => static::$ERR_OP_ALR_DONE,
                'msgHuman' => static::$ERR_OP_ALR_DONE,
            ];

            return $arRes;
        }

        // task dispatcher
        switch ($this->params['task']) {
            case 'wage_50':
            case 'wage_100':
                $bOk = $this->addHalfWages();
                break;

            case 'fixed_costs':
                $bOk = $this->addFixedCosts();
                break;

            case 'open_leads':
                $bOk = $this->copyPrevOperationsByType(Paytype::TYPE_LEAD);
                break;

            case 'open_bills':
                $bOk = $this->copyPrevOperationsByType(Paytype::TYPE_EXPECT_REVENUE);
                break;
        }

        if (!$bOk) {
            $arRes = [
                'error'    => true,
                'msg'      => static::$ERR_OP_FAIL,
                'msgHuman' => static::$ERR_OP_FAIL,
            ];

            return $arRes;
        }

        // ставим статус что операция закрыта
        /* @var MonthlyTask $rsTask */
        $rsTask = MonthlyTask::byMonthYear($this->params['month'], $this->params['year'])
                             ->first();

        $rsTask->setAttribute($this->params['task'], true);

        if (!$rsTask->save()) {
            $arRes = [
                'error'    => true,
                'msg'      => static::$ERR_TASK_SAVE_FAIL,
                'msgHuman' => static::$ERR_TASK_SAVE_FAIL,
            ];

            return $arRes;
        }

        // if all good
        return $this->getTaskStatus();
    }

    /**
     * копируем операции определенного типа из предыдущего месяца (Лиды в работе, Одижаемый приход)
     *
     * @param $idType
     *
     * @return bool
     */
    private function copyPrevOperationsByType($idType)
    {
        // вытащить все операции с типом $idType
        $arOpTypes = PaytypeController::getAllByType($idType);
        $arOpTypes = array_map(function ($item) {
            return $item['id'];
        }, $arOpTypes);
        // dump($arOpTypes);

        // testing
        // $this->params['month'] = 12;
        // $this->params['year'] = 2017;
        // testing

        $arOps = Operation::byPreviousMonthYear($this->params['month'], $this->params['year'])
                          ->byPaytype($arOpTypes)/* query scope in Operation: scopeByPaytype(...) */
                          ->get()
                          ->toArray();
        // dump($arOps);

        if (count($arOps)) {
            // преобразуем записи для новой вставки в Операции
            $dateNow = Carbon::create($this->params['year'], $this->params['month'], 1, 0, 0, 0)
                             ->toDateString();

            $arOps = array_map(function ($item) use ($dateNow) {
                unset($item['id']);
                unset($item['is_lead']);
                $item['date'] = $dateNow;
                $item['created_at'] = $dateNow;
                $item['updated_at'] = $dateNow;
                if (!preg_match('/' . static::$OVER_TAG . '/u', $item['comment'])) {
                    $item['comment'] .= '(' . static::$OVER_TAG . ')';
                }

                return $item;
            }, $arOps);

            $arRes = $this->addOperations($arOps);

            return $arRes['result'];
        }

        // ничего не перенесли - тоже ок
        return true;
    }

    /**
     * обновление операций по массиву
     *
     * @param array $arInsert
     *
     * @return array
     * @throws \Throwable
     */
    private function addOperations(array $arInsert)
    {
        App::Log('MonthlyTaskController: создание Операций по задаче ' . $this->params['task']);

        // фильтруем инпут стандартным обработчиком
        $ctr = new OperationController;
        foreach ($arInsert as &$ar) {
            $ar = $ctr->fillDefault($ar);
        }
        // dump($arInsert);
        // die;

        $arRes = ['result' => true];
        try {
            if (count($arInsert)) {
                Operation::query()->insert($arInsert);
            }

            App::Log('Обновление успешно.');
        } catch (\Exception $e) {
            $arRes = [
                'result'     => false,
                'error'      => App::ErrorToHuman($e->getMessage()),
                'error_real' => utf8_encode($e->getMessage()),
            ];
            App::Log('Ошибка обновления:');
            App::Log($arRes);
        }
        // dump($arRes);
        // die;

        return $arRes;
    }

    /**
     * @return bool
     */
    private function addFixedCosts()
    {
        // вытащить все постоянные расходы
        $arFixedCosts = Cost::all()->toArray();
        // dump($arFixedCosts);

        // вытащить первую операцию с расходом
        $arOp = PaytypeController::getFirstExpectedCost();

        $dateOper = Carbon::create($this->params['year'], $this->params['month'], (int)date('j'), 0, 0, 0)
                          ->toDateString();
        $dateNow = date('Y-m-d H:i:s');

        // создать массово операции
        $arInsert = [];
        foreach ($arFixedCosts as $ar) {
            $arInsert[$ar['id']] = [
                'paytype_id' => $arOp['id'],
                'summ'       => (double)$ar['price'],
                'date'       => $dateOper,
                'created_at' => $dateNow,
                'updated_at' => $dateNow,
                'comment'    => static::$FIXED_COSTS_TAG . ' (' . $ar['name'] . ')',
            ];
        }
        // dump($arInsert);
        // die;

        $arRes = $this->addOperations($arInsert);

        return $arRes['result'];
    }

    /**
     * @return bool
     */
    private function addHalfWages()
    {
        // получить всех сотрудников портала с ЗП реальной > 0 и активных
        $arEmployees = Employee::query()
                               ->where('is_active', (int)true)
                               ->where('wage_real', '>', 0)
                               ->whereNotNull('departments')
                               ->where('departments', '<>', '')
                               ->get(['id', 'wage_real'])
                               ->toArray();
        // dump($arEmployees);

        // тип операции З/П
        $nDay = static::$WAGE_50_DAY;
        if ('wage_100' === $this->params['task']) {
            $nDay = static::$WAGE_100_DAY;
        }

        $dateOper = Carbon::create($this->params['year'], $this->params['month'], $nDay, 0, 0, 0);
        $dateNow = Carbon::now();

        // в зависимости от текущей даты
        // вытащить первую операцию с расходом
        if ($dateNow->getTimestamp() >= $dateOper->getTimestamp()) {
            $arOp = PaytypeController::getFirstCost();
        } else {
            // или вытащить первую операцию с ожидаемым расходом
            $arOp = PaytypeController::getFirstExpectedCost();
        }

        // создать массово операции
        $arInsert = [];
        foreach ($arEmployees as $ar) {
            $arInsert[$ar['id']] = [
                'paytype_id'  => $arOp['id'],
                'employee_id' => $ar['id'],
                'summ'        => (double)($ar['wage_real'] / 2),
                'date'        => $dateOper->toDateString(),
                'created_at'  => $dateNow->toDateString(),
                'updated_at'  => $dateNow->toDateString(),
                'comment'     => static::$WAGE_TAG,
            ];
        }
        // dump($arInsert);

        $arRes = $this->updateByEmployeeModel($arInsert, ['date', $dateOper]);

        // dump($arRes);

        return $arRes['result'];
    }

    /**
     * обновление операций по ЗП
     *
     * @param array $arInsert
     * @param array $arAddon
     *
     * @return array
     * @throws \Throwable
     */
    private function updateByEmployeeModel(array $arInsert, array $arAddon = [])
    {
        App::Log('MonthlyTaskController: обновление Операций по задаче ' . $this->params['task']);

        // фильтруем инпут стандартным обработчиком
        $ctr = new OperationController;
        foreach ($arInsert as &$ar) {
            $ar = $ctr->fillDefault($ar);
        }
        // dump($arInsert);

        // тянем существующие записи
        $arEmplId = array_map(function ($item) {
            return $item['employee_id'];
        }, $arInsert);
        // dump($arEmplId);
        $arUpdate = [];
        if (count($arEmplId)) {
            $rsUpdate = Operation::query()
                                 ->whereIn('employee_id', $arEmplId)
                                 ->where('comment', static::$WAGE_TAG);

            // учитываем передаваемую иногда доп фильтрацию
            if ($arAddon) {
                $rsUpdate->where(...$arAddon);
            }

            $arUpdate = $rsUpdate->get()->toArray();
        }
        // dump($arUpdate);

        // обновляем их (если нужно) и чистим Insert массив
        foreach ($arUpdate as $key => $arEl) {
            $arNew = $arInsert[$arEl['employee_id']];

            if ($arNew) {
                $arUpdate[$key] = $ctr->fillDefault($arNew);
                // чистим массив insert'a
                unset($arInsert[$arEl['employee_id']]);
            } else {
                // если новых данных нет, мы не обновляем
                unset($arUpdate[$key]);
            }
        }
        // dump($arInsert);
        // dump($arUpdate);

        $arRes = ['result' => true];
        try {
            if (count($arInsert)) {
                Operation::query()->insert($arInsert);
            }

            if (count($arUpdate)) {
                $db = $this->db::connection();
                $db->transaction(function () use ($db, $arUpdate, $arAddon) {
                    foreach ($arUpdate as $ar) {
                        // эта штука обновит запись только если она существует
                        $rsUpdate = Operation::query()
                                             ->where('employee_id', $ar['employee_id'])
                                             ->where('comment', static::$WAGE_TAG);

                        // учитываем передаваемую иногда доп фильтрацию
                        if ($arAddon) {
                            $rsUpdate->where(...$arAddon);
                        }

                        $rsUpdate->update($ar);
                    }
                });
            }

            App::Log('Обновление успешно.');
        } catch (\Exception $e) {
            $arRes = [
                'result'     => false,
                'error'      => App::ErrorToHuman($e->getMessage()),
                'error_real' => utf8_encode($e->getMessage()),
            ];
            App::Log('Ошибка обновления:');
            App::Log($arRes);
        }

        // dump($arInsert);

        return $arRes;
    }
}