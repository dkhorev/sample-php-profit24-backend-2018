<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Eloquent\Builder;
use WebNow\Helpers;
use WebNow\Project\Model\B24Status;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Traits\hasApi;

final class PaytypeController implements ControllerInterface
{
    use hasApi;

    private static $paytypes;
    private static $allLeadId;
    private static $allId;
    private static $arStatusMap;

    public static $B24_BILL_STATUS_PAYED = 'P';

    /**
     * простейшая валидация наличия полей в формах
     *
     * @param $input
     *
     * @return array
     */
    public function validate(array $input): array
    {
        $rules = [
            'name' => 'required',
            'type' => 'required',
        ];

        return Helpers::validateArray($input, $rules);
    }

    /**
     * наполняет входящие данные дефолтными значениями
     *
     * @param array $data
     *
     * @return array
     */
    public function fillDefault(array $data): array
    {
        return $data;
    }

    /**
     * типы всех платежей приложения
     *
     * @return array
     */
    public static function getPaytypeVariants()
    {
        return [
            ['id' => Paytype::TYPE_LEAD, 'name' => 'Лид в работе', 'code' => 'TYPE_LEAD'],
            ['id' => Paytype::TYPE_OVER, 'name' => 'Перенос', 'code' => 'TYPE_OVER'],
            ['id' => Paytype::TYPE_REVENUE, 'name' => 'Приход', 'code' => 'TYPE_REVENUE'],
            ['id' => Paytype::TYPE_COST, 'name' => 'Расход', 'code' => 'TYPE_COST'],
            ['id' => Paytype::TYPE_EXPECT_REVENUE, 'name' => 'Ожидаемый приход', 'code' => 'TYPE_EXPECT_REVENUE'],
            ['id' => Paytype::TYPE_EXPECT_COST, 'name' => 'Ожидаемый расход', 'code' => 'TYPE_EXPECT_COST'],
            ['id' => Paytype::TYPE_LEAD_LOST, 'name' => 'Лид проигран', 'code' => 'TYPE_LEAD_LOST'],
        ];
    }

    /**
     * @return array
     */
    private static function getPaytypes()
    {
        if (!static::$paytypes) {
            static::$paytypes = Paytype::all()->toArray();
        }

        return static::$paytypes;
    }

    /**
     * @param $idType
     *
     * @return array
     */
    public static function getAllByType($idType)
    {
        if (!is_array($idType)) {
            $idType = [$idType];
        }

        return array_filter(static::getPaytypes(), function ($item) use ($idType) {
            return in_array($item['type'], $idType);
        });
    }

    /**
     * все операции типа Лид у клиента
     *
     * @return array
     */
    public static function getAllLeadPaytype()
    {
        return static::getAllByType([Paytype::TYPE_LEAD, Paytype::TYPE_LEAD_LOST]);
    }

    /**
     * все id операций типа Лид у клиента
     *
     * @return array
     */
    public static function getAllId()
    {
        if (!static::$allId) {
            static::$allId = array_map(function ($item) {
                return $item['id'];
            }, static::getPaytypes());
        }

        return static::$allId;
    }

    /**
     * все id операций типа Лид у клиента
     *
     * @return array
     */
    public static function getAllLeadId()
    {
        if (!static::$allLeadId) {
            static::$allLeadId = array_map(function ($item) {
                return $item['id'];
            }, static::getAllLeadPaytype());
        }

        return static::$allLeadId;
    }

    /**
     * по коду статуса Лида определяет какой тип платежа по умолчанию ему назначить
     * 10.07.2018 - новые статусы лидов: Лид в работе, Лид проигран
     *
     * @param string $status_id
     *
     * @return bool
     */
    public static function getLeadPayTypeId(string $status_id) {
        // полуить статусы лидов
        $idLead = PaytypeController::getFirstLead()['id'];
        $idLeadFail = PaytypeController::getFirstLeadLost()['id'];

        // получить карту статусов портала
        $arMap = static::getStatusMap(B24Status::STATUS_LEAD);

        // найти переданный статус, определить в какой он группе
        switch ($arMap[$status_id]) {
            case B24Status::GROUP_PROGRESS:
                return $idLead;
            case B24Status::GROUP_FAIL:
                return $idLeadFail;
            case B24Status::GROUP_SUCCESS:
        }

        return false; // по умолчанию
    }

    /**
     * по коду статуса счета в Б24 должен вернуть подходящий тип платежа из системы
     * ОПЛАЧЕН - хардкод статус в б24
     *
     * @param string $status_id
     *
     * @return bool
     */
    public static function getInvoicePayTypeId(string $status_id)
    {
        // полуить статус ожидаемого прихода и прихода
        $idExpRev = PaytypeController::getFirstExpectedRevenue()['id'];
        $idRev = PaytypeController::getFirstRevenue()['id'];

        // получить карту статусов портала
        $arMap = static::getStatusMap(B24Status::STATUS_INVOICE);

        // найти переданный статус, определить в какой он группе
        switch ($arMap[$status_id]) {
            case B24Status::GROUP_PROGRESS:
                return $idExpRev;
            case B24Status::GROUP_SUCCESS:
                return $idRev;
            case B24Status::GROUP_FAIL:
        }

        return false; // по умолчанию
    }

    /**
     * @param $entity
     *
     * @return mixed
     */
    private static function getStatusMap($entity)
    {
        if (!static::$arStatusMap[$entity]) {
            $arRes = B24Status::getByEntity($entity)->get()->groupBy('status_id')->toArray();
            $arRes = array_map(function($items) {
                return current($items)['group'];
            }, $arRes);
            static::$arStatusMap[$entity] = $arRes;
        }

        return static::$arStatusMap[$entity];
    }

    /**
     * @return mixed
     */
    public static function getFirstLead()
    {
        return current(static::getAllByType(Paytype::TYPE_LEAD));
    }

    /**
     * @return mixed
     */
    public static function getFirstLeadLost()
    {
        return current(static::getAllByType(Paytype::TYPE_LEAD_LOST));
    }

    /**
     * @return mixed
     */
    public static function getFirstCost()
    {
        return current(static::getAllByType(Paytype::TYPE_COST));
    }

    /**
     * @return mixed
     */
    public static function getFirstExpectedCost()
    {
        $arCosts = static::getAllByType(Paytype::TYPE_EXPECT_COST);

        return current($arCosts);
    }

    /**
     * @return mixed
     */
    public static function getFirstRevenue()
    {
        $arCosts = static::getAllByType(Paytype::TYPE_REVENUE);

        return current($arCosts);
    }

    /**
     * @return mixed
     */
    public static function getFirstExpectedRevenue()
    {
        $arCosts = static::getAllByType(Paytype::TYPE_EXPECT_REVENUE);

        return current($arCosts);
    }

    public function applyScopes(Builder $query): Builder
    {
        return $query;
    }
}