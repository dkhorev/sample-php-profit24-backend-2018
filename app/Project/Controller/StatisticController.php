<?php

namespace WebNow\Project\Controller;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use WebNow\App;
use WebNow\Helpers;
use WebNow\Project\Model\Operation;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Model\Statistic;

class StatisticController implements StatisticInterface
{
    protected $fromDate;
    protected $toDate;
    protected $metrics;
    protected $type;
    protected $db;
    protected $result;

    // отрицательные множители при расчете сложных метрик
    protected $complexMetricRules = [
        Paytype::TYPE_COST        => -1,
        Paytype::TYPE_EXPECT_COST => -1,
    ];

    /**
     * StatisticController constructor.
     *
     * @param $db
     * @param $arParams
     */
    public function __construct(Capsule $db, $arParams)
    {
        $this->db = $db;
        $this->fromDate = $arParams['fromDate'];
        $this->toDate = $arParams['toDate'];
        $this->type = (int)$arParams['type'];
        $this->metrics = $arParams['metrics'];

        // проверяем вход с фронта
        $this->verifyInput();
        // парсим в нужный формат
        $this->parseInput();
        // проверяем что получилось после парсинга
        $this->verifyParsedInput();

        $this->db = $db;
    }

    /**
     * @throws \Exception
     */
    private function verifyInput()
    {
        if (!$this->metrics || !is_array($this->metrics)) {
            $this->metrics = json_decode($this->metrics);
            if (!$this->metrics || !is_array($this->metrics)) {
                throw new \Exception('Не указаны метрики отбора статистики');
            }
        }

        if (
            Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->fromDate)->getTimestamp()
            > Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->toDate)->getTimestamp()
        ) {
            throw new \Exception('Время ОТ не может быть меньше времени ДО');
        }

        if (!$this->fromDate || !$this->toDate) {
            throw new \Exception('Не указаны даты для отбора статистики: от и/или до');
        }

        if (!$this->type) {
            throw new \Exception('Не указан тип отбора статистики: день, неделя, месяц');
        }
    }

    /**
     *
     */
    private function parseInput()
    {
        // преобразуем метрики в массив и в них в сортированную строку 3,2,1 => 1,2,3
        foreach ($this->metrics as $key => $metric) {
            if (is_array($metric)) {
                $metric = array_unique(array_filter($metric));
                sort($metric);
                $this->metrics[$key] = implode(',', $metric);
            } else {
                $this->metrics[$key] = (string)$metric;
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function verifyParsedInput()
    {
        foreach ($this->metrics as $key => $metric) {
            if (!$metric) {
                throw new \Exception('Передана пустая метрика в массиве metrics в позиции ' . $key);
            }
        }
    }

    /**
     * @param $metric
     *
     * @return Builder
     */
    private function getStatsQuery($metric)
    {
        $query = Statistic::query();

        $query->where('type', $this->type);
        $query->where('metric', $metric);

        // выборка периодов зависит от запроса
        switch ($this->type) {
            case Statistic::TYPE_WEEK:
                $arAllPeriods = array_keys($this->getRealWeeksBetweenDates());
                // dump($arAllPeriods);
                $fromDate = current($arAllPeriods);
                $toDate = end($arAllPeriods);
                break;
            default:
                $fromDate = $this->fromDate;
                $toDate = $this->toDate;
        }

        $query->where('label_date', '>=', $fromDate);
        $query->where('label_date', '<=', $toDate);

        return $query;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        // todo пока что статистика всегда перестраивается
        $this->build();

        foreach ($this->metrics as $key => $metric) {
            $query = $this->getStatsQuery($metric);
            $ar = $query->get(['label_date', 'summ'])->sortBy('label_date')->toArray();
            $this->result[$metric] = $ar;
        }

        return $this->result;
    }

    /**
     * @return array
     */
    public function build(): array
    {
        foreach ($this->metrics as $key => $metric) {
            $this->buildByMetric($metric);
        }

        return ['result' => true];
    }

    /**
     * @param $metric
     *
     * @return array
     */
    private function buildByMetric($metric)
    {
        // из базовых типов платежей надо клиентские получить
        $arMetric = explode(',', $metric);
        $arPaytypeId = Helpers::extractByField('id', PaytypeController::getAllByType($arMetric));
        // dump($arPaytypeId);

        // выборка периодов зависит от запроса
        switch ($this->type) {
            case Statistic::TYPE_WEEK:
                $arAllPeriods = array_keys($this->getRealWeeksBetweenDates());
                // dump($arAllPeriods);
                $fromDate = current($arAllPeriods);
                // это начало недели, для запроса в БД надо его еще раз преобразовать в конец недели
                $toDate = end($arAllPeriods);
                $toDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $toDate)
                                ->endOfWeek()
                                ->toDateString();
                break;
            default:
                $fromDate = $this->fromDate;
                $toDate = $this->toDate;
        }

        $queryOps = (new Operation)->query();
        $queryOps->whereIn('paytype_id', $arPaytypeId);
        $queryOps->whereBetween('date', [$fromDate, $toDate]);

        $arData = $queryOps->get(['summ', 'date', 'paytype_id'])->toArray();
        // dump($arData);

        // стата по дням (!!! этот шаг важен всегда !!!)
        $arByDays = $this->opsToDays($arData, $metric, $arPaytypeId);
        // dump($arByDays);

        // стата по неделям (генерация и запись)
        if (Statistic::TYPE_WEEK === $this->type) {
            $arByWeek = $this->daysToWeeks($arByDays);
            // dump($arByWeek);
            // dump($this->Type);
            if ($arByWeek) {
                $arInsert = $this->prepareData($arByWeek, $metric);

                // dump($arInsert);
                return $this->insertData($arInsert);
            }
        }

        // стата по месяцам (генерация и запись)
        if (Statistic::TYPE_MONTH === $this->type) {
            $arByMonth = $this->daysToMonths($arByDays);
            // dump($arByMonth);
            // dump($this->Type);
            if ($arByMonth) {
                $arInsert = $this->prepareData($arByMonth, $metric);

                // dump($arInsert);
                return $this->insertData($arInsert);
            }
        }

        return [];
    }

    /**
     * @param $ar
     * @param $fields
     *
     * @return string
     */
    private function getHash($ar, $fields)
    {
        $res = array_reduce($fields, function ($acc, $field) use ($ar) {
            return $acc . $ar[$field];
        });

        return md5($res);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function insertData($data)
    {
        // получим существующие операции
        $arFields = ['type', 'label_date', 'metric'];
        $rsQuery = Statistic::query();

        foreach ($arFields as $field) {
            // $arIds = $this->mapFieldToArray($data, $field);
            $arIds = Helpers::extractByField($field, $data);
            if ($arIds) {
                $rsQuery->whereIn($field, $arIds);
            }
        }
        $arExistOps = $rsQuery->get(array_merge(['id'], $arFields))
                              ->toArray();
        // dump($arExistOps);

        // составим хеш существующих операций по ключам
        $arHashedExistOps = [];
        foreach ($arExistOps as $ar) {
            $hash = $this->getHash($ar, $arFields);
            $arHashedExistOps[$hash] = $ar;
        }

        // фильтруем операции которые уже добавлялись когда-то
        $arInsert = [];
        $arUpdate = [];
        foreach ($data as $ar) {
            $hash = $this->getHash($ar, $arFields);
            // dump($hash);
            if (!isset($arHashedExistOps[$hash])) {
                $arInsert[] = $ar;
            } else {
                $arNew = $arHashedExistOps[$hash];
                $arNew['summ'] = $ar['summ'];
                $arUpdate[] = $arNew;
            }
        }
        // dump($arInsert);
        // dump($arUpdate);

        try {
            if (count($arInsert)) {
                Statistic::query()->insert($arInsert);
            }

            if (count($arUpdate)) {
                $this->updateData($arUpdate);
            }
        } catch (\Exception $e) {
            $arRes = [
                'result'     => false,
                'error'      => App::ErrorToHuman($e->getMessage()),
                'error_real' => utf8_encode($e->getMessage()),
            ];

            return $arRes;
        }

        return ['result' => true];
    }

    /**
     * @param $data
     *
     * @throws \Throwable
     */
    private function updateData($data)
    {
        $db = $this->db::connection();
        $db->transaction(function () use ($db, $data) {
            foreach ($data as $ar) {
                // эта штука обновит запись только если она существует
                /* @var Statistic $model */
                $rsUpdate = Statistic::query()->where('id', $ar['id']);
                $rsUpdate->update($ar);
            }
        });
    }

    /**
     * @param $data
     *
     * @param $metric
     *
     * @return array
     */
    private function prepareData($data, $metric)
    {
        $arRes = array_map(function ($summ, $date) use ($metric) {
            return [
                'type'       => $this->type,
                'label_date' => $date,
                'metric'     => $metric,
                'summ'       => $summ,
            ];
        }, $data, array_keys($data));

        return $arRes;
    }

    /**
     * @param $data
     *
     * @param $metric
     *
     * @param $arPaytypeId
     *
     * @return array
     */
    private function opsToDays($data, $metric, $arPaytypeId)
    {
        // для комплесных метрик мы будем использовать правила
        $isComplexMetric = $this->isComplexMetric($metric);
        // dump($metric);
        // dump($isComplexMetric);
        // dump($arPaytypeId);

        $arByDays = [];
        collect($data)
            ->groupBy('date')
            ->each(function ($item, $key) use (&$arByDays, $arPaytypeId, $isComplexMetric) {
                foreach ($item as $op) {
                    // dump($op);
                    $realPaytypeId = array_search($op['paytype_id'], $arPaytypeId);
                    // dump($realPaytypeId);
                    // учитываем сложные метрики
                    if ($isComplexMetric && $realPaytypeId) {
                        $mod = $this->complexMetricRules[$realPaytypeId] ?: 1;
                        $arByDays[$key] += $mod * $op['summ'];
                    } else {
                        $arByDays[$key] += $op['summ'];
                    }

                }
            });

        return $arByDays;
    }

    /**
     * @param $metric
     *
     * @return bool
     */
    private function isComplexMetric($metric)
    {
        return strpos($metric, ',') > 0;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function daysToMonths($data)
    {
        // парсим реальные данные, которые есть
        $arByMonth = [];
        collect($data)->each(function ($summ, $date) use (&$arByMonth) {
            $dateCur = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $date);
            $arByMonth[$dateCur->startOfMonth()->toDateString()] += $summ;
        });
        // dump($arByMonth);

        // если данных нету, мы сгенерим пустышки, они нужны для точек на графике
        // $arAllPeriods = $this->getRealWeeksBetweenDates();
        $arAllPeriods = $this->getRealMonthsBetweenDates();
        // dump($arAllPeriods);

        // наполняем данными и болванками
        $arResult = [];
        foreach ($arAllPeriods as $date => $data) {
            if (isset($arByMonth[$date])) {
                $arResult[$date] = $arByMonth[$date];
            } else {
                $arResult[$date] = 0;
            }
        }

        // dump($arResult);

        return $arResult;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function daysToWeeks($data)
    {
        // парсим реальные данные, которые есть
        $arByWeek = [];
        collect($data)->each(function ($summ, $date) use (&$arByWeek) {
            $dateCur = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $date);
            $arByWeek[$dateCur->startOfWeek()->toDateString()] += $summ;
        });
        // dump($arByWeek);

        // если данных нету, мы сгенерим пустышки, они нужны для точек на графике
        $arAllPeriods = $this->getRealWeeksBetweenDates();
        // dump($arAllPeriods);

        // наполняем данными и болванками
        $arResult = [];
        foreach ($arAllPeriods as $date => $data) {
            if (isset($arByWeek[$date])) {
                $arResult[$date] = $arByWeek[$date];
            } else {
                $arResult[$date] = 0;
            }
        }

        // dump($arResult);

        return $arResult;
    }

    /**
     * @return array
     */
    private function getRealWeeksBetweenDates()
    {
        $fromDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->fromDate)->setTime(0, 0, 0);
        $toDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->toDate)->setTime(23, 59, 59);

        $arAllPeriods = [];
        while ($fromDate->getTimestamp() < $toDate->getTimestamp()) {
            $arAllPeriods[$fromDate->startOfWeek()->toDateString()] = 0;
            $fromDate->addWeek();
        }

        return $arAllPeriods;
    }

    /**
     * @return array
     */
    private function getRealMonthsBetweenDates()
    {
        $fromDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->fromDate)->setTime(0, 0, 0);
        $toDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $this->toDate)->setTime(23, 59, 59);

        $arAllPeriods = [];
        while ($fromDate->getTimestamp() < $toDate->getTimestamp()) {
            $arAllPeriods[$fromDate->startOfMonth()->toDateString()] = 0;
            $fromDate->addMonth();
        }

        return $arAllPeriods;
    }
}