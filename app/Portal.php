<?php

namespace WebNow;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Portal
 * ORM для работы с порталами
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package WebNow
 */
class Portal extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = CONNECTION_MAIN;

    protected $encryprt_algo = 'aes-256-ctr';

    protected $fillable = [
        'domain',
        'member_id',
        'access_token',
        'refresh_token',
        'expires_in',
        'db_host',
        'db_name',
        'db_user',
        'db_pass',
    ];

    /**
     * @param $data
     *
     * @return string
     */
    // public function getDbPassAttribute($data)
    // {
    //     return openssl_decrypt(
    //         $data,
    //         $this->encryprt_algo,
    //         APP_SECRET_CODE,
    //         null,
    //         substr(APP_SECRET_CODE, 0, 16)
    //     );
    // }

    /**
     * @param $pass
     */
    // public function setDbPassAttribute($pass)
    // {
    //     $this->setAttribute(
    //         'db_pass',
    //         openssl_encrypt(
    //             $pass,
    //             $this->encryprt_algo,
    //             APP_SECRET_CODE,
    //             null,
    //             substr(APP_SECRET_CODE, 0, 16)
    //         )
    //     );
    // }


}