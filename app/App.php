<?php

namespace WebNow;

use WebNow\Cache\Redis;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * контейнер приложения
 *
 * Class App
 *
 * @package WebNow
 */
class App
{
    // св-ва для всех приложений
    protected static $obRedis;
    protected static $obLog;
    protected static $isLog;
    protected static $DUMP = [];
    protected static $DUMP_EVENTS = [];
    protected static $curLogger = '_all';
    protected static $request;
    protected static $capsule;
    protected static $additionalApiResponse = [];
    
    // св-ва проекта
    // ....
    
    public function __construct($isLog = true)
    {
        static::$isLog = $isLog;

        // создаем глобальное подключение: CONNECTION_MAIN
        if (!static::$capsule) {
            // на это соединение завязана модель ORM WebNow\Portal
            $capsule = new Capsule;
            $capsule->addConnection([
                'driver'    => 'mysql',
                'host'      => DB_HOST,
                'database'  => DB_NAME,
                'username'  => DB_USER,
                'password'  => DB_PASS,
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ], CONNECTION_MAIN);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
            static::$capsule = $capsule;
        }
    }
    
    function __destruct()
    {
        $this->dumpLogToFile();
    }

    /**
     * иногда мы создаем доп сущности во время запроса апи, нам нужно хранилище чтобы их возвращать
     *
     * @param $data
     */
    public static function setApiAdditionalResponse($data) {
        static::$additionalApiResponse[] = $data;
    }

    /**
     * иногда мы создаем доп сущности во время запроса апи, нам нужно хранилище чтобы их возвращать
     *
     * @return array
     */
    public static function getApiAdditionalResponse() {
        return static::$additionalApiResponse;
    }

    /**
     * @param $error
     *
     * @return string
     */
    public static function ErrorToHuman($error)
    {
        if (preg_match('/Integrity constraint violation: 1062/', $error)) {
            return 'Запись с таким именем уже существует!';
        }
        if (preg_match('/Warning: 1265 Data truncated for column/', $error)) {
            return 'Слишком длинное значение в одном из полей!';
        }
        if (preg_match('/Integrity constraint violation: 1452 Cannot add or update a child row/', $error)) {
            return 'Неверное значение ключа в одном из полей!';
        }
        if (preg_match('/Integrity constraint violation: 1451 Cannot delete or update a parent row: a foreign key constraint fails/', $error)) {
            return 'Нельзя удалить этот элемент, так он связан с одной или несколькими операциями!';
        }

        return '';
    }

    /**
     * наш объект для работы с БД
     * по умолчанию в нем подключение общеее для приложение
     * для работы с базами клиентов к нему доабвляется соединение по умолчанию addConnection (без параметра CONNECTION_MAIN)
     * см. примеры работы кода по приложению
     *
     * @return \Illuminate\Database\Capsule\Manager
     */
    public static function Capsule()
    {
        return static::$capsule;
    }
    
    /**
     * @return Request
     */
    public static function Request()
    {
        if (!static::$request) {
            static::$request = Request::createFromGlobals();
        }

        return static::$request;
    }
    
    /**
     * @return Redis
     */
    public static function Cache()
    {
        if (APP_USE_REDIS && !static::$obRedis) {
            static::$obRedis = new Redis;
        }

        /* @var Redis static::$obRedis */
        return static::$obRedis;
    }
   
    /**
     * @return mixed
     */
    public static function GetLog()
    {
        return static::$DUMP[static::$curLogger];
    }
    
    /**
     * сборщик логов App
     *
     * @param string $str
     */
    public static function Log($str = '')
    {
        static::addToLog(static::$DUMP, $str);
    }

    /**
     * сборщик логов Event
     *
     * @param string $str
     */
    public static function EventLog($str = '')
    {
        static::addToLog(static::$DUMP_EVENTS, $str);
    }

    /**
     * @param $arLog
     * @param $sData
     */
    private static function addToLog(&$arLog, $sData)
    {
        if (static::$isLog) {
            if (is_array($sData)) {
                $sData = print_r($sData, true);
            }

            $arLog[static::$curLogger] .= date('d-m-Y h:i:s') . ' ' . $sData . "<br>";
        }
    }

    /**
     * @param $sCurLog
     */
    public static function setLogger($sCurLog)
    {
        static::$curLogger = $sCurLog;
    }

    /**
     * пишет весь лог Event в файл разово
     */
    public static function dumpEventLogToFile()
    {
        static::writeLogToFile(static::$DUMP_EVENTS, LOG_FILE_EVENT);
    }

    /**
     * пишет весь лог App в файл разово
     */
    public static function dumpLogToFile()
    {
        static::writeLogToFile(static::$DUMP, LOG_FILE_APP);
    }

    /**
     * @param $arLog
     * @param $sFile
     */
    private static function writeLogToFile(&$arLog, $sFile)
    {
        if (static::$isLog) {

            foreach ($arLog as $sLogger => $sData) {
                if (!$sData) {
                    continue;
                }
                $sPath = str_replace('%MEMBER%', $sLogger, $sFile);

                // создаем папки автоматом
                $sDirName = dirname($sPath);
                if (!is_dir($sDirName)) {
                    mkdir($sDirName, 0755, true);
                }

                $DUMP_FILE = fopen($sPath, "a");
                $sAllLog = str_replace("<br>", "\n", $sData);
                fwrite($DUMP_FILE, $sAllLog . "\n");

                // закрываем файл
                fclose($DUMP_FILE);

                // обнулим лог
                $arLog[$sLogger]  = '';
            }

        }
    }
}