<?php

// блокируем всех кроме реальных тестировщиков
if ('y' !== $_REQUEST['showme']) {
    die;
}

// phpinfo();

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Carbon;
use WebNow\App;
use WebNow\B24App;
use WebNow\Cron;
use WebNow\Helpers;
use WebNow\Portal;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Model\B24Status;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Cost;
use WebNow\Project\Model\MonthlyTask;

require_once '../init/init.php';

// запуск миграции через www
// require_once '../database/migrations/2018_08_13_clients.php';
// $rsMigration->Up();
// echo 'OK';

// DATA LOAD TESTING
// $request = App::Request();
// $arPortal = PortalController::findByCode('3dad04db75217fba1d701fefaad94bb1'); // dkdev
// $arPortal = PortalController::findByCode('52b1a20ed2cdfd70a736a9106e38ea8a'); // rfop
// $obB24App = new B24App($arPortal);
// dump($obB24App);
// PortalController::addConnection($arPortal);

// $arData = B24Status::all()->toArray();
// dump($arData);

// открытые счета из Б24
// $arOpenInv = $obB24App->getInvoicesOpen();
// $arOpenInvMap = array_map(function ($item) {
//     return  $item['ACCOUNT_NUMBER'];
// }, $arOpenInv);
// dump($arOpenInvMap);



// cтатусы из Б24
// $arInvStatus = $obB24App->getStatusList(B24Status::STATUS_INVOICE);
// $arLeadStatus = $obB24App->getStatusList(B24Status::STATUS_LEAD);
// B24Status::updateByEntity(array_merge(
//     $obB24App->getStatusList(B24Status::STATUS_INVOICE),
//     $obB24App->getStatusList(B24Status::STATUS_LEAD)
// ));
// B24Status::updateByEntity($arLeadStatus);

// Компании
// $arB24Company = $obB24App->getMyCompanies();
// $arB24Company = $obB24App->getNotMyCompanies();
// $arB24Firm = $obB24App->getCompaniesById();
// $arCompany = Helpers::extractByField(...['ID', $arB24Firm]);
// $arB24Company = $obB24App->getRequisiteById($arCompany);
// dump($arB24Company);
//
// $arB24Contact = $obB24App->getContactsById();
// $arCompany = Helpers::extractByField(...['ID', $arB24Contact]);
// $arB24Company = $obB24App->getRequisiteById($arCompany);
// dump($arB24Company);
// $arB24Company = $obB24App->getRequisiteById();
// $coll = collect($arB24Company['data'])->groupBy(['ENTITY_TYPE_ID', 'ENTITY_ID'])->toArray();
// dump($coll);

// $arInvoice = $obB24App->getInvoices(...['2018-06-01', '2018-07-02']);
// dump($arInvoice);

// $arMT = MonthlyTask::all()->toArray();
// dump($arMT);

// Сотрудники
// $arEmployee = Helpers::extractByField(...['RESPONSIBLE_ID', $arInvoice]);
// $arEmployee = $obB24App->getUsers($arEmployee);
// dump($arEmployee);
// // Каналы
// $arFirm = Helpers::extractByField(...['UF_MYCOMPANY_ID', $arInvoice]);
// $arB24Firm = $obB24App->getMyCompanies($arFirm);
// dump($arB24Firm);
//
// // Контрагенты Компании
// $arCtrCompany = Helpers::extractByField(...['UF_COMPANY_ID', $arInvoice]);
// $arB24Company = $obB24App->getNotMyCompanies($arCtrCompany);
// dump($arB24Company);
//
// // Контрагенты Контакты
// $arCtrContact = Helpers::extractByField(...['UF_CONTACT_ID', $arInvoice]);
// $arB24Contact = $obB24App->getContacts($arCtrContact);
// dump($arB24Contact);




// KEY REFRESH TESTING
// $request = App::Request();
// $arPortal = PortalController::findByCode('256f43bb56b53b3dcc3648af3eea5461');
//
// $obB24App = new B24App($arPortal);
// dump('getTokenStatus = ');
// dump($obB24App->getTokenStatus());
//
// $obB24User = new \Bitrix24\User\User($obB24App->getB24());
// $arCurrentB24User = $obB24User->current();
//
// dump($arCurrentB24User['result']);






// 1. Install App DB
// $rsInstall = new \WebNow\Project\Install\AppInstall(App::Capsule());
// $rsInstall->Up();

// 2. UnInstall App DB
// $rsInstall->Down();

// Portal data stub
$arPortal = [
    'domain'        => 'dkdev.bitrix24.ru',
    // 'domain'        => 'portal_222.bitrix24.ru',
    'member_id'     => 'c10bf9453cbcca8025a8216884ea79f4',
    // 'member_id'     => 'c10bf9453cbcca8025a8216884ea79f4123',
    'access_token'  => '6b96225b0026d11c00262de400000001000003b62e7c129d01320acc0458cabbde0817',
    'refresh_token' => '5b154a5b0026d11c00262de40000000100000387cee807d446dde4f4ba380c41f5a431',
    'expires_in'    => '1528982571',

    // 'db_host'       => '192.168.1.34',
    // 'db_name'       => 'portal_222',
    // 'db_user'       => 'root',
    // 'db_pass'       => 'qqq111',
    // 'storeType'     => 'self',

    'db_host'       => null,
    'db_name'       => null,
    'db_user'       => null,
    'db_pass'       => null,
];

// 3. Install new portal
// $sSecretCode = (new \WebNow\Project\Install\PortalInstall($arPortal, App::Capsule()))->Up();
// dump($sSecretCode);

// 4. Uninstall
// dump((new \WebNow\Project\Install\PortalInstall($arPortal, App::Capsule()))->Down());


// $request = \WebNow\App::Request();
// dump($request);