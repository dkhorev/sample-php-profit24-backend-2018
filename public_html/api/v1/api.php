<?php
@set_time_limit(0);

use Symfony\Component\HttpFoundation\JsonResponse;
use WebNow\App;
use WebNow\B24App;
use WebNow\Helpers;
use WebNow\Project\Api\BillsFileImport;
use WebNow\Project\BillParser\TxtBillParser;
use WebNow\Project\Charts\ChartFormat;
use WebNow\Project\Charts\ChartJsFormat;
use WebNow\Project\Controller\ContractorController;
use WebNow\Project\Controller\CostController;
use WebNow\Project\Controller\EmployeeController;
use WebNow\Project\Controller\MonthlyTaskController;
use WebNow\Project\Controller\OperationController;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Controller\StatisticController;
use WebNow\Project\Data\BillLoader;
use WebNow\Project\Data\PortalDataLoader;
use WebNow\Project\File\YadiskLoader;
use WebNow\Project\Model\Operation;

require_once '../../../init/init.php';

$request = \WebNow\App::Request();

App::Log('API request');

$arParams = array_merge($request->query->all(), $request->request->all());

// Code - проверка на мусор
if (!preg_match('/^[a-z0-9]{32}$/', $arParams['code'])) {
    App::Log('API error: неверный формат кода');
    $response = new JsonResponse([
        'error'    => true,
        'msg'      => 'Неверный формат кода',
        'msgHuman' => 'Неверный формат кода',
    ]);
    $response->send();
    die;
}

// логируем параметры запроса
App::Log($arParams);

// ищем портал по коду
$arPortal = PortalController::findByCode($arParams['code']);

$arRes = [];
if (!$arPortal) {
    App::Log('API error: portal not found!');
    $arRes = ['error' => true, 'msg' => 'Портал не найден', 'msgHuman' => 'Портал не найден'];
} else {
    // ставим логгер по порталу
    App::setLogger($arPortal['domain']);
    App::Log('API request');
    App::Log($arParams);

    // добавим новое соединение с БД клиента
    PortalController::addConnection($arPortal);

    // data приходит в json
    if ($arParams['data']) {
        $arParams['data'] = json_decode($arParams['data'], true);
    }

    // запросы авторизации по ключу
    if ('verify' === $arParams['operation']) {
        // если дошли сюда - значит ключ валиден в целом
        $arRes = ['error' => false];
    }

    // ручное обновление статусов
    if ('update' === $arParams['operation'] && 'B24Status' === $arParams['model']) {
        $rsDataLoader = new PortalDataLoader(App::Capsule(), $arPortal, $arParams);
        $arRes = $rsDataLoader->loadAllStatuses();
    }

    // Запросы к статистике
    if ('Statistic' === $arParams['model']) {
        // определяем формат
        switch ($arParams['chartFormat']) {
            case 'ChartJs':
                $rsFormat = new ChartJsFormat($arParams);
                break;
        }

        // получаем данные и обрабатываем
        if (is_a($rsFormat,ChartFormat::class)) {
            $rsStats = new StatisticController(App::Capsule(), $arParams);
            $arStats = $rsStats->get();
            $arRes = $rsFormat->format($arStats);
        } else {
            $arRes = [
                'error'    => true,
                'msg'      => 'Неверный формат графика',
                'msgHuman' => 'Неверный формат графика',
            ];
        }
    }

    // Контроллер загрузки файлов в обработку
    if ('uploadFile' === $arParams['operation']) {
        $arFile = $request->files->get('file');
        $path = Helpers::getBillsPath($arPortal);
        /* @var SplFileInfo $copyFile */
        $copyFile = $arFile->move($path, $arFile->getPathname() .  '.' . $arFile->guessExtension());
        $arRes = (new BillsFileImport([$copyFile->getPathname()], $arPortal))->run();
        unlink($copyFile->getPathname());

        $arRes = current($arRes);
    }

    // Контроллер разбора счетов из папки Я.диск
    if ('parseYadiskBills' === $arParams['operation'] && $arParams['file']) {
        $rsLoader = new YadiskLoader(App::Capsule(), $arPortal, $arParams);
        // только новые файлы на парсинг
        try {
            $arFiles = $rsLoader->getNewFiles();
            // dump('$arFiles');
            // dump($arFiles);
            if (count($arFiles)) {
                $arRes = (new BillsFileImport($arFiles, $arPortal))->run();
                $rsLoader->updateParsed($arRes);
            }
        } catch (Exception $e) {
            $arRes = [
                'error'    => true,
                'msg'      => $e->getMessage(),
                'msgHuman' => $e->getMessage(),
            ];
        }

    }

    // Контроллер загрузки данных от портала
    if ('loadData' === $arParams['operation']) {
        $rsDataLoader = new PortalDataLoader(App::Capsule(), $arPortal, $arParams);

        switch ($arParams['step']) {
            case 0:
                $arRes = $rsDataLoader->loadAllStatuses();
                break;
            case 1:
                $arRes = $rsDataLoader->loadActiveEmployees($arParams['from']);
                break;
            case 2:
                $arRes = $rsDataLoader->runForInvoices([], $arParams['from']);
                break;
            case 3:
                $arRes = $rsDataLoader->runForLeads([], $arParams['from']);
                break;
        }
    }

    // Контроллер управления Ежемесячными операциями
    if (
        'MonthlyTask' === $arParams['model']
        && $arParams['operation']
        && ($arParams['month'] && $arParams['year'])
    ) {
        switch ($arParams['operation']) {
            // получение статуса за месяц (вернет (создаст если нету) - статус за выбранный месяц)
            case 'get':
                $arRes = (new MonthlyTaskController(App::Capsule(), $arParams))->getTaskStatus();
                break;
            // произвести некое задание из списка и вернуть текущий статус
            case 'make':
                $arRes = (new MonthlyTaskController(App::Capsule(), $arParams))->makeTask();
                break;
        }
    }

    // Контроллеры API (CRUD с фронта) - по  модели передаем управление нужному контроллеру
    if (isset($arParams['operation']) && $arParams['model']) {
        switch ($arParams['model']) {
            case 'PaytypeVariant':
                $arRes = PaytypeController::getPaytypeVariants();
                break;
            case 'Paytype':
                $arRes = (new PaytypeController)->handle($arParams);
                break;
            case 'Contractor':
                $arRes = (new ContractorController)->handle($arParams);
                break;
            case 'Employee':
                $arRes = (new EmployeeController)->handle($arParams);
                break;
            case 'Cost':
                $arRes = (new CostController)->handle($arParams);
                break;
            case 'Operation':
                $arRes = (new OperationController)->handle($arParams);
                break;
            case 'Option':
                $arRes = (new OperationController)->handle($arParams);
                break;
        }
    }
}

// дополнение основного ответа
$arRes = [
    'main'       => $arRes,
    'additional' => App::getApiAdditionalResponse(),
];

App::dumpLogToFile();

$response = new JsonResponse($arRes);
$response->send();
