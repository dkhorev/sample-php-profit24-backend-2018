<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use WebNow\App;
use WebNow\Project\Event\B24Event;

require_once '../../../../init/init.php';

$request = App::Request();
$arParams = array_merge($request->query->all(), $request->request->all());
App::EventLog($arParams);

// application_token - проверка на мусор и наличие
// https://dev.1c-bitrix.ru/rest_help/general/events/event_safe.php
if (!preg_match('/^[a-z0-9]{32}$/', $arParams['auth']['application_token'])) {
    $response = new JsonResponse(['error' => true, 'msg' => 'Неверный application_token', 'msgHuman' => 'Неверный application_token']);
    $response->send();
    die;
}
App::setLogger($arParams['auth']['domain']);
App::EventLog($arParams);

$arRes = ['error' => false];
try {
    $rsEventApi = new B24Event(App::Capsule(), $arParams);
    $rsEventApi->handle();
} catch (Exception $e) {
    $arRes = ['error' => true, 'msg' => $e->getMessage(), 'msgHuman' => App::ErrorToHuman($e->getMessage())];
    App::EventLog('Event API error:');
    App::EventLog($arRes);
}

App::dumpEventLogToFile();

$response = new JsonResponse($arRes);
$response->send();
