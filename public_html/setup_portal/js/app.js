const event_url = 'https://profit.web-now.run/api/v1/events/'

// App constructor
function App () {
  // const url = 'https://profit.web-now.run'
}

// начало инсталляции на портале
App.prototype.startInstallation = function () {
  let curapp = this

  // анимации и т.д.
  this.resetAnimInstallSteps()

  // 1 получаем параметры авторизации админа
  let data = BX24.getAuth()

  // 2 ставим тип операции
  data['operation'] = 'register_portal'

  // 3 добавляем данные из формы установки (если она есть)
  const formData = new FormData($('#setupForm')[0])
  for (let pair of formData.entries()) {
    data[pair[0]] = pair[1]
  }

  // console.log(data)

  $.ajax({
    url: 'portalSetup.php',
    data,
    type: 'POST',
    dataType: 'html',
    timeout: 60000,
    async: true,
    success: function (response) {
      const answer = JSON.parse(response)
      if (answer.result === false) {
        // ресет анимации и показ ошибки
        curapp.resetAnimOnError(answer.error)
      }
      else {
        curapp.finishAnimStep(0)
        curapp.saveSecretCode(answer.sSecretCode)
      }
    }
  })
}

// сохраняем на портале серкетный ключ который получили от нашего приложения
App.prototype.saveSecretCode = function (sSecretCode) {
  let curapp = this

  // чистим хранилище
  BX24.callMethod('entity.delete', {'ENTITY': 'profit_b24'},
    () => {
      // создаем хранилище
      BX24.callMethod('entity.add', {
          'ENTITY': 'profit_b24',
          'NAME': 'Хранилище ProfitB24',
          'ACCESS': {U1: 'W', AU: 'R'}
        },
        (response) => {
          if (true === response.answer.result) {
            // создаем ключ
            BX24.callMethod('entity.item.add', {
                ENTITY: 'profit_b24',
                NAME: 'sSecretCode',
                CODE: sSecretCode
              },
              (response) => {
                if (response.answer.result > 0) {
                  // проверяем  что ключ можно прочитать
                  BX24.callMethod('entity.item.get', {
                      ENTITY: 'profit_b24',
                      SORT: {},
                      FILTER: {'NAME': 'sSecretCode'}
                    },
                    (response) => {
                      if (response.answer.result.length > 0) {
                        // BX24.installFinish()

                        curapp.finishAnimStep(1)

                        // выясняем как клиент хотел загрузить данные
                        const shouldLoadData = $('input[name=shouldLoadData]:checked').val()

                        if ('N' === shouldLoadData) {
                          curapp.finishAnimStep(2)
                          curapp.registerEvents()
                        } else {
                          curapp.loadData(sSecretCode)
                        }
                      }
                    })
                }
              }) // response 3
          }
        }) // response 2
    })// response 1
}

// дата лоадер для клиента
App.prototype.loadData = function (sSecretCode) {
  const curapp = this
  let step = 0 // шаги скачивания. 0 - активные сотрудники, 1 - счета, 2 - лиды
  let from = 0 // с какого элемента забирать

  // 1 ставим тип операции
  let data = {}
  data['operation'] = 'loadData'

  // 2 добавляем данные из формы установки (если она есть)
  const formData = new FormData($('#setupForm')[0])
  for (let pair of formData.entries()) {
    data[pair[0]] = pair[1]
  }

  // 3 используем код
  data['code'] = sSecretCode

  // debug only - skip loading
  // setTimeout(function () {
  //   curapp.finishAnimStep(3)
  //   curapp.registerEvents()
  // }, 500)
  // return
  // debug only - skip loading

  makeNextRequest()

  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async function makeNextRequest(delay = 2000)
  {
    await sleep(delay);
    // данные очередного запроса
    data['step'] = step
    data['from'] = from

    // таймер
    let start_time = new Date().getTime();

    $.ajax({
      url: '../api/v1/',
      data,
      type: 'POST',
      dataType: 'html',
      timeout: 60000,
      async: true,
      success: function (response) {
        const answer = JSON.parse(response).main
        // console.log(answer)

        // let request_time = new Date().getTime() - start_time;
        // let delay_time = 2000 - request_time
        // let delay_time = 2000
        // console.log(request_time)

        if (answer.result === false) {
          // ресет анимации и показ ошибки
          curapp.resetAnimOnError(answer.error)
        }
        else {
          curapp.updateProgress(step, answer.next, answer.total)

          // продолжаем в текущем шаге
          if (answer.next !== null) {
            from = answer.next
            makeNextRequest()
          } else {
            // поднять шаг, сбросить last_id
            step++
            from = 0

            if (step > 3) {
              // конец установки приложения
              curapp.finishAnimStep(2)
              curapp.registerEvents()
            } else {
              makeNextRequest()
            }
          }

        }
      }
    })
  }

}

// регистрация нужных обработчиков
App.prototype.registerEvents = function () {
  const curapp = this
  const eventList = [
    'OnAppInstall',
    'OnAppUninstall',
    'onCrmCompanyAdd',
    'onCrmCompanyDelete',
    'onCrmCompanyUpdate',
    'onCrmContactAdd',
    'onCrmContactDelete',
    'onCrmContactUpdate',
    'onCrmInvoiceAdd',
    'onCrmInvoiceDelete',
    'onCrmInvoiceUpdate',
    'onCrmInvoiceSetStatus',
    'onCrmLeadAdd',
    'onCrmLeadDelete',
    'onCrmLeadUpdate',
  ]

  const batch_unbind = eventList.map(event_name => {
    // console.log(event_name)
    return {
      method: 'event.unbind',
      params: {
        event: event_name,
        handler: event_url
      }
    }
  })

  BX24.callBatch(
    batch_unbind,
    (res) => {
      // console.log(res)
      batchBind()
    })

  function batchBind () {
    const batch = eventList.map(event_name => {
      // console.log(event_name)
      return {
        method: 'event.bind',
        params: {
          event: event_name,
          handler: event_url
        }
      }
    })
    // console.log(batch)

    BX24.callBatch(
      batch,
      (response) => {
        // console.log(response)
        // проверим что все события зарегились удачно
        let errors = ''
        response.forEach(res => {
          // console.log(res)
          // console.log(res.answer.result)
          if (res.answer.error) {
            errors += res.answer.error.error_description + '<br>'
          }
        })

        if (errors.length) {
          curapp.resetAnimOnError(errors)
        } else {
          curapp.finishAnimStep(3)
          setTimeout(() => {
            BX24.installFinish()
          }, 500)
        }
      })
  }
}

// ** ANIMATIONS **
App.prototype.resetAnimInstallSteps = () => {
  $('#error').hide()

  $('#save-btn').
    find('i').
    removeClass('fa-check').
    addClass('fa-spinner').
    addClass('fa-spin')

  $('.card').hide();
  $('#card-progress').show();

  const steps = $('.installStep');
  steps.
    addClass('fa-spinner').
    addClass('fa-spin').
    removeClass('fa-check')
}

App.prototype.resetAnimOnError = (error) => {
  $('#error').html(error).show()
  $('#save-btn').
    find('i').
    addClass('fa-check').
    removeClass('fa-spinner').
    removeClass('fa-spin')

  $('.card').show();
  $('#card-progress').hide();
}

App.prototype.finishAnimStep = (step) => {
  $('.installStep[data-step='+step+']').
    removeClass('fa-spinner').
    removeClass('fa-spin').
    addClass('fa-check')
}

App.prototype.updateProgress = (step, next, total) => {
  if (!total) {
    total = 1
  }
  if (!next) {
    next = total
  }
  next = parseInt(next)
  total = parseInt(total)

  let id_bar = '#status_progress'
  let id_perc = '#status_perc'

  switch (step) {
    case 0:
      id_bar = '#status_progress'
      id_perc = '#status_perc'
      break;
    case 1:
      id_bar = '#employee_progress'
      id_perc = '#employee_perc'
      break;
    case 2:
      id_bar = '#bill_progress'
      id_perc = '#bill_perc'
      break;
    case 3:
      id_bar = '#lead_progress'
      id_perc = '#lead_perc'
      break;
  }

  const elBar = $(id_bar)
  const elPerc = $(id_perc)

  let percent = 0
  if (total <= 0 || !next) {
    percent = 100
  } else {
    percent = parseInt(next / total * 100)
    elBar.css('width', percent + '%')
    elPerc.html(percent)
  }
}
