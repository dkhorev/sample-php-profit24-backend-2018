<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Установка приложения "Профит24 Бета"</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="./css/custom.css" rel="stylesheet" crossorigin="anonymous">
</head>

<body>
<div id="app" class="container-fluid">
    <h1>Установка приложения "Профит24 Бета"</h1>
    
    <form id="setupForm" class="my-3">

        <div class="card my-1 w-36rem">
            <div class="card-body">
                <h5 class="card-title">Загрузка сделок в приложение</h5>
                <h6 class="card-subtitle mb-2 text-muted">После установки приложения все новые сделки будут добавляться автоматически</h6>
                <div class="card-text">
                    <div class="form-check">
                        <input class="form-check-input shouldLoadData" type="radio" name="shouldLoadData" id="shouldLoadData1" value="N">
                        <label class="form-check-label" for="shouldLoadData1">
                            Использовать демо данные приложения
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input shouldLoadData" type="radio" name="shouldLoadData" id="shouldLoadData2" value="Y" checked>
                        <label class="form-check-label" for="shouldLoadData2">
                            Загрузить сделки, контрагентов и сотрудников с моего портала
                        </label>
                    </div>
                    <div id="dateSetup" style="" class="my-1">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="dateFrom">С</label>
                                <input type="date" class="form-control" name="dateFrom" id="dateFrom">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="dateTo">По</label>
                                <input type="date" class="form-control" name="dateTo" id="dateTo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card my-1 w-36rem">
            <div class="card-body">
                <h5 class="card-title">Выберите где будем хранить данные</h5>
                <div class="card-text">
                    <div class="form-check">
                        <input class="form-check-input storeType" type="radio" name="storeType" id="storeType1" value="app" checked>
                        <label class="form-check-label" for="storeType1">
                            Хранить данные в БД приложения
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input storeType" type="radio" name="storeType" id="storeType2" value="self">
                        <label class="form-check-label" for="storeType2">
                            Хранить данные в своей БД
                        </label>
                    </div>
                    <div id="bdSetup" style="display: none;" class="my-1">
                        <div class="form-group">
                            <label for="DB_HOST">Сервер</label>
                            <input type="text" class="form-control form-control-sm" id="DB_HOST" name="db_host">
                            <small id="DB_HOST" class="form-text text-muted">IP адрес сервера с БД</small>
                        </div>
                        <div class="form-group">
                            <label for="DB_NAME">База данных</label>
                            <input type="text" class="form-control form-control-sm" id="DB_NAME" name="db_name">
                            <small id="DB_NAME" class="form-text text-muted">База данных MYSQL</small>
                        </div>
                        <div class="form-group">
                            <label for="DB_USER">Пользователь</label>
                            <input type="text" class="form-control form-control-sm" id="DB_USER" name="db_user">
                            <small id="DB_NAME" class="form-text text-muted">Необходимы полные права на работу БД</small>
                        </div>
                        <div class="form-group">
                            <label for="DB_PASS">Пароль</label>
                            <input type="password" class="form-control form-control-sm" id="DB_PASS" name="db_pass">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row w-36rem" id="card-progress" style="display: none">
            <ul class="list-group">
                <li class="list-group-item">
                    <i class="fa fa-check text-success installStep" data-step="0"></i>
                    Проверка данных установки
                </li>
                <li class="list-group-item">
                    <i class="fa fa-check text-success installStep" data-step="1"></i>
                    Генерация и запись секретного ключа
                </li>
                <li class="list-group-item">
                    <i class="fa fa-check text-success installStep" data-step="2"></i>
                    <span>Загрузка данных из Bitirx24</span>
                    <div class="progress my-2">
                        <div class="progress-bar" id="status_progress" role="progressbar" style="width: 0;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            <span>Загрузка статусов <span id="status_perc">0</span>%</span>
                        </div>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar" id="employee_progress" role="progressbar" style="width: 0;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            <span>Загрузка сотрудников <span id="employee_perc">0</span>%</span>
                        </div>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar" id="bill_progress" role="progressbar" style="width: 0;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            <span>Загрузка счетов <span id="bill_perc">0</span>%</span>
                        </div>
                    </div>
                    <div class="progress mb-2">
                        <div class="progress-bar" id="lead_progress" role="progressbar" style="width: 0;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            <span>Загрузка лидов <span id="lead_perc">0</span>%</span>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <i class="fa fa-check text-success installStep" data-step="3"></i>
                    Регистрация событий CRM
                </li>
            </ul>
        </div>

    </form>
    

    <div class="alert alert-dismissable alert-warning" style="display: none" id="error"></div>
    <div class="my-3">
        <a href="#" id="save-btn" class="btn btn-success btn-raised">
            <i class="fa fa-check"></i> Установить
            <div class="ripple-wrapper"></div>
        </a>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

<script type="text/javascript" src="js/app.js?<?=md5(date('d.m.y H:i:s'))?>"></script>
<script src="//api.bitrix24.com/api/v1/"></script>

<script>
// create our App
const app = new App()

$(document).ready(function () {

  $('#save-btn').on('click', (e) => {
    e.preventDefault()
    app.startInstallation()
  })

  $('.storeType').on('change', function () {
    const bdSetup = $('#bdSetup')
    if ('app' === $(this).val()) {
      bdSetup.hide()
    } else {
      bdSetup.show()
    }
  })

  $('.shouldLoadData').on('change', function () {
    const dateSetup = $('#dateSetup')
    if ('N' === $(this).val()) {
      dateSetup.hide()
    } else {
      dateSetup.show()
    }
  })

  BX24.init(function () {
    // app.saveFrameWidth()
  })
})

</script>
</body>
</html>