<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use WebNow\App;
use WebNow\B24App;
use WebNow\Project\Install\PortalInstall;

require_once '../../init/init.php';

$request = App::Request();
$arParams = $request->request->all();
// dump($arParams);
// die;


if ('POST' === $request->getMethod() && isset($arParams['operation'])) {

    switch ($arParams['operation']) {
        case 'register_portal':

            App::Log('portalSetup request:');
            App::Log($arParams);

            // если у них свое хранилище, мы должны убедиться что к нему есть доступ и права на создание своих таблиц
            if ('self' === $arParams['storeType']) {
                $arResult = (new PortalInstall($arParams, App::Capsule()))->ValidateCustomConnection();

                // при ошибке вернем на фронт описание
                if (false === $arResult['result']) {
                    $response = new JsonResponse($arResult);
                    $response->send();
                    die;
                }
                // dump($arResult);
            }

            // init b24app
            $obB24App = new B24App($arParams, false);

            // в первый раз мы проверяем работу API по существованию юзера
            $obB24User = new \Bitrix24\User\User($obB24App->getB24());
            $arCurrentB24User = $obB24User->current();

            // сохраняем данные о компании и делаем первую обработку ее данных
            if ($arCurrentB24User['result']['ID']) {
                // dump($arCurrentB24User['result']);

                // устанавливаем и вернем cекретный ключ приложению
                $sSecretCode = (new PortalInstall($arParams, App::Capsule()))->Up();

                // вернем инсталлеру success
                $response = new JsonResponse(['status' => 'success', 'result' => '', 'sSecretCode' => $sSecretCode]);
                $response->send();
                // die;
            }
    }
}
