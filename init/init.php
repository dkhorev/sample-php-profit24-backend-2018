<?php

use WebNow\App;

// Constants
require_once __DIR__ . '/../config/constants.php';

// Composer
require_once __DIR__ . '/../vendor/autoload.php';

// Подключаем .env с настройками
require_once __DIR__ . '/../config/env.php';

// Init App
(new App);

