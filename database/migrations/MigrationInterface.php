<?php

namespace WebNow\Migrations;

interface MigrationInterface
{
    public function Up();
    public function Down();
}