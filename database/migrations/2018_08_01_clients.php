<?php

namespace WebNow\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на базы данных всех клиентов
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            if (!$schema->hasTable('b24_statuses')) {
                $schema->create('b24_statuses', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->charset = 'utf8';
                    $table->collation = 'utf8_unicode_ci';

                    $table->increments('id');
                    $table->unsignedInteger('b24_id');
                    $table->string('entity', 20); // STATUS_INVOICE = 14 chars
                    $table->unsignedInteger('sort');
                    $table->string('status_id', 255);
                    $table->tinyInteger('group');

                    $table->timestamps();
                });
            }
            // [Пишем миграцию здесь]
            
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            $schema->dropIfExists('b24_statuses');
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
