<?php

namespace WebNow\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на базы данных всех клиентов
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            $schema->table('statistics', function (Blueprint $table) {
                $table->dropForeign('statistics_firm_id_foreign');
                $table->dropIndex('statistics_firm_id_foreign');
                $table->dropColumn('firm_id');
            });
            $schema->table('operations', function (Blueprint $table) {
                $table->dropForeign('operations_firm_id_foreign');
                $table->dropIndex('operations_firm_id_foreign');
                $table->dropColumn('firm_id');
            });

            $schema->table('statistics', function (Blueprint $table) {
                $table->unsignedInteger('firm_id')->after('name');
                $table->foreign('firm_id')->references('id')->on('contractors')->change();
            });
            $schema->table('operations', function (Blueprint $table) {
                $table->unsignedInteger('firm_id')->after('paytype_id');
                $table->foreign('firm_id')->references('id')->on('contractors')->change();
            });

            $schema->dropIfExists('firms');
            // [Пишем миграцию здесь]
            
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            if (!$schema->hasTable('firms')) {
                $schema->create('firms', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->charset = 'utf8';
                    $table->collation = 'utf8_unicode_ci';

                    $table->increments('id');
                    $table->string('name', 255);
                    $table->string('code', 255)->unique();
                    $table->unsignedInteger('b24_id')->nullable(true);
                    $table->timestamps();
                });
            }
            $schema->table('statistics', function (Blueprint $table) {
                $table->dropForeign('statistics_firm_id_foreign');
                $table->dropIndex('statistics_firm_id_foreign');
                $table->dropColumn('firm_id');
            });
            $schema->table('operations', function (Blueprint $table) {
                $table->dropForeign('operations_firm_id_foreign');
                $table->dropIndex('operations_firm_id_foreign');
                $table->dropColumn('firm_id');
            });
            $schema->table('statistics', function (Blueprint $table) {
                $table->unsignedInteger('firm_id')->after('name');
                $table->foreign('firm_id')->references('id')->on('firms')->change();
            });
            $schema->table('operations', function (Blueprint $table) {
                $table->unsignedInteger('firm_id')->after('paytype_id');
                $table->foreign('firm_id')->references('id')->on('firms')->change();
            });
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
