<?php

namespace WebNow\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на базы данных всех клиентов
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            if (!$schema->hasTable('monthly_tasks')) {
                $schema->create('monthly_tasks', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->charset = 'utf8';
                    $table->collation = 'utf8_unicode_ci';

                    $table->increments('id');
                    $table->tinyInteger('month');
                    $table->smallInteger('year');
                    $table->boolean('open_bills')->default(false);
                    $table->boolean('open_leads')->default(false);
                    $table->boolean('wage_50')->default(false);
                    $table->boolean('wage_100')->default(false);
                    $table->boolean('fixed_costs')->default(false);

                    $table->timestamps();
                });
            }
            // [Пишем миграцию здесь]
            
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            if ($schema->hasTable('monthly_tasks')) {
                $schema->drop('monthly_tasks');
            }
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
