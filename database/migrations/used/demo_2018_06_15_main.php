<?php

namespace WebNow\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на основную БД
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makeMainMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            $schema->table('portals', function (Blueprint $table) use ($schema) {
                // пример проверки сушествования поля
                if (!$schema->hasColumn('portals', 'test')) {
                    // $table->string('test', 2);
                }
            });
            // [Пишем миграцию здесь]
        
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makeMainMigration(function (Builder $schema) {
            
            // [Пишем миграцию здесь]
            $schema->table('portals', function (Blueprint $table) {
                // $table->dropColumn('test');
            });
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
