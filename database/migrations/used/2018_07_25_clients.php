<?php

namespace WebNow\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на базы данных всех клиентов
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            if (!$schema->hasTable('statistics')) {
                $schema->create('statistics', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->charset = 'utf8';
                    $table->collation = 'utf8_unicode_ci';

                    $table->increments('id');
                    $table->year('year');
                    $table->tinyInteger('type'); // тип 1 - недельный, 2 - месячный, 3 - дневной
                    $table->date('label_date')->useCurrent(); // дата для лейбла (недельный - начало недели, месячный - начало месяца, годовой - ...)
                    $table->text('metric'); // разрезы статисткики, через зпт типы платежей
                    $table->double('summ', 20, 2)->default(0);

                    $table->timestamps();
                });
            }
            // [Пишем миграцию здесь]
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            $schema->dropIfExists('statistics');
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
