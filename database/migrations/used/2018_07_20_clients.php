<?php

namespace WebNow\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

/**
 * пример миграции на базы данных всех клиентов
 * https://laravel.com/docs/5.6/migrations#creating-columns
 * https://laravel.com/docs/5.6/migrations#modifying-columns
 * https://laravel.com/docs/5.6/migrations#creating-tables
 *
 * Class Migration
 *
 * @package WebNow\Migrations
 */
class Migration extends MigrationBase implements MigrationInterface
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function Up()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            // !!! https://github.com/laravel/framework/issues/7298
            /// !!! на double поля не работает change() в момент написания миграции
            // $schema->table('costs', function (Blueprint $table) {
            //     $table->double('price', 10, 2)->change();
            // });
            $schema->getConnection()->getPdo()->exec('ALTER TABLE costs MODIFY COLUMN price double(10,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE employees MODIFY COLUMN wage_real double(10,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE employees MODIFY COLUMN wage double(10,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE operations MODIFY COLUMN summ double(16,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE rois MODIFY COLUMN revenue double(16,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE rois MODIFY COLUMN costs double(16,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE statistics MODIFY COLUMN summ double(16,2) NOT NULL DEFAULT 0');
            // [Пишем миграцию здесь]
            
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function Down()
    {
        $this->makePortalMigration(function (Builder $schema) {
    
            // [Пишем миграцию здесь]
            $schema->getConnection()->getPdo()->exec('ALTER TABLE costs MODIFY COLUMN price double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE employees MODIFY COLUMN wage_real double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE employees MODIFY COLUMN wage double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE operations MODIFY COLUMN summ double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE rois MODIFY COLUMN revenue double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE rois MODIFY COLUMN costs double(8,2) NOT NULL DEFAULT 0');
            $schema->getConnection()->getPdo()->exec('ALTER TABLE statistics MODIFY COLUMN summ double(8,2) NOT NULL DEFAULT 0');
            // [Пишем миграцию здесь]
        
        });
    }
}

$rsMigration = new Migration;
