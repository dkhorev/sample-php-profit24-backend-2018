<?php

namespace WebNow\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use WebNow\App;
use WebNow\Portal;

abstract class MigrationBase
{
    /**
     * @var array $arData
     *
     * @return Capsule
     */
    private function getConnection(array $arData)
    {
        $capsule = new Capsule;
        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => $arData['db_host'] ? : DB_HOST,
            'database'  => $arData['db_name'] ? : '',
            'username'  => $arData['db_user'] ? : DB_USER,
            'password'  => $arData['db_pass'] ? : DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);
        // Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();
        
        return $capsule;
    }
    
    /**
     * @param $callable
     */
    protected function makePortalMigration($callable) {
        $arPortals = Portal::all();
    
        App::Log('Migrating portals...');
        
        foreach ($arPortals as $arPortal) {
            $arData = $arPortal->getAttributes();
            
            if (count($arData)) {
                App::Log('Migrating portal: ' . $arData['domain']);
                $capsule = $this->getConnection($arData);
                $schema = $capsule->schema();
                
                $callable($schema);
            }
        }
        App::Log('OK');
    }
    
    /**
     * @param $callable
     */
    protected function makeMainMigration($callable) {
        App::Log('Migrating MAIN portal...');
        $callable(Capsule::schema(CONNECTION_MAIN));
        App::Log('OK');
    }
}