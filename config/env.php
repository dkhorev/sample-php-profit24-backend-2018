<?php
try {
    $dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
    $dotenv->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    echo $e->getMessage();
    die;
}

if (
    !getenv('DB_HOST')
    || !getenv('DB_NAME')
    || !getenv('DB_USER')
    || !getenv('APP_SECRET_CODE')
) {
    throw new Exception('!!! Внесите настройки в файл .env !!!');
}

// конфиг бд
define('DB_HOST', getenv('DB_HOST'));
define('DB_PORT', getenv('DB_PORT'));
define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));

// настройки Redis по умолчанию
define('DEFAULT_REDIS_CONNECT', getenv('DEFAULT_REDIS_CONNECT'));
define('DEFAULT_REDIS_IP', getenv('DEFAULT_REDIS_IP'));
define('DEFAULT_REDIS_PORT', getenv('DEFAULT_REDIS_PORT'));
define('DEFAULT_REDIS_PREFIX', getenv('DEFAULT_REDIS_PREFIX'));

// секретный код для генерации ключей приложения
define('APP_SECRET_CODE', getenv('APP_SECRET_CODE'));