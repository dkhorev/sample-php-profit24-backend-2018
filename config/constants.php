<?php

define('APP_ROOT', __DIR__ . '/../');

// КОСНТАНТЫ ПРОЕКТА
define('CLIENT_ID', 'app.XXX.YYY'); // из настроек MP Код приложения (client_id)
define('CLIENT_SECRET', ''); // из настроек MP Секретный ключ (client_secret)
define('APP_REG_URL', 'https://profit.test.run/setup_portal/');

// основное соединение проекта, БД где порталы
define('CONNECTION_MAIN', 'MAIN');

// задержка перед батч запросами б24 апи
define('BATCH_DELAY', 2000);
define('BATCH_LIMIT', 50);

// использует ли приложение Redis
define('APP_USE_REDIS', false);

// файл лога апи Б24
define('LOG_FILE', APP_ROOT . 'app_logs/b24api/' . date('Y-m-d-H') . '.log');

// файл лога нашего приложения
define('LOG_FILE_APP', APP_ROOT . 'app_logs/app/%MEMBER%/' . date('Y-m-d') . '.log');
define('LOG_FILE_EVENT', APP_ROOT . 'app_logs/events/%MEMBER%/' . date('Y-m-d') . '.log');

// путь до папки со счетами
define('BILLS_PATH', APP_ROOT . 'upload/yadisk_bills/');

// файл лога агента обновляющего данные (UpdateDataSimpleAgent::class)
// define('LOG_FILE_UPD', APP_ROOT . 'app_logs/update/' . date('Y-m-d-H') . '.log');
