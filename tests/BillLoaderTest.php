<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Data\BillLoader;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Operation;

class BillLoaderTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;
    private $arFirms;
    private $arCtrs;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента

        // демо данные
        // наши фирмы
        $this->arFirms = [
            ['name' => 'Firm1', 'is_company' => true, 'is_my' => true, 'inn' => 77341],
            ['name' => 'Firm2', 'is_company' => true, 'is_my' => true, 'inn' => 77342],
            ['name' => 'Firm3', 'is_company' => true, 'is_my' => true, 'inn' => 77343],
        ];
        // чужие фирмы
        $this->arCtrs = [
            ['name' => 'Ctr1', 'is_company' => true, 'is_my' => false, 'inn' => 99341],
            ['name' => 'Ctr2', 'is_company' => true, 'is_my' => false, 'inn' => 99342],
            ['name' => 'CtrContact3', 'is_company' => false, 'is_my' => false, 'inn' => 99343],
        ];
        foreach ($this->arFirms as $ar) {
            Contractor::query()->firstOrCreate($ar);
        }
        foreach ($this->arCtrs as $ar) {
            Contractor::query()->firstOrCreate($ar);
        }
        // dump(Contractor::all()->toArray());
    }

    /** @test */
    function it_can_be_instanced()
    {
        $bl = new BillLoader([], $this->arPortal);
        $this->assertInstanceOf(BillLoader::class, $bl);
    }

    /** @test */
    function it_can_add_cost_operations()
    {
        $arDemoData = [
            [
                "bank_id"   => 1,
                "summ"      => 100,
                "date_cost" => "2018-07-01",
                "from_inn"  => "INN1",
                "to_inn"    => "INN2",
                "desc"      => "Оплата по договору возмездного оказания услуг.НДС не облагается",
            ],
            [
                "bank_id"   => 2,
                "summ"      => 200,
                "date_cost" => "2018-07-02",
                "from_inn"  => "INN3",
                "to_inn"    => "INN4",
                "desc"      => "Оплата по договору возмездного оказания услуг.НДС не облагается",
            ],
        ];

        $bl = new BillLoader($arDemoData, $this->arPortal);
        $bl->load();

        $arOps = Operation::all()->toArray();
        $this->assertCount(2, $arOps);
        // dump($arOps);

        foreach ($arOps as $key => $arOp) {
            $arExpected = $arDemoData[$key];
            $this->assertEquals($arExpected['summ'], $arOp['summ']);
            $this->assertEquals($arExpected['bank_id'], $arOp['bank_id']);
            $this->assertEquals($arExpected['date_cost'], $arOp['date']);
            $this->assertEquals($arExpected['firm_id'], null);
            $this->assertEquals($arExpected['contractor_id'], null);
            $this->assertEquals($arExpected['employee_id'], null);
            $this->assertEquals($arExpected['manager_id'], null);
            $this->assertEquals($arExpected['b24_id'], null);
        }
    }

    /** @test */
    function it_can_add_revenue_operations()
    {
        $arDemoData = [
            [
                "bank_id"      => 1,
                "summ"         => 100,
                "date_revenue" => "2018-07-01",
                "from_inn"     => "INN1",
                "to_inn"       => "INN2",
                "desc"         => "Оплата по договору возмездного оказания услуг.НДС не облагается",
            ],
            [
                "bank_id"      => 2,
                "summ"         => 200,
                "date_revenue" => "2018-07-02",
                "from_inn"     => "INN3",
                "to_inn"       => "INN4",
                "desc"         => "Оплата по договору возмездного оказания услуг.НДС не облагается",
            ],
        ];

        $bl = new BillLoader($arDemoData, $this->arPortal);
        $bl->load();

        $arOps = Operation::all()->toArray();
        $this->assertCount(2, $arOps);
        // dump($arOps);

        foreach ($arOps as $key => $arOp) {
            $arExpected = $arDemoData[$key];
            $this->assertEquals($arExpected['summ'], $arOp['summ']);
            $this->assertEquals($arExpected['bank_id'], $arOp['bank_id']);
            $this->assertEquals($arExpected['date_revenue'], $arOp['date']);
            $this->assertEquals($arExpected['firm_id'], null);
            $this->assertEquals($arExpected['contractor_id'], null);
            $this->assertEquals($arExpected['employee_id'], null);
            $this->assertEquals($arExpected['manager_id'], null);
            $this->assertEquals($arExpected['b24_id'], null);
        }
    }

    /** @test */
    function it_will_update_operation_by_b24_account_to_revenue()
    {
        $arFirm = Contractor::query()->where('is_my', true)->get()->toArray();
        $arCtr = Contractor::query()->where('is_my', false)->get()->toArray();
        // dump($arCtr);
        $idExpRev = PaytypeController::getFirstExpectedRevenue()['id'];
        $idRev = PaytypeController::getFirstRevenue()['id'];
        $arSum = [100, 200, 300, 400, 500];
        $dateCreated = '2018-08-06';
        $datePayed = '2018-08-07';

        // счета и их названия, а также карта соответствия их Б24 (кол-во в обоих массивах должно совпадать)
        $arB24Id = [1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900];
        $arB24AccNum = [1100, 'M-1200', 1300, 'XT-1500/1', 1500, 1600, 1700, 'TG1800', 1900];
        $arInvOpenMap = [];
        foreach ($arB24Id as $key => $id) {
            $arInvOpenMap[$id] = $arB24AccNum[$key];
        }

        // создадим операции ожидаемого прихода
        $arOps = [];
        foreach ($arB24AccNum as $key => $b24_accnum) {
            $arOps[$b24_accnum] = [
                'paytype_id'    => $idExpRev,
                'firm_id'       => $arFirm[array_rand($arFirm)]['id'],
                'contractor_id' => $arCtr[array_rand($arCtr)]['id'],
                'summ'          => $arSum[array_rand($arSum)],
                'b24_id'        => $arB24Id[$key],
                'date'          => $dateCreated,
            ];
        }
        // dump($arOps);
        Operation::query()->insert($arOps);
        // dump(Operation::all()->toArray());

        // создаем оплату счетов
        $arBillsPayed = [];
        foreach ($arB24AccNum as $key => $b24_accnum) {
            $firm = array_filter($arFirm, function ($itm) use ($arOps, $b24_accnum) {
                return $arOps[$b24_accnum]['firm_id'] === $itm['id'];
            });
            $ctr = array_filter($arCtr, function ($itm) use ($arOps, $b24_accnum) {
                return $arOps[$b24_accnum]['contractor_id'] === $itm['id'];
            });

            $arBillsPayed[] = [
                "bank_id"      => $key + 1,
                "summ"         => $arOps[$b24_accnum]['summ'],
                "date_revenue" => $datePayed,
                "from_inn"     => end($ctr)['inn'],
                "to_inn"       => end($firm)['inn'],
                "desc"         => 'Оплата по счету ' . $b24_accnum . ' от 19.07.2018 за доработку веб проекта. НДС не облагается.',
            ];
        }
        // dump($arBillsPayed);
        // dump($idRev);

        // загрузим оплату счетов
        $bl = new BillLoader($arBillsPayed, $this->arPortal, $arInvOpenMap);
        $bl->load(false);

        // проверяем результаты
        $arResult = Operation::all()->toArray();
        // dump($arResult);
        $this->assertCount(count($arB24Id), $arResult);
        foreach ($arResult as $ar) {
            $this->assertEquals($idRev, $ar['paytype_id']);
        }
    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
