<?php


use WebNow\Project\Model\Option;
use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Data\BillLoader;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\Contractor;
use WebNow\Project\Model\Operation;

class OptionTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;
    private $arFirms;
    private $arCtrs;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента

        // демо данные
        // наши фирмы
        $this->arFirms = [
            ['name' => 'Firm1', 'is_company' => true, 'is_my' => true, 'inn' => 77341],
            ['name' => 'Firm2', 'is_company' => true, 'is_my' => true, 'inn' => 77342],
            ['name' => 'Firm3', 'is_company' => true, 'is_my' => true, 'inn' => 77343],
        ];
        // чужие фирмы
        $this->arCtrs = [
            ['name' => 'Ctr1', 'is_company' => true, 'is_my' => false, 'inn' => 99341],
            ['name' => 'Ctr2', 'is_company' => true, 'is_my' => false, 'inn' => 99342],
            ['name' => 'CtrContact3', 'is_company' => false, 'is_my' => false, 'inn' => 99343],
        ];
        foreach ($this->arFirms as $ar) {
            Contractor::query()->firstOrCreate($ar);
        }
        foreach ($this->arCtrs as $ar) {
            Contractor::query()->firstOrCreate($ar);
        }
        // dump(Contractor::all()->toArray());
    }

    /** @test */
    function it_can_set_option()
    {
        Option::set('KEY1', 'VALUE1');
        $ar = Option::all()->toArray();
        $this->assertCount(1, $ar);
        $this->assertContains(['key' => 'KEY1', 'value' => 'VALUE1', 'id' => 1], $ar);

        Option::set('KEY2', 'VALUE2');
        $ar = Option::all()->toArray();
        $this->assertCount(2, $ar);
        $this->assertContains(['key' => 'KEY1', 'value' => 'VALUE1', 'id' => 1], $ar);
        $this->assertContains(['key' => 'KEY2', 'value' => 'VALUE2', 'id' => 2], $ar);
    }
    
    /** @test */
    function it_can_get_option()
    {
        Option::set('KEY1', 'VALUE1');
        Option::set('KEY2', 'VALUE2');
        $test = Option::get('KEY1');
        $this->assertEquals('VALUE1', $test);
        $test = Option::get('KEY2');
        $this->assertEquals('VALUE2', $test);
    }

    /** @test */
    function it_will_return_null_if_has_no_such_key_value()
    {
        $test = Option::get('KEY1');
        $this->assertEquals(null, $test);
    }

    /** @test */
    function it_can_overwrite_key()
    {
        Option::set('KEY1', 'VALUE1');
        Option::set('KEY1', 'VALUE2');
        $test = Option::get('KEY1');
        $this->assertEquals('VALUE2', $test);
    }

    /** @test */
    function it_can_delete_existing_option()
    {
        Option::set('KEY1', 'VALUE1');
        Option::del('KEY1');
        $test = Option::get('KEY1');
        $this->assertEquals(null, $test);
    }

    /** @test */
    function it_can_delete_non_existing_option()
    {
        Option::del('KEY1');
        $test = Option::get('KEY1');
        $this->assertEquals(null, $test);
    }

    /** @test */
    function it_will_save_array_and_automatically_serialize_it()
    {
        $arTest = [1, 2 ,3];
        Option::set('KEY1', $arTest);
        $test = Option::get('KEY1');
        $this->assertEquals($arTest, $test);
    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
