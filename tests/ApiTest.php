<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\CostController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;

class ApiTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение
    }

    function test_can_handle_bad_bad_Model()
    {
        $arRes = (new CostController)->handle(['model' => 'badModelNameXXX555777']);
        $this->assertTrue($arRes['error']);
    }

    function test_can_handle_bad_bad_api_method()
    {
        $arRes = (new CostController)->handle(['model' => 'Cost', 'operation' => 'xdsfsfdwerwrwerwrwerwer']);
        $this->assertTrue($arRes['error']);
    }

    function test_can_handle_getAll_request()
    {
        $arRes = (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'get',
        ]);
        $this->assertCount(3, $arRes);
    }

    function test_can_handle_delete_request()
    {
        (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'delete',
            'id'        => 1,
        ]);
        $arRes = (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'get',
        ]);
        $this->assertCount(2, $arRes);
    }

    function test_can_handle_add_request()
    {
        (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'add',
            'data'      => [
                'name'  => 'TestCost',
                'price' => 555.55,
            ],
        ]);
        $arRes = (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'get',
        ]);
        $this->assertCount(4, $arRes);

        $arNew = end($arRes);
        $this->assertEquals('TestCost', $arNew['name']);
        $this->assertEquals(555.55, $arNew['price']);

        (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'delete',
            'id'        => 4,
        ]);
    }

    function test_can_handle_update_request()
    {
        (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'update',
            'data'      => [
                'id'    => 1,
                'name'  => 'TestCost',
                'price' => 555.55,
            ],
        ]);
        $arRes = (new CostController)->handle([
            'model'     => 'Cost',
            'operation' => 'get',
        ]);
        $this->assertCount(3, $arRes);

        $arNew = current($arRes);
        $this->assertEquals('TestCost', $arNew['name']);
        $this->assertEquals(555.55, $arNew['price']);
    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
