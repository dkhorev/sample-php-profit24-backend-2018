<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\StatisticController;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Model\Statistic;

class StatisticControllerInstanceTestTest extends TestCase
{
    /**
     * @var Capsule
     */
    protected $db;

    protected function setUp()
    {
        parent::setUp();
        $this->db = new Capsule;
    }

    public function badValues()
    {
        return [[false], [null], [''], [[]]];
    }

    /** @test */
    function it_can_be_instanced()
    {
        $arParams = [
            'fromDate'     => '2018-07-01',
            'toDate'       => '2018-07-30',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => ['1'],
        ];
        $ctr = new StatisticController($this->db, $arParams);
        $this->assertInstanceOf(StatisticController::class, $ctr);
    }

    /** @test */
    function it_has_exception_form_bad_dates1()
    {
        $arParams = [
            'fromDate'     => '',
            'toDate'       => '2018-07-30',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => ['1'],
        ];
        $this->expectException(Exception::class);
        new StatisticController($this->db, $arParams);
    }

    /** @test */
    function it_has_exception_form_bad_dates2()
    {
        $arParams = [
            'fromDate'     => '2018-07-30',
            'toDate'       => '',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => ['1'],
        ];
        $this->expectException(Exception::class);
        new StatisticController($this->db, $arParams);
    }

    /** @test */
    function it_has_exception_form_bad_dates3()
    {
        $arParams = [
            'fromDate'     => '',
            'toDate'       => '',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => ['1'],
        ];
        $this->expectException(Exception::class);
        new StatisticController($this->db, $arParams);
    }


    /**
     * @test
     * @dataProvider badValues
     * @expectedException Exception
     *
     * @param $badValues
     *
     * @throws Exception
     */
    function it_has_exception_on_bad_type($badValues)
    {
        $arParams = [
            'fromDate'     => '',
            'toDate'       => '',
            'type'         => $badValues,
            'metrics'      => ['1'],
        ];
        new StatisticController($this->db, $arParams);
    }

    /**
     * @test
     * @dataProvider badValues
     * @expectedException Exception
     *
     * @param $badValues
     *
     * @throws Exception
     */
    function it_has_exception_on_bad_metrics($badValues)
    {
        $arParams = [
            'fromDate'     => '',
            'toDate'       => '',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => [$badValues],
        ];
        new StatisticController($this->db, $arParams);
    }

    /**
     * @test
     * @expectedException Exception
     *
     * @throws Exception
     */
    function it_has_exception_if_empty_one_of_metrics()
    {
        $arParams = [
            'fromDate'     => '2018-07-30',
            'toDate'       => '2018-07-30',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => [
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
                [Paytype::TYPE_REVENUE, Paytype::TYPE_REVENUE],
                [],
            ]
        ];
        new StatisticController($this->db, $arParams);
    }

    /**
     * @test
     * @expectedException Exception
     *
     * @throws Exception
     */
    function it_has_exception_if_dateFrom_is_less_than_dateTo()
    {
        $arParams = [
            'fromDate'     => '2018-07-30',
            'toDate'       => '2018-07-29',
            'type'         => Statistic::TYPE_WEEK,
            'metrics'      => [
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
                [Paytype::TYPE_REVENUE, Paytype::TYPE_REVENUE],
            ]
        ];
        new StatisticController($this->db, $arParams);
    }
}
