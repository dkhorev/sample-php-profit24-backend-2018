<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Helpers;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Controller\StatisticController;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\B24Status;
use WebNow\Project\Model\Operation;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Model\Statistic;

class PaytypeControllerTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента
    }

    /** @test */
    function it_can_idenify_InvoicePayTypeId_by_status_and_group()
    {
        // записать тестовые статусы
        $arStatus = [
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N'],
            ['ID' => 2, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N1'],
            ['ID' => 3, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N2'],
            ['ID' => 4, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S'],
            ['ID' => 5, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S1'],
            ['ID' => 6, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S2'],
            ['ID' => 7, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'P'],
            ['ID' => 8, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D'],
            ['ID' => 9, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D1'],
            ['ID' => 10, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D2'],
        ];
        B24Status::updateByEntity($arStatus);

        // сгенерить тестовые операции
        $arOpStatuses = array_map(function ($item) {
            return $item['STATUS_ID'];
        }, $arStatus);

        // прогнать их статусы через getInvoicePayTypeId и проверить что группа верная
        $idExpRev = PaytypeController::getFirstExpectedRevenue()['id'];
        $idRev = PaytypeController::getFirstRevenue()['id'];

        foreach ($arOpStatuses as $status_id) {
            $idPaytype = PaytypeController::getInvoicePayTypeId($status_id);

            switch ($status_id) {
                case 'N':
                case 'N1':
                case 'N2':
                case 'S':
                case 'S1':
                case 'S2':
                    $this->assertEquals($idExpRev, $idPaytype);
                    break;
                case 'P':
                    $this->assertEquals($idRev, $idPaytype);
                    break;
                case 'D':
                case 'D1':
                case 'D2':
                    $this->assertEquals(false, $idPaytype);
                    break;
            }
        }


    }

    /** @test */
    function it_can_idenify_LeadPayTypeId_by_status_and_group()
    {
        // записать тестовые статусы
        $arStatus = [
            ['ID' => 1, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW'],
            ['ID' => 2, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW1'],
            ['ID' => 3, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS1'],
            ['ID' => 5, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS2'],
            ['ID' => 6, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'CONVERTED'],
            ['ID' => 7, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK'],
            ['ID' => 8, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK1'],
            ['ID' => 9, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK2'],
            ['ID' => 10, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK3'],
        ];
        B24Status::updateByEntity($arStatus);

        // сгенерить тестовые операции
        $arOpStatuses = array_map(function ($item) {
            return $item['STATUS_ID'];
        }, $arStatus);

        // прогнать их статусы через getInvoicePayTypeId и проверить что группа верная
        $idLead = PaytypeController::getFirstLead()['id'];
        $idLeadFail = PaytypeController::getFirstLeadLost()['id'];

        foreach ($arOpStatuses as $status_id) {
            $idPaytype = PaytypeController::getLeadPayTypeId($status_id);
            // dump($status_id);
            // dump($idPaytype);

            switch ($status_id) {
                case 'NEW':
                case 'NEW1':
                case 'IN_PROCESS':
                case 'IN_PROCESS1':
                case 'IN_PROCESS2':
                    $this->assertEquals($idLead, $idPaytype);
                    break;
                case 'CONVERTED':
                    $this->assertEquals(false, $idPaytype);
                    break;
                case 'JUNK':
                case 'JUNK1':
                case 'JUNK2':
                case 'JUNK3':
                    $this->assertEquals($idLeadFail, $idPaytype);
                    break;
            }
        }


    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
