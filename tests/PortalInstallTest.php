<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Portal;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\Cost;

class PortalInstallTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arDataSelfHosted;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];
    }

    function test_PortalInstall_can_be_instanced()
    {
        $PortalInstall = new PortalInstall([], $this->db);
        $this->assertInstanceOf('WebNow\Project\Install\PortalInstall', $PortalInstall);
    }

    function test_can_Up_app_hosted()
    {
        // dump(openssl_get_cipher_methods());
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $this->assertEquals($this->sSecretCode, $PortalInstall->Up());
        $this->assertCount(1, Portal::all()->toArray());
    }

    function test_has_standart_tables()
    {
        // dump(openssl_get_cipher_methods());
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $this->assertEquals($this->sSecretCode, $PortalInstall->Up());

        $arTables = $this->db->getConnection()->getDoctrineSchemaManager()->listTableNames();
        $this->assertCount(10, $arTables); // в проекте 10 таблиц портала
        $this->assertContains('b24_statuses', $arTables);
        $this->assertContains('contractors', $arTables);
        $this->assertContains('costs', $arTables);
        $this->assertContains('employees', $arTables);
        $this->assertContains('monthly_tasks', $arTables);
        $this->assertContains('operations', $arTables);
        $this->assertContains('options', $arTables);
        $this->assertContains('paytypes', $arTables);
        $this->assertContains('rois', $arTables);
        $this->assertContains('statistics', $arTables);
    }

    function test_can_Down_app_hosted()
    {
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $this->assertTrue($PortalInstall->Down());
        $this->assertEquals([], Portal::all()->toArray());
    }

    function test_can_Up_self_hosted()
    {
        // данные когда БД хостится у клиента
        $this->arDataSelfHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => '127.0.0.1',
            'db_name'       => 'testing_db',
            'db_user'       => 'testing_user',
            'db_pass'       => 'testing_pass',

            'storeType' => 'self',
        ];
        // надо на лету создать test БД и юзера
        $this->db->getConnection()->getPdo()->exec("CREATE DATABASE `testing_db`");
        $this->db->getConnection()->getPdo()->exec("CREATE USER `testing_user`@`localhost` IDENTIFIED BY 'testing_pass'");
        $this->db->getConnection()->getPdo()->exec("GRANT ALL PRIVILEGES ON `testing_db` . * TO `testing_user`@`localhost`");
        $this->db->getConnection()->getPdo()->exec("FLUSH PRIVILEGES");

        $PortalInstall = new PortalInstall($this->arDataSelfHosted, $this->db);

        $this->assertEquals(['result' => true, 'error' => ''], $PortalInstall->ValidateCustomConnection());

        $this->assertEquals($this->sSecretCode, $PortalInstall->Up());
        $this->assertCount(1, Portal::all()->toArray());
        $this->assertCount(3, Cost::all()->toArray());

        $this->assertTrue($PortalInstall->Down());

        // надо уничтожить test БД и юзера
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE `testing_db`");
        $this->db->getConnection()->getPdo()->exec("DROP USER `testing_user`@`localhost`");
    }

    public function tearDown()
    {
        parent::tearDown();
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);

        // $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");
    }

}
