<?php

use Illuminate\Support\Carbon;
use WebNow\Project\Charts\ChartJsFormat;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Model\Statistic;

class ChartJsFormatTest extends TestCase
{
    protected $stdParams = [
        'labelFormat' => '%d %b %Y'
    ];

    public function badValues()
    {
        return [[false], [null], [''], [[]], ['    ']];
    }

    /** @test */
    function it_can_be_instanced()
    {
        $format = new ChartJsFormat($this->stdParams);
        $this->assertInstanceOf(ChartJsFormat::class, $format);
    }

    /**
     * @test
     * @dataProvider badValues
     * @expectedException Exception
     *
     * @param $badValues
     *
     * @throws Exception
     */
    function it_has_exception_for_bad_label_format($badValues)
    {
        new ChartJsFormat(['labelFormat' => $badValues]);
    }

    /** @test */
    function it_returns_correct_array_with_labels_and_datasets()
    {
        $cjs = new ChartJsFormat($this->stdParams);
        $arRes = $cjs->format([]);
        $this->assertArrayHasKey('labels', $arRes);
        $this->assertArrayHasKey('datasets', $arRes);
    }

    /** @test */
    function it_return_resulting_data_as_arrays()
    {
        $cjs = new ChartJsFormat($this->stdParams);
        $data = [
            // dataset 1
            [
                ['label_date' => '2018-05-28', 'summ' => 0],
            ]
        ];
        $arRes = $cjs->format($data);
        $this->assertTrue(is_array($arRes['labels']));
        $this->assertTrue(is_array($arRes['datasets']));
    }

    /** @test */
    function it_returns_labels_and_in_requested_format()
    {
        $arParams = [
            'labelFormat' => '%d %b %Y'
        ];
        $cjs = new ChartJsFormat($arParams);

        $data = [
            // dataset 1
            [
                ['label_date' => '2018-05-28', 'summ' => 0],
                ['label_date' => '2018-06-04', 'summ' => 0],
                ['label_date' => '2018-06-11', 'summ' => 0],
                ['label_date' => '2018-06-18', 'summ' => 0],
                ['label_date' => '2018-06-25', 'summ' => 0],
            ]
        ];
        $arRes = $cjs->format($data);

        $this->assertArrayHasKey('labels', $arRes);
        $arLabels = $arRes['labels'];
        $arSet1 = $data[0];
        setlocale(LC_TIME, 'ru_RU.UTF-8');
        foreach ($arSet1 as $key => $ar) {
            $expectedDate = Carbon::createFromFormat(Statistic::LABEL_DATE_FORMAT, $ar['label_date'])
                          ->formatLocalized($arParams['labelFormat']);
            $this->assertEquals($expectedDate, $arLabels[$key]);
        }
    }

    /** @test */
    function it_returns_correct_single_dataset()
    {
        $cjs = new ChartJsFormat($this->stdParams);
        $data = [
            // dataset 1
            [
                ['label_date' => '2018-05-28', 'summ' => 10],
                ['label_date' => '2018-06-04', 'summ' => 20],
                ['label_date' => '2018-06-11', 'summ' => 30],
                ['label_date' => '2018-06-18', 'summ' => 40],
                ['label_date' => '2018-06-25', 'summ' => 50],
            ]
        ];
        $arRes = $cjs->format($data);
        // dump($arRes);
        $this->assertCount(1, $arRes['datasets']);
        $this->assertCount(5, $arRes['datasets'][0]);
        $this->assertEquals(10, $arRes['datasets'][0][0]);
        $this->assertEquals(20, $arRes['datasets'][0][1]);
        $this->assertEquals(30, $arRes['datasets'][0][2]);
        $this->assertEquals(40, $arRes['datasets'][0][3]);
        $this->assertEquals(50, $arRes['datasets'][0][4]);
    }

    /** @test */
    function it_returns_correct_multiple_datasets()
    {
        $cjs = new ChartJsFormat($this->stdParams);
        $data = [
            // dataset 1
            [
                ['label_date' => '2018-05-28', 'summ' => 10],
                ['label_date' => '2018-06-04', 'summ' => 20],
                ['label_date' => '2018-06-11', 'summ' => 30],
                ['label_date' => '2018-06-18', 'summ' => 40],
                ['label_date' => '2018-06-25', 'summ' => 50],
            ],
            // dataset 2
            [
                ['label_date' => '2018-05-28', 'summ' => 60],
                ['label_date' => '2018-06-04', 'summ' => 70],
                ['label_date' => '2018-06-11', 'summ' => 80],
                ['label_date' => '2018-06-18', 'summ' => 90],
                ['label_date' => '2018-06-25', 'summ' => 100],
            ],
            // dataset 3
            [
                ['label_date' => '2018-05-28', 'summ' => 110],
                ['label_date' => '2018-06-04', 'summ' => 120],
                ['label_date' => '2018-06-11', 'summ' => 130],
                ['label_date' => '2018-06-18', 'summ' => 140],
                ['label_date' => '2018-06-25', 'summ' => 150],
            ]
        ];
        $arRes = $cjs->format($data);
        // dump($arRes);
        $this->assertCount(3, $arRes['datasets']);

        $arSet1 = $arRes['datasets'][0];
        $this->assertCount(5, $arSet1);
        $this->assertEquals(10, $arSet1[0]);
        $this->assertEquals(20, $arSet1[1]);
        $this->assertEquals(30, $arSet1[2]);
        $this->assertEquals(40, $arSet1[3]);
        $this->assertEquals(50, $arSet1[4]);

        $arSet2 = $arRes['datasets'][1];
        $this->assertCount(5, $arSet2);
        $this->assertEquals(60, $arSet2[0]);
        $this->assertEquals(70, $arSet2[1]);
        $this->assertEquals(80, $arSet2[2]);
        $this->assertEquals(90, $arSet2[3]);
        $this->assertEquals(100, $arSet2[4]);

        $arSet3 = $arRes['datasets'][2];
        $this->assertCount(5, $arSet3);
        $this->assertEquals(110, $arSet3[0]);
        $this->assertEquals(120, $arSet3[1]);
        $this->assertEquals(130, $arSet3[2]);
        $this->assertEquals(140, $arSet3[3]);
        $this->assertEquals(150, $arSet3[4]);
    }


}
