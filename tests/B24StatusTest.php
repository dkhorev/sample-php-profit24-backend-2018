<?php


use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\B24Status;

class B24StatusTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента
    }

    /** @test */
    function it_can_create_status_in_empty_db()
    {
        // создать болванку для загрузки в формате Б24
        $arStatus = [
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'IS1'],
            ['ID' => 2, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'IS2'],
            ['ID' => 3, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'IS3'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'S1'],
            ['ID' => 5, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'S2'],
            ['ID' => 6, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'S3'],
        ];

        // проверить что по началу пусто
        $arExist = B24Status::all()->toArray();
        $this->assertCount(0, $arExist);

        // загрухить статусы
        B24Status::updateByEntity($arStatus);

        // проверить их кол-во и состав
        $arExist = B24Status::all()->toArray();
        $this->assertCount(6, $arExist);

        foreach ($arExist as $key => $status) {
            $this->assertEquals($arStatus[$key]['ID'], $status['b24_id']);
            $this->assertEquals($arStatus[$key]['ENTITY_ID'], $status['entity']);
            $this->assertEquals($arStatus[$key]['status_id'], $status['STATUS_ID']);
        }
    }

    /** @test */
    function it_can_update_status_by_b24_id()
    {
        // создать болванку для загрузки в формате Б24
        $arStatus = [
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'IS1'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'S1'],
        ];

        // проверить что по началу пусто
        $arExist = B24Status::all()->toArray();
        $this->assertCount(0, $arExist);

        // загрухить статусы
        B24Status::updateByEntity($arStatus);

        // внести изменения
        $arStatus = [
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'IS333'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'S333'],
        ];

        // проверить их кол-во и состав
        B24Status::updateByEntity($arStatus);
        $arExist = B24Status::all()->toArray();
        $this->assertCount(2, $arExist);

        foreach ($arExist as $key => $status) {
            $this->assertEquals($arStatus[$key]['ID'], $status['b24_id']);
            $this->assertEquals($arStatus[$key]['ENTITY_ID'], $status['entity']);
            $this->assertEquals($arStatus[$key]['status_id'], $status['STATUS_ID']);
        }
    }

    /** @test */
    function it_will_generate_correct_groups_for_lead_statuses()
    {
        // записать тестовые статусы
        $arStatus = [
            // B24Status::GROUP_PROGRESS
            ['ID' => 1, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW'],
            ['ID' => 2, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW1'],
            ['ID' => 3, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW2'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS'],
            ['ID' => 5, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS1'],
            ['ID' => 6, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS2'],
            // B24Status::GROUP_SUCCESS
            ['ID' => 7, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'CONVERTED'],
            // B24Status::GROUP_FAIL
            ['ID' => 8, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK'],
            ['ID' => 9, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK1'],
            ['ID' => 10, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK2'],
        ];
        B24Status::updateByEntity($arStatus);

        $arExist = B24Status::all()->toArray();

        // проверить
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[0]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[1]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[2]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[3]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[4]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[5]['group']);

        $this->assertEquals(B24Status::GROUP_SUCCESS, $arExist[6]['group']);

        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[7]['group']);
        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[8]['group']);
        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[9]['group']);
    }

    /** @test */
    function it_will_generate_correct_groups_for_invoice_statuses()
    {
        // записать тестовые статусы
        $arStatus = [
            // B24Status::GROUP_PROGRESS
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N'],
            ['ID' => 2, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N1'],
            ['ID' => 3, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N2'],
            ['ID' => 4, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S'],
            ['ID' => 5, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S1'],
            ['ID' => 6, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S2'],
            // B24Status::GROUP_SUCCESS
            ['ID' => 7, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'P'],
            // B24Status::GROUP_FAIL
            ['ID' => 8, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D'],
            ['ID' => 9, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D1'],
            ['ID' => 10, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D2'],
        ];
        B24Status::updateByEntity($arStatus);

        $arExist = B24Status::all()->toArray();

        // проверить
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[0]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[1]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[2]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[3]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[4]['group']);
        $this->assertEquals(B24Status::GROUP_PROGRESS, $arExist[5]['group']);

        $this->assertEquals(B24Status::GROUP_SUCCESS, $arExist[6]['group']);

        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[7]['group']);
        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[8]['group']);
        $this->assertEquals(B24Status::GROUP_FAIL, $arExist[9]['group']);
    }

    /** @test */
    function it_will_return_correct_invoice_statuses_by_group()
    {
        // записать тестовые статусы
        $arStatus = [
            // B24Status::GROUP_PROGRESS
            ['ID' => 1, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N'],
            ['ID' => 2, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N1'],
            ['ID' => 3, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'N2'],
            ['ID' => 4, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S'],
            ['ID' => 5, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S1'],
            ['ID' => 6, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'S2'],
            // B24Status::GROUP_SUCCESS
            ['ID' => 7, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'P'],
            // B24Status::GROUP_FAIL
            ['ID' => 8, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D'],
            ['ID' => 9, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D1'],
            ['ID' => 10, 'ENTITY_ID' => 'INVOICE_STATUS', 'STATUS_ID' => 'D2'],
        ];
        B24Status::updateByEntity($arStatus);

        $arTest = B24Status::getStatuses(B24Status::STATUS_INVOICE, B24Status::GROUP_FAIL);

        $this->assertContains('D', $arTest);
        $this->assertContains('D1', $arTest);
        $this->assertContains('D2', $arTest);

        $this->assertNotContains('P', $arTest);
        $this->assertNotContains('N', $arTest);
        $this->assertNotContains('N1', $arTest);
        $this->assertNotContains('N2', $arTest);
        $this->assertNotContains('S', $arTest);
        $this->assertNotContains('S1', $arTest);
        $this->assertNotContains('S2', $arTest);
    }

    /** @test */
    function it_will_return_correct_lead_statuses_by_group_array()
    {
        // записать тестовые статусы
        $arStatus = [
            // B24Status::GROUP_PROGRESS
            ['ID' => 1, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW'],
            ['ID' => 2, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW1'],
            ['ID' => 3, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'NEW2'],
            ['ID' => 4, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS'],
            ['ID' => 5, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS1'],
            ['ID' => 6, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'IN_PROCESS2'],
            // B24Status::GROUP_SUCCESS
            ['ID' => 7, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'CONVERTED'],
            // B24Status::GROUP_FAIL
            ['ID' => 8, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK'],
            ['ID' => 9, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK1'],
            ['ID' => 10, 'ENTITY_ID' => 'STATUS', 'STATUS_ID' => 'JUNK2'],
        ];
        B24Status::updateByEntity($arStatus);

        $arTest = B24Status::getStatuses(B24Status::STATUS_LEAD, [B24Status::GROUP_FAIL, B24Status::GROUP_SUCCESS]);

        $this->assertContains('CONVERTED', $arTest);
        $this->assertContains('JUNK', $arTest);
        $this->assertContains('JUNK1', $arTest);
        $this->assertContains('JUNK2', $arTest);

        $this->assertNotContains('NEW', $arTest);
        $this->assertNotContains('NEW1', $arTest);
        $this->assertNotContains('NEW2', $arTest);
        $this->assertNotContains('IN_PROCESS', $arTest);
        $this->assertNotContains('IN_PROCESS1', $arTest);
        $this->assertNotContains('IN_PROCESS2', $arTest);
    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
