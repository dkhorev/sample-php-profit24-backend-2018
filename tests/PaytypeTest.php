<?php

use WebNow\Project\Model\Paytype;
use PHPUnit\Framework\TestCase;

class PaytypeTest extends TestCase
{
    function test_it_has_needed_constants()
    {
        $paytype = new \ReflectionClass(Paytype::class);
        $arConst = $paytype->getConstants();
        $this->assertArrayHasKey('TYPE_LEAD', $arConst);
        $this->assertArrayHasKey('TYPE_OVER', $arConst);
        $this->assertArrayHasKey('TYPE_REVENUE', $arConst);
        $this->assertArrayHasKey('TYPE_COST', $arConst);
        $this->assertArrayHasKey('TYPE_EXPECT_REVENUE', $arConst);
        $this->assertArrayHasKey('TYPE_EXPECT_COST', $arConst);
        $this->assertArrayHasKey('TYPE_LEAD_LOST', $arConst);

        // наших констант должно быть ровно 7
        $arConst = array_filter(array_keys($arConst), function ($item) {
           if (preg_match('/TYPE_/', $item)) {
               return true;
           }
           return false;
        });

        $this->assertCount(7, $arConst);
    }
}
