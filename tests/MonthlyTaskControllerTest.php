<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Carbon;
use PHPUnit\Framework\TestCase;
use WebNow\Project\Controller\CostController;
use WebNow\Project\Controller\EmployeeController;
use WebNow\Project\Controller\MonthlyTaskController;
use WebNow\Project\Controller\OperationController;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\Cost;
use WebNow\Project\Model\Employee;
use WebNow\Project\Model\MonthlyTask;
use WebNow\Project\Model\Operation;
use WebNow\Project\Model\Paytype;

class MonthlyTaskControllerTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;

    private $arEmpl;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента

        // ДОБАВЛЯЕМ ДАННЫЕ ПО АПИ
        // сотрудники
        $this->arEmpl = [
            ['name' => 'Сотрудник', 'lastname' => '1', 'wage_real' => 10000, 'is_active' => true, 'departments' => 1],
            ['name' => 'Сотрудник', 'lastname' => '2', 'wage_real' => 500, 'is_active' => true, 'departments' => 1],
            ['name' => 'Сотрудник', 'lastname' => '3', 'wage_real' => 200, 'is_active' => true, 'departments' => 1],
        ];
        $rsEmployee = new EmployeeController;
        foreach ($this->arEmpl as $ar) {
            $rsEmployee->handle([
                'model'     => 'Employee',
                'operation' => 'add',
                'data'      => $ar,
            ]);
        }
    }

    /** @test */
    function it_can_be_instanced()
    {
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('yy'),
        ];
        // dump($arParams);
        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $this->assertInstanceOf(MonthlyTaskController::class, $rsMt);
    }
    
    /** @test */
    function it_can_parse_bad_task()
    {
        $task = 'somejunktaskthatcannotbe';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(true, $arRes['error']);
        $this->assertEquals(MonthlyTaskController::$ERR_OP_FAIL, $arRes['msgHuman']);
    }

    /** @test */
    function it_will_not_do_repeated_task()
    {
        $task = 'wage_50';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $rsMt->makeTask();
        $arRes = $rsMt->makeTask();
        // dump($arRes);
        $this->assertEquals(true, $arRes['error']);
        $this->assertEquals(MonthlyTaskController::$ERR_OP_ALR_DONE, $arRes['msgHuman']);
    }

    /** @test */
    function it_can_get_task_statuses_when_db_is_empty_auto_create_it_with_zeros_and_after_add_get_updated()
    {
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('yy'),
        ];
        // на чистой базе, ожидаем все 0
        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arStatus = $rsMt->getTaskStatus();
        $nCount = array_reduce($arStatus, function ($acc, $item) {
            return $acc + $item;
        });
        $this->assertEquals(0, $nCount);

        // добавим пару флагов
        $rsTask = MonthlyTask::query()->find(1);
        $rsTask->open_bills = 1;
        $rsTask->open_leads = 1;
        $rsTask->wage_50 = 1;
        $rsTask->save();

        // ожидаем 3
        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arStatus = $rsMt->getTaskStatus();
        $nCount = array_reduce($arStatus, function ($acc, $item) {
            return $acc + $item;
        });
        $this->assertEquals(3, $nCount);
    }

    /** @test */
    function it_can_add_wage_50()
    {
        $task = 'wage_50';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);
        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $expectedDate = implode('-', [date('Y'), date('m'), '0' . MonthlyTaskController::$WAGE_50_DAY]);
            $this->assertEquals($arE['wage_real'] / 2, $arOp['summ']);
            $this->assertEquals($expectedDate, $arOp['date']);
            $this->assertEquals(MonthlyTaskController::$WAGE_TAG, $arOp['comment']);
        }
    }

    /** @test */
    function it_can_add_wage_100()
    {
        $task = 'wage_100';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);
        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $this->assertEquals($arE['wage_real'] / 2, $arOp['summ']);
            $this->assertEquals(implode('-', [date('Y'), date('m'), MonthlyTaskController::$WAGE_100_DAY]), $arOp['date']);
            $this->assertEquals(MonthlyTaskController::$WAGE_TAG, $arOp['comment']);
        }
    }

    /** @test */
    function it_will_add_wage_50_as_expected_cost_if_date_is_less_than_5()
    {
        $task = 'wage_50';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n') + 1,
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);

        $arExpectedOpType = PaytypeController::getFirstExpectedCost();

        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $this->assertEquals($arExpectedOpType['id'], $arOp['paytype_id']);
        }
    }

    /** @test */
    function it_will_add_wage_50_as_cost_if_date_is_more_than_5()
    {
        $task = 'wage_50';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n') - 1,
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);

        $arExpectedOpType = PaytypeController::getFirstCost();

        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $this->assertEquals($arExpectedOpType['id'], $arOp['paytype_id']);
        }
    }

    /** @test */
    function it_will_add_wage_100_as_expected_cost_if_date_is_less_than_20()
    {
        $task = 'wage_100';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n') + 1,
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);

        $arExpectedOpType = PaytypeController::getFirstExpectedCost();

        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $this->assertEquals($arExpectedOpType['id'], $arOp['paytype_id']);
        }
    }

    /** @test */
    function it_will_add_wage_100_as_cost_if_date_is_more_than_20()
    {
        $task = 'wage_100';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n') - 1,
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        $arEmpl = Employee::all()->toArray();
        $arOps = Operation::all()->toArray();
        // dump($arEmpl);
        // dump($arOps);

        $arExpectedOpType = PaytypeController::getFirstCost();

        foreach ($arEmpl as $arE) {
            $userId = $arE['id'];
            $arOp = array_filter($arOps, function ($item) use ($userId, $arE) {
                return $item['employee_id'] === $userId;
            });
            $arOp = end($arOp);

            $this->assertEquals($arExpectedOpType['id'], $arOp['paytype_id']);
        }
    }

    /** @test */
    function it_can_add_fixed_costs()
    {
        $arAll = Cost::all();
        foreach ($arAll as $cost) {
            $cost->delete();
        }

        // добавим что-то в постоянные расходы
        $input = [
            ['name' => 'Аренда', 'price' => 0],
            ['name' => 'Офис', 'price' => 1000],
            ['name' => 'Бухгалтерия', 'price' => 2000],
            ['name' => 'Уборка', 'price' => 4000],
        ];
        $rsHandler = new CostController;
        foreach ($input as $ar) {
            $rsHandler->handle([
                'model'     => 'Cost',
                'operation' => 'add',
                'data'      => $ar,
            ]);
        }
        // dump(Cost::all()->toArray());

        $task = 'fixed_costs';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => date('n'),
            'year'      => date('Y'),
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        // проверяем что все записалось
        $arOps = Operation::all()->toArray();
        // dump($arOps);
        $arExpectedCostOp = PaytypeController::getFirstExpectedCost();

        foreach ($input as $arE) {
            $name = $arE['name'];
            $price = (double) $arE['price'];
            $arOp = array_filter($arOps, function ($item) use ($name, $price, $arExpectedCostOp) {
                return (
                    $item['summ'] === $price
                    && $item['comment'] === MonthlyTaskController::$FIXED_COSTS_TAG . ' (' . $name . ')'
                    && $item['paytype_id'] === $arExpectedCostOp['id']
                );
            });
            $this->assertCount(1, $arOp);
        }
    }

    /** @test */
    function it_can_copy_open_leads_from_previous_month()
    {
        // вытащить все операции с типом $idType
        $arOpTypes = PaytypeController::getAllByType(Paytype::TYPE_LEAD);
        $arOpTypes = array_map(function ($item) {
            return $item['id'];
        }, $arOpTypes);
        // dump($arOpTypes);

        // сгенерить рандомные лиды "месяц назад"
        $year = 2019;
        $month = 1;
        $dateNow = Carbon::create($year, $month, 15, 0, 0, 0);
        $dateAdd = $dateNow->copy()->subMonth()->toDateString();
        $arSumm = [500, 1000, 2000, 5000, 15000];

        $input = [
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 1],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 11],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 111],
        ];

        // добавить в предыдущий месяц
        $rsHandler = new OperationController;
        foreach ($input as $ar) {
            $rsHandler->handle([
                'model'     => 'Operation',
                'operation' => 'add',
                'data'      => $ar,
            ]);
        }
        // dump(Operation::all()->toArray());

        // вызвать копирование
        $task = 'open_leads';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => $month,
            'year'      => $year,
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        // проверить что данные идентичны
        $arOps = Operation::byMonthYear($year, $month)->get()->toArray();
        $this->assertCount(5, $arOps);
        foreach ($arOps as $key => $ar) {
            $arCopy = $input[$key];
            $this->assertEquals($arCopy['summ'], $ar['summ']);
            $this->assertEquals($arCopy['paytype_id'], $ar['paytype_id']);
            $this->assertEquals($arCopy['b24_id'], $ar['b24_id']);
        }
    }

    /** @test */
    function it_can_copy_open_bills_from_previous_month()
    {
        // вытащить все операции с типом $idType
        $arOpTypes = PaytypeController::getAllByType(Paytype::TYPE_EXPECT_REVENUE);
        $arOpTypes = array_map(function ($item) {
            return $item['id'];
        }, $arOpTypes);
        // dump($arOpTypes);

        // сгенерить рандомные лиды "месяц назад"
        $year = 2019;
        $month = 1;
        $dateNow = Carbon::create($year, $month, 15, 0, 0, 0);
        $dateAdd = $dateNow->copy()->subMonth()->toDateString();
        $arSumm = [500, 1000, 2000, 5000, 15000];

        $input = [
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 1],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 11],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd],
            ['paytype_id' => $arOpTypes[array_rand($arOpTypes)], 'summ' => $arSumm[array_rand($arSumm)], 'date' => $dateAdd, 'b24_id' => 111],
        ];

        // добавить в предыдущий месяц
        $rsHandler = new OperationController;
        foreach ($input as $ar) {
            $rsHandler->handle([
                'model'     => 'Operation',
                'operation' => 'add',
                'data'      => $ar,
            ]);
        }
        // dump(Operation::all()->toArray());

        // вызвать копирование
        $task = 'open_bills';
        $arParams = [
            'model'     => 'MonthlyTask',
            'month'     => $month,
            'year'      => $year,
            'task'      => $task,
        ];

        $rsMt = new MonthlyTaskController($this->db, $arParams);
        $arRes = $rsMt->makeTask();
        $this->assertEquals(1, $arRes[$task]);

        // проверить что данные идентичны
        $arOps = Operation::byMonthYear($year, $month)->get()->toArray();
        $this->assertCount(5, $arOps);
        foreach ($arOps as $key => $ar) {
            $arCopy = $input[$key];
            $this->assertEquals($arCopy['summ'], $ar['summ']);
            $this->assertEquals($arCopy['paytype_id'], $ar['paytype_id']);
            $this->assertEquals($arCopy['b24_id'], $ar['b24_id']);
        }
    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
