<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\Helpers;
use WebNow\Project\Controller\PaytypeController;
use WebNow\Project\Controller\PortalController;
use WebNow\Project\Controller\StatisticController;
use WebNow\Project\Install\AppInstall;
use WebNow\Project\Install\PortalInstall;
use WebNow\Project\Model\Operation;
use WebNow\Project\Model\Paytype;
use WebNow\Project\Model\Statistic;

class StatisticControllerTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected $dbName;
    protected $arData;
    protected $arDataCustom;
    private $sSecretCode;
    private $arDataAppHosted;
    private $arPortal;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();

        // ставим БД для тестового основного портала
        $AppInstall = new AppInstall($this->db);
        $AppInstall->Up($this->dbTestName);

        $this->sSecretCode = md5('non_existent' . APP_SECRET_CODE);

        // данные когда БД хостится "у нас"
        $this->arDataAppHosted = [
            'domain'        => 'non_existent.bitrix24.ru',
            'member_id'     => 'non_existent',
            'access_token'  => '111',
            'refresh_token' => '222',
            'expires_in'    => '333',
            'db_host'       => null,
            'db_name'       => null,
            'db_user'       => null,
            'db_pass'       => null,
        ];

        // поднимаем Портал
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Up();

        // поднимаем БД клиента
        $this->arPortal = PortalController::findByCode($this->sSecretCode);
        PortalController::addConnection($this->arPortal); // это метод добавляет в глобал капсулу новое подключение клиента
    }

    /** @test */
    function it_will_return_null_summs_when_operations_are_empty()
    {
        $arParams = [
            'fromDate' => '2018-07-30',
            'toDate'   => '2018-07-30',
            'type'     => Statistic::TYPE_WEEK,
            'metrics'  => [
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
            ],
        ];
        $ctr = new StatisticController($this->db, $arParams);
        $ar = $ctr->get();
        $this->assertCount(3, $ar);
        foreach ($ar as $data) {
            $this->assertCount(1, $data);
            $this->assertEquals(0, end($data)['summ']);
        }
    }

    /** @test */
    function it_can_build_weekly_stats_with_one_simple_metric()
    {
        // запушим тестовые данные
        $arMetric = explode(',', Paytype::TYPE_REVENUE);
        $arPaytypeId = Helpers::extractByField('id', PaytypeController::getAllByType($arMetric));
        $arOps = [
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-16'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-16'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-17'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-17'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-18'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-18'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-19'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-20'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-23'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-24'],
            ['paytype_id' => $arPaytypeId[array_rand($arPaytypeId)], 'summ' => 1000, 'date' => '2018-07-25'],
        ];
        Operation::query()->insert($arOps);

        $arParams = [
            'fromDate' => '2018-07-01',
            'toDate'   => '2018-07-31',
            'type'     => Statistic::TYPE_WEEK,
            'metrics'  => [Paytype::TYPE_REVENUE],
        ];
        $rsStat = new StatisticController($this->db, $arParams);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 1 метрику статитике
        $this->assertCount(1, $arStats);
        // и в ней 6 недель
        $arStat = $arStats[Paytype::TYPE_REVENUE];
        $this->assertCount(6, $arStat);
        $this->assertEquals(0, $arStat[0]['summ']);
        $this->assertEquals(0, $arStat[1]['summ']);
        $this->assertEquals(0, $arStat[2]['summ']);
        $this->assertEquals(8000, $arStat[3]['summ']);
        $this->assertEquals(3000, $arStat[4]['summ']);
        $this->assertEquals(0, $arStat[5]['summ']);

        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 1 метрику статитике
        $this->assertCount(1, $arStats);
        // и в ней 6 недель и суммы вдвое больше
        $arStat = $arStats[Paytype::TYPE_REVENUE];
        $this->assertCount(6, $arStat);
        $this->assertEquals(0, $arStat[0]['summ']);
        $this->assertEquals(0, $arStat[1]['summ']);
        $this->assertEquals(0, $arStat[2]['summ']);
        $this->assertEquals(16000, $arStat[3]['summ']);
        $this->assertEquals(6000, $arStat[4]['summ']);
        $this->assertEquals(0, $arStat[5]['summ']);
    }

    /** @test */
    function it_can_build_weekly_stats_with_multiple_simple_metrics()
    {
        // запушим тестовые данные
        $arType = [Paytype::TYPE_REVENUE, Paytype::TYPE_COST];
        $arPatypes = PaytypeController::getAllByType($arType);
        $arPaytypeId = Helpers::extractByField('id', $arPatypes);
        // dump($arPaytypeId);

        // в июне 2018 - 5 недель было (по охвату дат)
        $arWeeks = [
            ['01', '02', '03'],
            ['04', '05', '06'],
            ['11', '12', '13'],
            ['18', '19', '20'],
            ['25', '26', '27'],
        ];
        $arSumm = [100, 200, 300];

        $arOps = [];
        $arCompareData = []; // с этими значениями мы будем сравнивать выборку из базы
        foreach ($arWeeks as $key => $arWeek) {
            for ($i = 0; $i < 10; $i++) {
                $paytype = $arPaytypeId[array_rand($arPaytypeId)];
                $summ = $arSumm[array_rand($arSumm)];
                $arOps[] = [
                    'paytype_id' => $paytype,
                    'summ'       => $summ,
                    'date'       => '2018-06-' . $arWeek[array_rand($arWeek)],
                ];
                $arCompareData[$key][$paytype] += $summ;
            }
        }
        // dump($arOps);
        // dump($arCompareData);
        // die;
        Operation::query()->insert($arOps);

        // создаем наш парсер статы
        $arParams = [
            'fromDate' => '2018-06-01',
            'toDate'   => '2018-06-31',
            'type'     => Statistic::TYPE_WEEK,
            'metrics'  => [
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
            ],
        ];
        $rsStat = new StatisticController($this->db, $arParams);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 2 метрику статитике
        $this->assertCount(2, $arStats);
        // и в ней 5 недель
        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $this->assertCount(5, $arStats[Paytype::TYPE_REVENUE]);
        $this->assertEquals($arCompareData[0][$idRevenue], $arStats[Paytype::TYPE_REVENUE][0]['summ']);
        $this->assertEquals($arCompareData[1][$idRevenue], $arStats[Paytype::TYPE_REVENUE][1]['summ']);
        $this->assertEquals($arCompareData[2][$idRevenue], $arStats[Paytype::TYPE_REVENUE][2]['summ']);
        $this->assertEquals($arCompareData[3][$idRevenue], $arStats[Paytype::TYPE_REVENUE][3]['summ']);
        $this->assertEquals($arCompareData[4][$idRevenue], $arStats[Paytype::TYPE_REVENUE][4]['summ']);

        $idCost = $arPaytypeId[Paytype::TYPE_COST];
        $this->assertCount(5, $arStats[Paytype::TYPE_COST]);
        $this->assertEquals($arCompareData[0][$idCost], $arStats[Paytype::TYPE_COST][0]['summ']);
        $this->assertEquals($arCompareData[1][$idCost], $arStats[Paytype::TYPE_COST][1]['summ']);
        $this->assertEquals($arCompareData[2][$idCost], $arStats[Paytype::TYPE_COST][2]['summ']);
        $this->assertEquals($arCompareData[3][$idCost], $arStats[Paytype::TYPE_COST][3]['summ']);
        $this->assertEquals($arCompareData[4][$idCost], $arStats[Paytype::TYPE_COST][4]['summ']);


        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 2 метрику статитике
        $this->assertCount(2, $arStats);
        // и в ней 5 недель и суммы вдвое больше
        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $this->assertCount(5, $arStats[Paytype::TYPE_REVENUE]);
        $this->assertEquals($arCompareData[0][$idRevenue] * 2, $arStats[Paytype::TYPE_REVENUE][0]['summ']);
        $this->assertEquals($arCompareData[1][$idRevenue] * 2, $arStats[Paytype::TYPE_REVENUE][1]['summ']);
        $this->assertEquals($arCompareData[2][$idRevenue] * 2, $arStats[Paytype::TYPE_REVENUE][2]['summ']);
        $this->assertEquals($arCompareData[3][$idRevenue] * 2, $arStats[Paytype::TYPE_REVENUE][3]['summ']);
        $this->assertEquals($arCompareData[4][$idRevenue] * 2, $arStats[Paytype::TYPE_REVENUE][4]['summ']);

        $idCost = $arPaytypeId[Paytype::TYPE_COST];
        $this->assertCount(5, $arStats[Paytype::TYPE_COST]);
        $this->assertEquals($arCompareData[0][$idCost] * 2, $arStats[Paytype::TYPE_COST][0]['summ']);
        $this->assertEquals($arCompareData[1][$idCost] * 2, $arStats[Paytype::TYPE_COST][1]['summ']);
        $this->assertEquals($arCompareData[2][$idCost] * 2, $arStats[Paytype::TYPE_COST][2]['summ']);
        $this->assertEquals($arCompareData[3][$idCost] * 2, $arStats[Paytype::TYPE_COST][3]['summ']);
        $this->assertEquals($arCompareData[4][$idCost] * 2, $arStats[Paytype::TYPE_COST][4]['summ']);
    }

    /** @test */
    function it_can_build_weekly_stats_with_single_complex_metrics_revenue_minus_cost()
    {
        // тест на создание композитной статистки "баланс" = доходы - минус расходы
        // запушим тестовые данные
        $arType = [Paytype::TYPE_REVENUE, Paytype::TYPE_COST];
        $arPatypes = PaytypeController::getAllByType($arType);
        $arPaytypeId = Helpers::extractByField('id', $arPatypes);
        // в июне 2018 - 5 недель было (по охвату дат)
        $arWeeks = [
            ['01', '02', '03'],
            ['04', '05', '06'],
            ['11', '12', '13'],
            ['18', '19', '20'],
            ['25', '26', '27'],
        ];
        $arSumm = [100, 200, 300, 400, 500];

        $arOps = [];
        $arCmpData = []; // с этими значениями мы будем сравнивать результаты
        foreach ($arWeeks as $key => $arWeek) {
            for ($i = 0; $i < 10; $i++) {
                $paytype = $arPaytypeId[array_rand($arPaytypeId)];
                $summ = $arSumm[array_rand($arSumm)];
                $arOps[] = [
                    'paytype_id' => $paytype,
                    'summ'       => $summ,
                    'date'       => '2018-06-' . $arWeek[array_rand($arWeek)],
                ];
                $arCmpData[$key][$paytype] += $summ;
            }
        }
        // dump($arOps);
        // dump($arCmpData);
        // die;
        Operation::query()->insert($arOps);

        // создаем наш парсер статы
        $arParams = [
            'fromDate' => '2018-06-01',
            'toDate'   => '2018-06-31',
            'type'     => Statistic::TYPE_WEEK,
            'metrics'  => [
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
            ],
        ];
        $rsStat = new StatisticController($this->db, $arParams);
        $arStats = $rsStat->get();

        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $idCost = $arPaytypeId[Paytype::TYPE_COST];
        $metric = (string)Paytype::TYPE_REVENUE . ',' . (string)Paytype::TYPE_COST;

        // ожидаем 1 метрику статитики
        $this->assertCount(1, $arStats);
        // и в ней 5 недель
        $this->assertCount(5, $arStats[$metric]);
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCmpData[$key][$idRevenue] - $arCmpData[$key][$idCost];
            $this->assertEquals($expectedSumm, $ar['summ']);
        }

        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        $arStats = $rsStat->get();
        // ожидаем 1 метрику статитики
        $this->assertCount(1, $arStats);
        // и в ней 5 недель и суммы вдвое больше
        $this->assertCount(5, $arStats[$metric]);
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCmpData[$key][$idRevenue] - $arCmpData[$key][$idCost];
            $this->assertEquals($expectedSumm * 2, $ar['summ']);
        }
    }

    /** @test */
    function it_can_build_weekly_stats_with_one_complex_and_two_simple_metrics()
    {
        // тест на создание композитной статистки "баланс" = доходы - минус расходы
        // и одновременно с базовыми статистиками
        // запушим тестовые данные
        $arType = [Paytype::TYPE_REVENUE, Paytype::TYPE_COST];
        $arPatypes = PaytypeController::getAllByType($arType);
        $arPaytypeId = Helpers::extractByField('id', $arPatypes);
        // в июне 2018 - 5 недель было (по охвату дат)
        $arWeeks = [
            ['01', '02', '03'],
            ['04', '05', '06'],
            ['11', '12', '13'],
            ['18', '19', '20'],
            ['25', '26', '27'],
        ];
        $arSumm = [100, 200, 300, 400, 500];

        $arOps = [];
        $arCmpData = []; // с этими значениями мы будем сравнивать результаты
        foreach ($arWeeks as $key => $arWeek) {
            for ($i = 0; $i < 50; $i++) {
                $paytype = $arPaytypeId[array_rand($arPaytypeId)];
                $summ = $arSumm[array_rand($arSumm)];
                $arOps[] = [
                    'paytype_id' => $paytype,
                    'summ'       => $summ,
                    'date'       => '2018-06-' . $arWeek[array_rand($arWeek)],
                ];
                $arCmpData[$key][$paytype] += $summ;
            }
        }
        // dump($arOps);
        // dump($arCmpData);
        // die;
        Operation::query()->insert($arOps);

        // создаем наш парсер статы
        $arParams = [
            'fromDate' => '2018-06-01',
            'toDate'   => '2018-06-31',
            'type'     => Statistic::TYPE_WEEK,
            'metrics'  => [
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
            ],
        ];
        $rsStat = new StatisticController($this->db, $arParams);
        $arStats = $rsStat->get();

        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $idCost = $arPaytypeId[Paytype::TYPE_COST];
        $metric = (string)Paytype::TYPE_REVENUE . ',' . (string)Paytype::TYPE_COST;

        // ожидаем 3 метрики статитики
        $this->assertCount(3, $arStats);
        // и в ней 5 недель [комлпексная]
        $this->assertCount(5, $arStats[$metric]);
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCmpData[$key][$idRevenue] - $arCmpData[$key][$idCost];
            $this->assertEquals($expectedSumm, $ar['summ']);
        }
        // и в ней 5 недель [обычная доходы]
        $this->assertCount(5, $arStats[Paytype::TYPE_REVENUE]);
        foreach ($arStats[Paytype::TYPE_REVENUE] as $key => $ar) {
            $this->assertEquals($arCmpData[$key][$idRevenue], $ar['summ']);
        }
        // и в ней 5 недель [обычная расходы]
        $this->assertCount(5, $arStats[Paytype::TYPE_COST]);
        foreach ($arStats[Paytype::TYPE_COST] as $key => $ar) {
            $this->assertEquals($arCmpData[$key][$idCost], $ar['summ']);
        }

        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        $arStats = $rsStat->get();
        // ожидаем 3 метрики статитики
        $this->assertCount(3, $arStats);
        // и в ней 5 недель и суммы вдвое больше
        $this->assertCount(5, $arStats[$metric]);
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCmpData[$key][$idRevenue] - $arCmpData[$key][$idCost];
            $this->assertEquals($expectedSumm * 2, $ar['summ']);
        }
        // и в ней 5 недель  и суммы вдвое больше [обычная доходы]
        $this->assertCount(5, $arStats[Paytype::TYPE_REVENUE]);
        foreach ($arStats[Paytype::TYPE_REVENUE] as $key => $ar) {
            $this->assertEquals($arCmpData[$key][$idRevenue] * 2, $ar['summ']);
        }
        // и в ней 5 недель  и суммы вдвое больше [обычная расходы]
        $this->assertCount(5, $arStats[Paytype::TYPE_COST]);
        foreach ($arStats[Paytype::TYPE_COST] as $key => $ar) {
            $this->assertEquals($arCmpData[$key][$idCost] * 2, $ar['summ']);
        }
    }

    /** @test */
    function it_can_build_monthly_stats_with_one_simple_metric()
    {
        // запушим тестовые данные
        $arType = [Paytype::TYPE_REVENUE];
        $arPatypes = PaytypeController::getAllByType($arType);
        $arPaytypeId = Helpers::extractByField('id', $arPatypes);
        // dump($arPaytypeId);

        // в июне 2018 - 5 недель было (по охвату дат)
        $arDays = ['01', '02', '03', '04', '05', '06', '11', '12', '13', '14', '15', '18', '19', '20', '21', '22', '25', '26', '27', '28',];
        $arMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $arSumm = [100, 200, 300];

        $arOps = [];
        $arCompareData = []; // с этими значениями мы будем сравнивать выборку из базы
        foreach ($arMonths as $month) {
            for ($i = 0; $i < 50; $i++) {
                $paytype = $arPaytypeId[array_rand($arPaytypeId)];
                $summ = $arSumm[array_rand($arSumm)];
                $arOps[] = [
                    'paytype_id' => $paytype,
                    'summ'       => $summ,
                    'date'       => '2018-' . $month . '-' . $arDays[array_rand($arDays)],
                ];
                $arCompareData[(int)$month - 1][$paytype] += $summ;
            }
        }

        // dump($arOps);
        // dump($arCompareData);
        // die;
        Operation::query()->insert($arOps);

        // создаем наш парсер статы
        $arParams = [
            'fromDate' => '2018-01-01',
            'toDate'   => '2018-12-31',
            'type'     => Statistic::TYPE_MONTH,
            'metrics'  => [
                Paytype::TYPE_REVENUE,
            ],
        ];
        $rsStat = new StatisticController($this->db, $arParams);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 2 метрику статитике
        $this->assertCount(1, $arStats);
        // и в ней 12 месяцев
        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $arStat = $arStats[Paytype::TYPE_REVENUE];
        $this->assertCount(12, $arStat);
        // и все суммы совпадают
        foreach ($arStat as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idRevenue], $ar['summ']);
        }

        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 1 метрику статитике
        $this->assertCount(1, $arStats);
        // и в ней 12 месяцев
        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $arStat = $arStats[Paytype::TYPE_REVENUE];
        $this->assertCount(12, $arStat);
        // и все суммы совпадают
        foreach ($arStat as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idRevenue] * 2, $ar['summ']);
        }

    }

    /** @test */
    function it_can_build_monthly_stats_with_one_complex_and_two_simple_metrics()
    {
        // запушим тестовые данные
        $arType = [Paytype::TYPE_REVENUE, Paytype::TYPE_COST];
        $arPatypes = PaytypeController::getAllByType($arType);
        $arPaytypeId = Helpers::extractByField('id', $arPatypes);
        // dump($arPaytypeId);

        // в июне 2018 - 5 недель было (по охвату дат)
        $arDays = ['01', '02', '03', '04', '05', '06', '11', '12', '13', '14', '15', '18', '19', '20', '21', '22', '25', '26', '27', '28',];
        $arMonths = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $arSumm = [100, 200, 300];

        $arOps = [];
        $arCompareData = []; // с этими значениями мы будем сравнивать выборку из базы
        foreach ($arMonths as $month) {
            for ($i = 0; $i < 50; $i++) {
                $paytype = $arPaytypeId[array_rand($arPaytypeId)];
                $summ = $arSumm[array_rand($arSumm)];
                $arOps[] = [
                    'paytype_id' => $paytype,
                    'summ'       => $summ,
                    'date'       => '2018-' . $month . '-' . $arDays[array_rand($arDays)],
                ];
                $arCompareData[(int)$month - 1][$paytype] += $summ;
            }
        }

        // dump($arOps);
        // dump($arCompareData);
        // die;
        Operation::query()->insert($arOps);

        // создаем наш парсер статы
        $arParams = [
            'fromDate' => '2018-01-01',
            'toDate'   => '2018-12-31',
            'type'     => Statistic::TYPE_MONTH,
            'metrics'  => [
                [Paytype::TYPE_REVENUE, Paytype::TYPE_COST],
                Paytype::TYPE_REVENUE,
                Paytype::TYPE_COST,
            ],
        ];

        $idRevenue = $arPaytypeId[Paytype::TYPE_REVENUE];
        $idCost = $arPaytypeId[Paytype::TYPE_COST];
        $metric = (string)Paytype::TYPE_REVENUE . ',' . (string)Paytype::TYPE_COST;

        $rsStat = new StatisticController($this->db, $arParams);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 3 метрику статитике
        $this->assertCount(3, $arStats);
        // и в ней 12 месяцев [комлексная]
        $arStat = $arStats[$metric];
        $this->assertCount(12, $arStat);
        // и все суммы совпадают [комлексная]
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCompareData[$key][$idRevenue] - $arCompareData[$key][$idCost];
            $this->assertEquals($expectedSumm, $ar['summ']);
        }
        // и в ней 12 месяцев [обычная доходы]
        foreach ($arStats[Paytype::TYPE_REVENUE] as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idRevenue], $ar['summ']);
        }
        // и в ней 12 месяцев [обычная расходы]
        foreach ($arStats[Paytype::TYPE_COST] as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idCost], $ar['summ']);
        }

        // добавим еще столько же операций
        Operation::query()->insert($arOps);
        // $rsStat->build();
        $arStats = $rsStat->get();
        // dump($arStats);
        // ожидаем 1 метрику статитике
        $this->assertCount(3, $arStats);
        // и в ней 12 месяцев
        $arStat = $arStats[$metric];
        $this->assertCount(12, $arStat);
        // и все суммы совпадают [комлексная]
        foreach ($arStats[$metric] as $key => $ar) {
            $expectedSumm = $arCompareData[$key][$idRevenue] - $arCompareData[$key][$idCost];
            $this->assertEquals($expectedSumm * 2, $ar['summ']);
        }
        // и в ней 12 месяцев [обычная доходы]
        foreach ($arStats[Paytype::TYPE_REVENUE] as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idRevenue] * 2, $ar['summ']);
        }
        // и в ней 12 месяцев [обычная расходы]
        foreach ($arStats[Paytype::TYPE_COST] as $key => $ar) {
            $this->assertEquals($arCompareData[$key][$idCost] * 2, $ar['summ']);
        }

    }

    public function tearDown()
    {
        parent::tearDown();
        $PortalInstall = new PortalInstall($this->arDataAppHosted, $this->db);
        $PortalInstall->Down();

        // вручную нужну убить БД тестовую
        $this->db->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `portal_non_existent`");

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }
}
