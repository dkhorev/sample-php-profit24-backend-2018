<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPUnit\Framework\TestCase;
use WebNow\App;
use WebNow\Project\Install\AppInstall;

class AppInstallTest extends TestCase
{
    protected $dbTestName;
    /**
     * @var Capsule
     */
    protected $db;

    protected function setUp()
    {
        parent::setUp();
        $this->dbTestName = 'profit_b24_testing';
        // $this->db = App::Capsule();
        $this->db = new Capsule;

        // нужно задать "основное" соединение в капсуле
        $this->db->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => $this->dbTestName,
            'username'  => DB_USER,
            'password'  => DB_PASS,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ], CONNECTION_MAIN);
        $this->db->setAsGlobal();
        $this->db->bootEloquent();
    }

    function test_it_can_be_instanced()
    {
        $AppInstall = new AppInstall($this->db);
        $this->assertInstanceOf('WebNow\Project\Install\AppInstall', $AppInstall);
    }

    function test_can_do_Up_method()
    {
        $AppInstall = new AppInstall($this->db);
        $this->assertTrue($AppInstall->Up($this->dbTestName));
        $this->assertTrue($this->db->schema()->hasTable('portals'));
    }

    function test_can_do_Down_method()
    {
        $AppInstall = new AppInstall($this->db);
        $this->assertTrue($AppInstall->Down($this->dbTestName));
    }

    public function tearDown()
    {
        parent::tearDown();

        $AppInstall = new AppInstall($this->db);
        $AppInstall->Down($this->dbTestName);
    }

}
