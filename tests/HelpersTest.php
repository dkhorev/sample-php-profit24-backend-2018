<?php

use WebNow\Helpers;
use PHPUnit\Framework\TestCase;

class HelpersTest extends TestCase
{

    public function testValidateArray()
    {
        $rules = ['name' => 'required'];
        $input = ['name' => ''];
        $arValidate = Helpers::validateArray($input, $rules);
        $this->assertTrue($arValidate['error']);

        $rules = ['name' => 'required'];
        $input = ['name' => '   '];
        $arValidate = Helpers::validateArray($input, $rules);
        $this->assertTrue($arValidate['error']);

        $rules = ['name' => 'required'];
        $input = ['name' => 'good name   '];
        $arValidate = Helpers::validateArray($input, $rules);
        $this->assertCount(0, $arValidate);
    }

    public function testExtractByField()
    {
        $input = [
            ['ID' => 1, 'TEST' => -1],
            ['ID' => 2, 'TEST' => -2],
            ['ID' => 3, 'TEST' => -3],
            ['ID' => 4, 'TESTBAD' => -3],
        ];
        $arRes = Helpers::extractByField('TEST', $input);
        $this->assertCount(3, $arRes);
        $this->assertEquals([-1, -2, -3], $arRes);

        $arRes = Helpers::extractByField('ID', $input);
        $this->assertCount(4, $arRes);
        $this->assertEquals([1, 2, 3, 4], $arRes);
    }

    public function testGetCode()
    {
        $code = Helpers::getCode('test', 999);
        $this->assertEquals('999_test', $code);

        $code = Helpers::getCode('test');
        $this->assertEquals('test', $code);

        $code = Helpers::getCode('вася пупкин');
        $this->assertEquals('vasia_pupkin', $code);
    }
}
