<?php

namespace WebNow;


/**
 * Class Cron
 *
 * @package WebNow\Cron
 */
class Cron
{
    /**
     * выполняет задачи по списку
     */
    public function run()
    {
        dump('(new Cron)->run();');

        // id, задача, периодичность, посл. запуск
        // todo выбрать из таблицы заданий те что запускались давно
        // todo последовательно их запустить в работу
        // todo первая задача - обновление ключей по кругу (считаем что рефреш длится 28 дней)
        // todo считаем что при активной работе портала ключ и так будет обновлен (это все надо проверять)
    }

    /**
     * @param array $arData
     *
     * @return bool
     */
    public function addJob(array $arData)
    {
        return true;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteJob(int $id)
    {
        return true;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    private function runJob(int $id)
    {
        return true;
    }
}
